/* Copyright 2012 Mozilla Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @module pdfjsLib
 */
import {
    createPromiseCapability,
    getVerbosityLevel,
    shadow,
    warn,
    AnnotationMode,
    RenderingIntentFlag,
    AbortException
} from "../shared/util.js";
import {MessageHandler} from "../shared/message_handler.js";
import {WorkerMessageHandler} from "../core/worker.js";
import { XfaText } from "./xfa_text.js";
import {
    deprecated,
    DOMCanvasFactory,
    DOMCMapReaderFactory,
    DOMStandardFontDataFactory,
    isDataScheme,
    loadScript,
    PageViewport,
    RenderingCancelledException,
    StatTimer,
} from "./display_utils.js";
import { OptionalContentConfig } from "./optional_content_config.js";
import { CanvasGraphics } from "./canvas.js";
import { FontFaceObject, FontLoader } from "./font_loader.js";
//import {deepCopy} from '../deepCopyAll.js';

const DefaultCanvasFactory = DOMCanvasFactory;
const DefaultCMapReaderFactory = DOMCMapReaderFactory;
const DefaultStandardFontDataFactory =DOMStandardFontDataFactory;

const PDFWorkerUtil = {
    isWorkerDisabled: false,
    fallbackWorkerSrc: null,
    fakeWorkerId: 0,
};


class PDFWorker {
    static get _workerPorts() {
        return shadow(this, "_workerPorts", new WeakMap());
    }

    constructor({
                    name = null,
                    port = null,
                    verbosity = getVerbosityLevel(),
                } = {}) {
        if (port && PDFWorker._workerPorts.has(port)) {
            throw new Error("Cannot use more than one PDFWorker per port.");
        }

        this.name = name;
        this.destroyed = false;
        this.verbosity = verbosity;

        this._readyCapability = createPromiseCapability();
        this._port = null;
        this._webWorker = null;
        this._messageHandler = null;
        this._initialize();
    }

    get promise() {
        return this._readyCapability.promise;
    }

    get port() {
        return this._port;
    }

    get messageHandler() {
        return this._messageHandler;
    }

    _initialize() {
        this._setupFakeWorker();
    }

    _setupFakeWorker() {
        if (!PDFWorkerUtil.isWorkerDisabled) {
            warn("Setting up fake worker.");
            PDFWorkerUtil.isWorkerDisabled = true;
        }
        //var workerMessageHandler = shadow(this, "_setupFakeWorkerGlobal", new WorkerMessageHandler());
        //PDFWorker._setupFakeWorkerGlobal
        //    .then(WorkerMessageHandler => {
        if (this.destroyed) {
            this._readyCapability.reject(new Error("Worker was destroyed"));
            return;
        }
        const port = new LoopbackPort();
        this._port = port;

        // All fake workers use the same port, making id unique.
        const id = `fake${PDFWorkerUtil.fakeWorkerId++}`;

        // If the main thread is our worker, setup the handling for the
        // messages -- the main thread sends to it self.
        const workerHandler = new MessageHandler(id + "_worker", id, port);
        WorkerMessageHandler.setup(workerHandler, port);

        const messageHandler = new MessageHandler(id, id + "_worker", port);
        this._messageHandler = messageHandler;
        this._readyCapability.resolve();
        // Send global setting, e.g. verbosity level.
        messageHandler.send("configure", {
            verbosity: this.verbosity,
        });
        // })
        //  .catch(reason => {
        //      this._readyCapability.reject(
        //         new Error(`Setting up fake worker failed: "${reason.message}".`)
        //     );
        // });
    }

    static get _setupFakeWorkerGlobal() {
        return shadow(this, "_setupFakeWorkerGlobal", new WorkerMessageHandler());
    }
}

class LoopbackPort {
    constructor() {
        this._listeners = [];
        this._deferred = Promise.resolve();
    }

    postMessage(obj, transfers) {
        const event = {
            data: obj,//deepCopy(obj),
        };

        this._deferred.then(() => {
                for (const listener of this._listeners) {
                    try {
                        listener.call(this, event);
                    }catch(e){
                        console.log("pdfjs** "+ e)
                    }
                }
        }).catch((err)=>{
            console.log("pdfjs** "+err)
        });
    }

    addEventListener(name, listener) {
        this._listeners.push(listener);
    }

    removeEventListener(name, listener) {
        const i = this._listeners.indexOf(listener);
        this._listeners.splice(i, 1);
    }

    terminate() {
        this._listeners.length = 0;
    }
}

class PDFDocumentLoadingTask {
    static get idCounters() {
        return shadow(this, "idCounters", { doc: 0 });
    }

    constructor() {
        this._capability = createPromiseCapability();
        this._transport = null;
        this._worker = null;


        this.docId = `d${PDFDocumentLoadingTask.idCounters.doc++}`;


        this.destroyed = false;


        this.onPassword = null;


        this.onProgress = null;


        this.onUnsupportedFeature = null;
    }


    get promise() {
        return this._capability.promise;
    }


    async destroy() {
        this.destroyed = true;
        await this._transport?.destroy();

        this._transport = null;
        if (this._worker) {
            this._worker.destroy();
            this._worker = null;
        }
    }
}
function getDocument(src) {
    const task = new PDFDocumentLoadingTask();
    var worker = new PDFWorker();
    task._worker = worker;
    const docId = task.docId;
    worker.promise
        .then(function () {
            if (task.destroyed) {
                throw new Error("Loading aborted");
            }

            const workerIdPromise = _fetchDocument(worker, src, docId);
            return Promise.all([workerIdPromise]).then(
                function ([workerId]) {
                    console.log("pdfjs** pdf2:----------:" + workerId);
                    if (task.destroyed) {
                        throw new Error("pdfjs** Loading aborted");
                    }

                    const messageHandler = new MessageHandler(
                        docId,
                        workerId,
                        worker.port
                    );
                    //TODO sreeni handle transport
                    const transport = new WorkerTransport(
                        messageHandler,
                        task,
                        null,
                        {"ownerDocument":null,"pdfBug":null}
                    );
                    task._transport = transport;
                    messageHandler.send("Ready", null);
                }
            );
        });
    return task;
}

async function _fetchDocument(worker, source, docId) {
    if (worker.destroyed) {
        throw new Error("Worker was destroyed");
    }
    const workerId = await worker._messageHandler.sendWithPromise("GetDocRequest", {
        "apiVersion": "2.13.167",
        "cMapUrl": undefined,
        "disableFontFace": false,
        "docBaseUrl": null,
        "docId": docId,
        "enableXfa": false,
        "fontExtraProperties": false,
        "ignoreErrors": true,
        "isEvalSupported": true,
        "maxImageSize": -1,
        "source": {
            data: source,
            url: undefined,
            password: undefined,
            disableAutoFetch: false,
            rangeChunkSize: 65536,
            length: undefined,
        },
        "standardFontDataUrl": undefined,
        "useSystemFonts": true
    });
    if (worker.destroyed) {
        throw new Error("Worker was destroyed");
    }
    return workerId;
}
class WorkerTransport {
    docStats = null;

    pageCache = new Map();

    pagePromises = new Map();

    metadataPromise = null;

    constructor(messageHandler, loadingTask, networkStream, params) {
        this.messageHandler = messageHandler;
        this.loadingTask = loadingTask;
        this.commonObjs = new PDFObjects();
        this.fontLoader = new FontLoader({
            docId: loadingTask.docId,
            onUnsupportedFeature: this._onUnsupportedFeature.bind(this),
            ownerDocument: params.ownerDocument,
            styleElement: params.styleElement,
        });
        this._params = params;
        //TODO sreeni


        this.destroyed = false;
        this.destroyCapability = null;
        this._passwordCapability = null;

        this._networkStream = networkStream;
        this._fullReader = null;
        this._lastProgress = null;
        this.downloadInfoCapability = createPromiseCapability();

        this.setupMessageHandler();
    }
    get annotationStorage() {
        return shadow(this, "annotationStorage", new AnnotationStorage());
    }

    get stats() {
        return this.docStats;
    }

    getRenderingIntent(
        intent,
        annotationMode = AnnotationMode.ENABLE,
        isOpList = false
    ) {
        let renderingIntent = RenderingIntentFlag.DISPLAY; // Default value.
        let lastModified = "";

        switch (intent) {
            case "any":
                renderingIntent = RenderingIntentFlag.ANY;
                break;
            case "display":
                break;
            case "print":
                renderingIntent = RenderingIntentFlag.PRINT;
                break;
            default:
                warn(`getRenderingIntent - invalid intent: ${intent}`);
        }

        switch (annotationMode) {
            case AnnotationMode.DISABLE:
                renderingIntent += RenderingIntentFlag.ANNOTATIONS_DISABLE;
                break;
            case AnnotationMode.ENABLE:
                break;
            case AnnotationMode.ENABLE_FORMS:
                renderingIntent += RenderingIntentFlag.ANNOTATIONS_FORMS;
                break;
            case AnnotationMode.ENABLE_STORAGE:
                renderingIntent += RenderingIntentFlag.ANNOTATIONS_STORAGE;

                lastModified = this.annotationStorage.lastModified;
                break;
            default:
                warn(`getRenderingIntent - invalid annotationMode: ${annotationMode}`);
        }

        if (isOpList) {
            renderingIntent += RenderingIntentFlag.OPLIST;
        }

        return {
            renderingIntent,
            cacheKey: `${renderingIntent}_${lastModified}`,
        };
    }

    destroy() {
        if (this.destroyCapability) {
            return this.destroyCapability.promise;
        }

        this.destroyed = true;
        this.destroyCapability = createPromiseCapability();

        if (this._passwordCapability) {
            this._passwordCapability.reject(
                new Error("Worker was destroyed during onPassword callback")
            );
        }

        const waitOn = [];
        // We need to wait for all renderings to be completed, e.g.
        // timeout/rAF can take a long time.
        for (const page of this.pageCache.values()) {
            waitOn.push(page._destroy());
        }
        this.pageCache.clear();
        this.pagePromises.clear();
        // Allow `AnnotationStorage`-related clean-up when destroying the document.
        if (this.hasOwnProperty("annotationStorage")) {
            this.annotationStorage.resetModified();
        }
        // We also need to wait for the worker to finish its long running tasks.
        const terminated = this.messageHandler.sendWithPromise("Terminate", null);
        waitOn.push(terminated);

        Promise.all(waitOn).then(() => {
            this.commonObjs.clear();
            this.fontLoader.clear();
            this.metadataPromise = null;
            this._getFieldObjectsPromise = null;
            this._hasJSActionsPromise = null;

            if (this._networkStream) {
                this._networkStream.cancelAllRequests(
                    new AbortException("Worker was terminated.")
                );
            }

            if (this.messageHandler) {
                this.messageHandler.destroy();
                this.messageHandler = null;
            }
            this.destroyCapability.resolve();
        }, this.destroyCapability.reject);
        return this.destroyCapability.promise;
    }

    setupMessageHandler() {
        const { messageHandler, loadingTask } = this;

        messageHandler.on("GetReader", (data, sink) => {
            assert(
                this._networkStream,
                "GetReader - no `IPDFStream` instance available."
            );
            this._fullReader = this._networkStream.getFullReader();
            this._fullReader.onProgress = evt => {
                this._lastProgress = {
                    loaded: evt.loaded,
                    total: evt.total,
                };
            };
            sink.onPull = () => {
                this._fullReader
                    .read()
                    .then(function ({ value, done }) {
                        if (done) {
                            sink.close();
                            return;
                        }
                        assert(
                        isArrayBuffer(value),
                            "GetReader - expected an ArrayBuffer."
                        );
                        // Enqueue data chunk into sink, and transfer it
                        // to other side as `Transferable` object.
                        sink.enqueue(new Uint8Array(value), 1, [value]);
                    })
                    .catch(reason => {
                        sink.error(reason);
                    });
            };

            sink.onCancel = reason => {
                this._fullReader.cancel(reason);

                sink.ready.catch(readyReason => {
                    if (this.destroyed) {
                        return; // Ignore any pending requests if the worker was terminated.
                    }
                    throw readyReason;
                });
            };
        });

        messageHandler.on("ReaderHeadersReady", data => {
            const headersCapability = createPromiseCapability();
            const fullReader = this._fullReader;
            fullReader.headersReady.then(() => {
                // If stream or range are disabled, it's our only way to report
                // loading progress.
                if (!fullReader.isStreamingSupported || !fullReader.isRangeSupported) {
                    if (this._lastProgress) {
                        loadingTask.onProgress?.(this._lastProgress);
                    }
                    fullReader.onProgress = evt => {
                        loadingTask.onProgress?.({
                            loaded: evt.loaded,
                            total: evt.total,
                        });
                    };
                }

                headersCapability.resolve({
                    isStreamingSupported: fullReader.isStreamingSupported,
                    isRangeSupported: fullReader.isRangeSupported,
                    contentLength: fullReader.contentLength,
                });
            }, headersCapability.reject);

            return headersCapability.promise;
        });

        messageHandler.on("GetRangeReader", (data, sink) => {
            assert(
                this._networkStream,
                "GetRangeReader - no `IPDFStream` instance available."
            );
            const rangeReader = this._networkStream.getRangeReader(
                data.begin,
                data.end
            );

            // When streaming is enabled, it's possible that the data requested here
            // has already been fetched via the `_fullRequestReader` implementation.
            // However, given that the PDF data is loaded asynchronously on the
            // main-thread and then sent via `postMessage` to the worker-thread,
            // it may not have been available during parsing (hence the attempt to
            // use range requests here).
            //
            // To avoid wasting time and resources here, we'll thus *not* dispatch
            // range requests if the data was already loaded but has not been sent to
            // the worker-thread yet (which will happen via the `_fullRequestReader`).
            if (!rangeReader) {
                sink.close();
                return;
            }

            sink.onPull = () => {
                rangeReader
                    .read()
                    .then(function ({ value, done }) {
                        if (done) {
                            sink.close();
                            return;
                        }
                        assert(
                        isArrayBuffer(value),
                            "GetRangeReader - expected an ArrayBuffer."
                        );
                        sink.enqueue(new Uint8Array(value), 1, [value]);
                    })
                    .catch(reason => {
                        sink.error(reason);
                    });
            };

            sink.onCancel = reason => {
                rangeReader.cancel(reason);

                sink.ready.catch(readyReason => {
                    if (this.destroyed) {
                        return; // Ignore any pending requests if the worker was terminated.
                    }
                    throw readyReason;
                });
            };
        });

        messageHandler.on("GetDoc", ({ pdfInfo }) => {
            this._numPages = pdfInfo.numPages;
            this._htmlForXfa = pdfInfo.htmlForXfa;
            delete pdfInfo.htmlForXfa;
            loadingTask._capability.resolve(new PDFDocumentProxy(pdfInfo, this));
        });

        messageHandler.on("DocException", function (ex) {
            let reason;
            switch (ex.name) {
                case "PasswordException":
                    reason = new PasswordException(ex.message, ex.code);
                    break;
                case "InvalidPDFException":
                    reason = new InvalidPDFException(ex.message);
                    break;
                case "MissingPDFException":
                    reason = new MissingPDFException(ex.message);
                    break;
                case "UnexpectedResponseException":
                    reason = new UnexpectedResponseException(ex.message, ex.status);
                    break;
                case "UnknownErrorException":
                    reason = new UnknownErrorException(ex.message, ex.details);
                    break;
                default:
                    unreachable("DocException - expected a valid Error.");
            }
            loadingTask._capability.reject(reason);
        });

        messageHandler.on("PasswordRequest", exception => {
            this._passwordCapability = createPromiseCapability();

            if (loadingTask.onPassword) {
                const updatePassword = password => {
                    if (password instanceof Error) {
                        this._passwordCapability.reject(password);
                    } else {
                        this._passwordCapability.resolve({ password });
                    }
                };
                try {
                    loadingTask.onPassword(updatePassword, exception.code);
                } catch (ex) {
                    this._passwordCapability.reject(ex);
                }
            } else {
                this._passwordCapability.reject(
                    new PasswordException(exception.message, exception.code)
                );
            }
            return this._passwordCapability.promise;
        });

        messageHandler.on("DataLoaded", data => {
            // For consistency: Ensure that progress is always reported when the
            // entire PDF file has been loaded, regardless of how it was fetched.
            if( loadingTask.onProgress) {
                loadingTask.onProgress?.({
                    loaded: data.length,
                    total: data.length,
                });
            }

            this.downloadInfoCapability.resolve(data);
        });

        messageHandler.on("StartRenderPage", data => {
            if (this.destroyed) {
                return; // Ignore any pending requests if the worker was terminated.
            }

            const page = this.pageCache.get(data.pageIndex);
            page._startRenderPage(data.transparency, data.cacheKey);
        });

        messageHandler.on("commonobj", ([id, type, exportedData]) => {
            if (this.destroyed) {
                return; // Ignore any pending requests if the worker was terminated.
            }

            if (this.commonObjs.has(id)) {
                return;
            }

            switch (type) {
                case "Font":
                    const params = this._params;

                    if ("error" in exportedData) {
                        const exportedError = exportedData.error;
                        warn(`Error during font loading: ${exportedError}`);
                        this.commonObjs.resolve(id, exportedError);
                        break;
                    }

                    let fontRegistry = null;
                    if (params.pdfBug && globalThis.FontInspector?.enabled) {
                        fontRegistry = {
                            registerFont(font, url) {
                                globalThis.FontInspector.fontAdded(font, url);
                            },
                        };
                    }
                    const font = new FontFaceObject(exportedData, {
                        isEvalSupported: params.isEvalSupported,
                        disableFontFace: params.disableFontFace,
                        ignoreErrors: params.ignoreErrors,
                        onUnsupportedFeature: this._onUnsupportedFeature.bind(this),
                        fontRegistry,
                    });
                    this.commonObjs.resolve(id, font);
                    //TODO sreeni

                    /* sreeni 把fontLoader去掉了
                    this.fontLoader
                        .bind(font)
                        .catch(reason => {
                            return messageHandler.sendWithPromise("FontFallback", { id });
                        })
                        .finally(() => {
                            if (!params.fontExtraProperties && font.data) {
                                // Immediately release the `font.data` property once the font
                                // has been attached to the DOM, since it's no longer needed,
                                // rather than waiting for a `PDFDocumentProxy.cleanup` call.
                                // Since `font.data` could be very large, e.g. in some cases
                                // multiple megabytes, this will help reduce memory usage.
                                font.data = null;
                            }
                            this.commonObjs.resolve(id, font);
                        });
                        */

                    break;
                case "FontPath":
                case "Image":
                    this.commonObjs.resolve(id, exportedData);
                    break;
                default:
                    throw new Error(`Got unknown common object type ${type}`);
            }
        });

        messageHandler.on("obj", ([id, pageIndex, type, imageData]) => {
            if (this.destroyed) {
                // Ignore any pending requests if the worker was terminated.
                return;
            }

            const pageProxy = this.pageCache.get(pageIndex);
            if (pageProxy.objs.has(id)) {
                return;
            }

            switch (type) {
                case "Image":
                    pageProxy.objs.resolve(id, imageData);

                // Heuristic that will allow us not to store large data.
                    const MAX_IMAGE_SIZE_TO_STORE = 8000000;
                    if (imageData.data.length > MAX_IMAGE_SIZE_TO_STORE) {
                        pageProxy.cleanupAfterRender = true;
                    }
                    break;
                case "Pattern":
                    pageProxy.objs.resolve(id, imageData);
                    break;
                default:
                    throw new Error(`Got unknown object type ${type}`);
            }
        });

        messageHandler.on("DocProgress", data => {
            if (this.destroyed) {
                return; // Ignore any pending requests if the worker was terminated.
            }
            loadingTask.onProgress?.({
                loaded: data.loaded,
                total: data.total,
            });
        });

        messageHandler.on("DocStats", data => {
            if (this.destroyed) {
                return; // Ignore any pending requests if the worker was terminated.
            }
            // Ensure that a `PDFDocumentProxy.stats` call-site cannot accidentally
            // modify this internal data.
            this.docStats = Object.freeze({
                streamTypes: Object.freeze(data.streamTypes),
                fontTypes: Object.freeze(data.fontTypes),
            });
        });

        messageHandler.on(
            "UnsupportedFeature",
        this._onUnsupportedFeature.bind(this)
        );

        messageHandler.on("FetchBuiltInCMap", data => {
            if (this.destroyed) {
                return Promise.reject(new Error("Worker was destroyed."));
            }
            if (!this.CMapReaderFactory) {
                return Promise.reject(
                    new Error(
                        "CMapReaderFactory not initialized, see the `useWorkerFetch` parameter."
                    )
                );
            }
            return this.CMapReaderFactory.fetch(data);
        });

        messageHandler.on("FetchStandardFontData", data => {
            if (this.destroyed) {
                return Promise.reject(new Error("Worker was destroyed."));
            }
            if (!this.StandardFontDataFactory) {
                return Promise.reject(
                    new Error(
                        "StandardFontDataFactory not initialized, see the `useWorkerFetch` parameter."
                    )
                );
            }
            return this.StandardFontDataFactory.fetch(data);
        });
    }

    _onUnsupportedFeature({ featureId }) {
        if (this.destroyed) {
            return; // Ignore any pending requests if the worker was terminated.
        }
        if(this.loadingTask.onUnsupportedFeature) {
            this.loadingTask.onUnsupportedFeature.featureId;
        }
    }

    getData() {
        return this.messageHandler.sendWithPromise("GetData", null);
    }

    getPage(pageNumber) {
        if (
        !Number.isInteger(pageNumber) ||
        pageNumber <= 0 ||
        pageNumber > this._numPages
        ) {
            return Promise.reject(new Error("Invalid page request."));
        }

        const pageIndex = pageNumber - 1,
            cachedPromise = this.pagePromises.get(pageIndex);
        if (cachedPromise) {
            return cachedPromise;
        }
        const promise = this.messageHandler
            .sendWithPromise("GetPage", {
                pageIndex,
            })
            .then(pageInfo => {
                if (this.destroyed) {
                    throw new Error("Transport destroyed");
                }
                const page = new PDFPageProxy(
                    pageIndex,
                    pageInfo,
                    this,
                    this._params.ownerDocument,
                    this._params.pdfBug
                );
                this.pageCache.set(pageIndex, page);
                return page;
            });
        this.pagePromises.set(pageIndex, promise);
        return promise;
    }

    getPageIndex(ref) {
        if (
        typeof ref !== "object" ||
        ref === null ||
        !Number.isInteger(ref.num) ||
        ref.num < 0 ||
        !Number.isInteger(ref.gen) ||
        ref.gen < 0
        ) {
            return Promise.reject(new Error("Invalid pageIndex request."));
        }
        return this.messageHandler.sendWithPromise("GetPageIndex", {
            num: ref.num,
            gen: ref.gen,
        });
    }

    getAnnotations(pageIndex, intent) {
        return this.messageHandler.sendWithPromise("GetAnnotations", {
            pageIndex,
            intent,
        });
    }

    saveDocument() {
        return this.messageHandler
            .sendWithPromise("SaveDocument", {
                isPureXfa: !!this._htmlForXfa,
                numPages: this._numPages,
                annotationStorage: this.annotationStorage.serializable,
                filename: this._fullReader?.filename ?? null,
            })
            .finally(() => {
                this.annotationStorage.resetModified();
            });
    }

    getFieldObjects() {
        return (this._getFieldObjectsPromise ||=
        this.messageHandler.sendWithPromise("GetFieldObjects", null));
    }

    hasJSActions() {
        return (this._hasJSActionsPromise ||= this.messageHandler.sendWithPromise(
            "HasJSActions",
            null
        ));
    }

    getCalculationOrderIds() {
        return this.messageHandler.sendWithPromise("GetCalculationOrderIds", null);
    }

    getDestinations() {
        return this.messageHandler.sendWithPromise("GetDestinations", null);
    }

    getDestination(id) {
        if (typeof id !== "string") {
            return Promise.reject(new Error("Invalid destination request."));
        }
        return this.messageHandler.sendWithPromise("GetDestination", {
            id,
        });
    }

    getPageLabels() {
        return this.messageHandler.sendWithPromise("GetPageLabels", null);
    }

    getPageLayout() {
        return this.messageHandler.sendWithPromise("GetPageLayout", null);
    }

    getPageMode() {
        return this.messageHandler.sendWithPromise("GetPageMode", null);
    }

    getViewerPreferences() {
        return this.messageHandler.sendWithPromise("GetViewerPreferences", null);
    }

    getOpenAction() {
        return this.messageHandler.sendWithPromise("GetOpenAction", null);
    }

    getAttachments() {
        return this.messageHandler.sendWithPromise("GetAttachments", null);
    }

    getJavaScript() {
        return this.messageHandler.sendWithPromise("GetJavaScript", null);
    }

    getDocJSActions() {
        return this.messageHandler.sendWithPromise("GetDocJSActions", null);
    }

    getPageJSActions(pageIndex) {
        return this.messageHandler.sendWithPromise("GetPageJSActions", {
            pageIndex,
        });
    }

    getStructTree(pageIndex) {
        return this.messageHandler.sendWithPromise("GetStructTree", {
            pageIndex,
        });
    }

    getOutline() {
        return this.messageHandler.sendWithPromise("GetOutline", null);
    }

    getOptionalContentConfig() {
        return this.messageHandler
            .sendWithPromise("GetOptionalContentConfig", null)
            .then(results => {
                return new OptionalContentConfig(results);
            });
    }

    getPermissions() {
        return this.messageHandler.sendWithPromise("GetPermissions", null);
    }

    getMetadata() {
        return (this.metadataPromise ||= this.messageHandler
            .sendWithPromise("GetMetadata", null)
            .then(results => {
                return {
                    info: results[0],
                    metadata: results[1] ? new Metadata(results[1]) : null,
                    contentDispositionFilename: this._fullReader?.filename ?? null,
                    contentLength: this._fullReader?.contentLength ?? null,
                };
            }));
    }

    getMarkInfo() {
        return this.messageHandler.sendWithPromise("GetMarkInfo", null);
    }

    async startCleanup(keepLoadedFonts = false) {
        await this.messageHandler.sendWithPromise("Cleanup", null);

        if (this.destroyed) {
            return; // No need to manually clean-up when destruction has started.
        }
        for (const page of this.pageCache.values()) {
            const cleanupSuccessful = page.cleanup();

            if (!cleanupSuccessful) {
                throw new Error(
                    `startCleanup: Page ${page.pageNumber} is currently rendering.`
                );
            }
        }
        this.commonObjs.clear();
        if (!keepLoadedFonts) {
            this.fontLoader.clear();
        }
        this.metadataPromise = null;
        this._getFieldObjectsPromise = null;
        this._hasJSActionsPromise = null;
    }

    get loadingParams() {
        const params = this._params;
        return shadow(this, "loadingParams", {
            disableAutoFetch: params.disableAutoFetch,
            enableXfa: params.enableXfa,
        });
    }
}

class PDFDocumentProxy {
    constructor(pdfInfo, transport) {
        this._pdfInfo = pdfInfo;
        this._transport = transport;
    }

    get annotationStorage() {
        return this._transport.annotationStorage;
    }


    get numPages() {
        return this._pdfInfo.numPages;
    }


    get fingerprints() {
        return this._pdfInfo.fingerprints;
    }


    get stats() {
        return this._transport.stats;
    }


    get isPureXfa() {
        return !!this._transport._htmlForXfa;
    }

    get allXfaHtml() {
        return this._transport._htmlForXfa;
    }


    getPage(pageNumber) {
        return this._transport.getPage(pageNumber);
    }

    getPageIndex(ref) {
        return this._transport.getPageIndex(ref);
    }

    getDestinations() {
        return this._transport.getDestinations();
    }

    getDestination(id) {
        return this._transport.getDestination(id);
    }

    getPageLabels() {
        return this._transport.getPageLabels();
    }

    getPageLayout() {
        return this._transport.getPageLayout();
    }

    getPageMode() {
        return this._transport.getPageMode();
    }

    getViewerPreferences() {
        return this._transport.getViewerPreferences();
    }

    getOpenAction() {
        return this._transport.getOpenAction();
    }

    getAttachments() {
        return this._transport.getAttachments();
    }


    getJavaScript() {
        return this._transport.getJavaScript();
    }

    getJSActions() {
        return this._transport.getDocJSActions();
    }


    getOutline() {
        return this._transport.getOutline();
    }

    getOptionalContentConfig() {
        return this._transport.getOptionalContentConfig();
    }


    getPermissions() {
        return this._transport.getPermissions();
    }


    getMetadata() {
        return this._transport.getMetadata();
    }


    getMarkInfo() {
        return this._transport.getMarkInfo();
    }

    getData() {
        return this._transport.getData();
    }


    getDownloadInfo() {
        return this._transport.downloadInfoCapability.promise;
    }

    cleanup(keepLoadedFonts = false) {
        return this._transport.startCleanup(keepLoadedFonts || this.isPureXfa);
    }


    destroy() {
        return this.loadingTask.destroy();
    }

    get loadingParams() {
        return this._transport.loadingParams;
    }


    get loadingTask() {
        return this._transport.loadingTask;
    }


    saveDocument() {
        return this._transport.saveDocument();
    }


    getFieldObjects() {
        return this._transport.getFieldObjects();
    }


    hasJSActions() {
        return this._transport.hasJSActions();
    }


    getCalculationOrderIds() {
        return this._transport.getCalculationOrderIds();
    }
}
class PDFObjects {
    objs = Object.create(null);


    ensureObj(objId) {
        const obj = this.objs[objId];
        if (obj) {
            return obj;
        }
        return (this.objs[objId] = {
            capability: createPromiseCapability(),
            data: null,
        });
    }


    get(objId, callback = null) {
        // If there is a callback, then the get can be async and the object is
        // not required to be resolved right now.
        if (callback) {
            const obj = this.ensureObj(objId);
            obj.capability.promise.then(() => callback(obj.data));
            return null;
        }
        // If there isn't a callback, the user expects to get the resolved data
        // directly.
        const obj = this.objs[objId];
        // If there isn't an object yet or the object isn't resolved, then the
        // data isn't ready yet!
        if (!obj?.capability.settled) {
            throw new Error(`Requesting object that isn't resolved yet ${objId}.`);
        }
        return obj.data;
    }


    has(objId) {
        const obj = this.objs[objId];
        return obj?.capability.settled || false;
    }


    resolve(objId, data = null) {
        const obj = this.ensureObj(objId);
        obj.data = data;
        obj.capability.resolve();
    }

    clear() {
        this.objs = Object.create(null);
    }
}

class PDFPageProxy {
    constructor(pageIndex, pageInfo, transport, ownerDocument, pdfBug = false) {
        this._pageIndex = pageIndex;
        this._pageInfo = pageInfo;
        this._ownerDocument = ownerDocument;
        this._transport = transport;
        //TODO sreeni
        //this._stats = pdfBug ? new StatTimer() : null;
        this._stats = null;
        this._pdfBug = pdfBug;
        this.commonObjs = transport.commonObjs;
        this.objs = new PDFObjects();

        this.cleanupAfterRender = false;
        this.pendingCleanup = false;
        this._intentStates = new Map();
        this._annotationPromises = new Map();
        this.destroyed = false;
    }


    get pageNumber() {
        return this._pageIndex + 1;
    }


    get rotate() {
        return this._pageInfo.rotate;
    }


    get ref() {
        return this._pageInfo.ref;
    }


    get userUnit() {
        return this._pageInfo.userUnit;
    }


    get view() {
        return this._pageInfo.view;
    }

    getViewport({
                    scale,
                    rotation = this.rotate,
                    offsetX = 0,
                    offsetY = 0,
                    dontFlip = false,
                } = {}) {
        return new PageViewport({
            viewBox: this.view,
            scale,
            rotation,
            offsetX,
            offsetY,
            dontFlip,
        });
    }

    getAnnotations({ intent = "display" } = {}) {
        const intentArgs = this._transport.getRenderingIntent(intent);

        let promise = this._annotationPromises.get(intentArgs.cacheKey);
        if (!promise) {
            promise = this._transport.getAnnotations(
                this._pageIndex,
                intentArgs.renderingIntent
            );
            this._annotationPromises.set(intentArgs.cacheKey, promise);
        }
        return promise;
    }


    getJSActions() {
        return (this._jsActionsPromise ||= this._transport.getPageJSActions(
            this._pageIndex
        ));
    }


    async getXfa() {
        return this._transport._htmlForXfa?.children[this._pageIndex] || null;
    }

    render({
               canvasContext,
               viewport,
               intent = "display",
               annotationMode = AnnotationMode.ENABLE,
               transform = null,
               imageLayer = null,
               canvasFactory = null,
               background = null,
               optionalContentConfigPromise = null,
               annotationCanvasMap = null,
           }) {

        if (this._stats) {
            this._stats.time("Overall");
        }

        const intentArgs = this._transport.getRenderingIntent(
            intent,
            annotationMode
        );
        // If there was a pending destroy, cancel it so no cleanup happens during
        // this call to render.
        this.pendingCleanup = false;

        if (!optionalContentConfigPromise) {
            optionalContentConfigPromise = this._transport.getOptionalContentConfig();
        }

        let intentState = this._intentStates.get(intentArgs.cacheKey);
        if (!intentState) {
            intentState = Object.create(null);
            this._intentStates.set(intentArgs.cacheKey, intentState);
        }

        // Ensure that a pending `streamReader` cancel timeout is always aborted.
        if (intentState.streamReaderCancelTimeout) {
            clearTimeout(intentState.streamReaderCancelTimeout);
            intentState.streamReaderCancelTimeout = null;
        }

        //TODO sreni
        const canvasFactoryInstance =
            canvasFactory ||
            new DefaultCanvasFactory({ ownerDocument: this._ownerDocument });
        //const canvasFactoryInstance = null;
        const intentPrint = !!(
            intentArgs.renderingIntent & RenderingIntentFlag.PRINT
        );

        // If there's no displayReadyCapability yet, then the operatorList
        // was never requested before. Make the request and create the promise.
        if (!intentState.displayReadyCapability) {
            intentState.displayReadyCapability = createPromiseCapability();
            intentState.operatorList = {
                fnArray: [],
                argsArray: [],
                lastChunk: false,
            };

            if (this._stats) {
                this._stats.time("Page Request");
            }
            this._pumpOperatorList(intentArgs);
        }

        const complete = error => {
            intentState.renderTasks.delete(internalRenderTask);

            // Attempt to reduce memory usage during *printing*, by always running
            // cleanup once rendering has finished (regardless of cleanupAfterRender).
            if (this.cleanupAfterRender || intentPrint) {
                this.pendingCleanup = true;
            }
            this._tryCleanup();

            if (error) {
                internalRenderTask.capability.reject(error);

                this._abortOperatorList({
                    intentState,
                    reason: error instanceof Error ? error : new Error(error),
                });
            } else {
                internalRenderTask.capability.resolve();
            }
            if (this._stats) {
                this._stats.timeEnd("Rendering");
                this._stats.timeEnd("Overall");
            }
        };

        const internalRenderTask = new InternalRenderTask({
            callback: complete,
            // Only include the required properties, and *not* the entire object.
            params: {
                canvasContext,
                viewport,
                transform,
                imageLayer,
                background,
            },
            objs: this.objs,
            commonObjs: this.commonObjs,
            annotationCanvasMap,
            operatorList: intentState.operatorList,
            pageIndex: this._pageIndex,
            canvasFactory: canvasFactoryInstance,
            useRequestAnimationFrame: !intentPrint,
            pdfBug: this._pdfBug,
        });

        (intentState.renderTasks ||= new Set()).add(internalRenderTask);
        const renderTask = internalRenderTask.task;

        Promise.all([
            intentState.displayReadyCapability.promise,
            optionalContentConfigPromise,
        ])
            .then(([transparency, optionalContentConfig]) => {
                if (this.pendingCleanup) {
                    complete();
                    return;
                }
                if (this._stats) {
                    this._stats.time("Rendering");
                }
                internalRenderTask.initializeGraphics({
                    transparency,
                    optionalContentConfig,
                });
                internalRenderTask.operatorListChanged();
            })
            .catch(complete);

        return renderTask;
    }

    getOperatorList({
                        intent = "display",
                        annotationMode = AnnotationMode.ENABLE,
                    } = {}) {
        function operatorListChanged() {
            if (intentState.operatorList.lastChunk) {
                intentState.opListReadCapability.resolve(intentState.operatorList);

                intentState.renderTasks.delete(opListTask);
            }
        }

        const intentArgs = this._transport.getRenderingIntent(
            intent,
            annotationMode,
            true
        );
        let intentState = this._intentStates.get(intentArgs.cacheKey);
        if (!intentState) {
            intentState = Object.create(null);
            this._intentStates.set(intentArgs.cacheKey, intentState);
        }
        let opListTask;

        if (!intentState.opListReadCapability) {
            opListTask = Object.create(null);
            opListTask.operatorListChanged = operatorListChanged;
            intentState.opListReadCapability = createPromiseCapability();
            (intentState.renderTasks ||= new Set()).add(opListTask);
            intentState.operatorList = {
                fnArray: [],
                argsArray: [],
                lastChunk: false,
            };

            if (this._stats) {
                this._stats.time("Page Request");
            }
            this._pumpOperatorList(intentArgs);
        }
        return intentState.opListReadCapability.promise;
    }


    streamTextContent({
                          disableCombineTextItems = false,
                          includeMarkedContent = false,
                      } = {}) {
        const TEXT_CONTENT_CHUNK_SIZE = 100;

        return this._transport.messageHandler.sendWithStream(
            "GetTextContent",
            {
                pageIndex: this._pageIndex,
                combineTextItems: disableCombineTextItems !== true,
                includeMarkedContent: includeMarkedContent === true,
            },
            {
                highWaterMark: TEXT_CONTENT_CHUNK_SIZE,
                size(textContent) {
                    return textContent.items.length;
                },
            }
        );
    }

    getTextContent(params = {}) {
        if (this._transport._htmlForXfa) {
            // TODO: We need to revisit this once the XFA foreground patch lands and
            // only do this for non-foreground XFA.
            return this.getXfa().then(xfa => {
                return XfaText.textContent(xfa);
            });
        }
        const readableStream = this.streamTextContent(params);

        return new Promise(function (resolve, reject) {
            function pump() {
                reader.read().then(function ({ value, done }) {
                    if (done) {
                        resolve(textContent);
                        return;
                    }
                    Object.assign(textContent.styles, value.styles);
                    textContent.items.push(...value.items);
                    pump();
                }, reject);
            }

            const reader = readableStream.getReader();
            const textContent = {
                items: [],
                styles: Object.create(null),
            };
            pump();
        });
    }

    getStructTree() {
        return (this._structTreePromise ||= this._transport.getStructTree(
            this._pageIndex
        ));
    }


    _destroy() {
        this.destroyed = true;

        const waitOn = [];
        for (const intentState of this._intentStates.values()) {
            this._abortOperatorList({
                intentState,
                reason: new Error("Page was destroyed."),
                force: true,
            });

            if (intentState.opListReadCapability) {
                // Avoid errors below, since the renderTasks are just stubs.
                continue;
            }
            for (const internalRenderTask of intentState.renderTasks) {
                waitOn.push(internalRenderTask.completed);
                internalRenderTask.cancel();
            }
        }
        this.objs.clear();
        this._annotationPromises.clear();
        this._jsActionsPromise = null;
        this._structTreePromise = null;
        this.pendingCleanup = false;
        return Promise.all(waitOn);
    }


    cleanup(resetStats = false) {
        this.pendingCleanup = true;
        return this._tryCleanup(resetStats);
    }


    _tryCleanup(resetStats = false) {
        if (!this.pendingCleanup) {
            return false;
        }
        for (const { renderTasks, operatorList } of this._intentStates.values()) {
            if (renderTasks.size > 0 || !operatorList.lastChunk) {
                return false;
            }
        }

        this._intentStates.clear();
        this.objs.clear();
        this._annotationPromises.clear();
        this._jsActionsPromise = null;
        this._structTreePromise = null;
        if (resetStats && this._stats) {
            //TODO sreeni
            //this._stats = new StatTimer();
        }
        this.pendingCleanup = false;
        return true;
    }


    _startRenderPage(transparency, cacheKey) {
        const intentState = this._intentStates.get(cacheKey);
        if (!intentState) {
            return; // Rendering was cancelled.
        }
        if (this._stats) {
            this._stats.timeEnd("Page Request");
        }
        // TODO Refactor RenderPageRequest to separate rendering
        // and operator list logic
        if (intentState.displayReadyCapability) {
            intentState.displayReadyCapability.resolve(transparency);
        }
    }


    _renderPageChunk(operatorListChunk, intentState) {
        // Add the new chunk to the current operator list.
        for (let i = 0, ii = operatorListChunk.length; i < ii; i++) {
            intentState.operatorList.fnArray.push(operatorListChunk.fnArray[i]);
            intentState.operatorList.argsArray.push(operatorListChunk.argsArray[i]);
        }
        intentState.operatorList.lastChunk = operatorListChunk.lastChunk;

        // Notify all the rendering tasks there are more operators to be consumed.
        for (const internalRenderTask of intentState.renderTasks) {
            internalRenderTask.operatorListChanged();
        }

        if (operatorListChunk.lastChunk) {
            this._tryCleanup();
        }
    }


    _pumpOperatorList({ renderingIntent, cacheKey }) {
        const readableStream = this._transport.messageHandler.sendWithStream(
            "GetOperatorList",
            {
                pageIndex: this._pageIndex,
                intent: renderingIntent,
                cacheKey,
                annotationStorage:
                    renderingIntent & RenderingIntentFlag.ANNOTATIONS_STORAGE
                    ? this._transport.annotationStorage.serializable
                    : null,
            }
        );
        const reader = readableStream.getReader();

        const intentState = this._intentStates.get(cacheKey);
        intentState.streamReader = reader;

        const pump = () => {
            reader.read().then(
                (value) => {
                    if(value.done){
                        intentState.streamReader = null;
                        return;
                    }

                    if (this._transport.destroyed) {
                        return; // Ignore any pending requests if the worker was terminated.
                    }
                    this._renderPageChunk(value.data, intentState);
                    pump();
                }
            );

        };
        pump();
    }


    _abortOperatorList({ intentState, reason, force = false }) {

        if (!intentState.streamReader) {
            return;
        }
        if (!force) {
            // Ensure that an Error occurring in *only* one `InternalRenderTask`, e.g.
            // multiple render() calls on the same canvas, won't break all rendering.
            if (intentState.renderTasks.size > 0) {
                return;
            }
            // Don't immediately abort parsing on the worker-thread when rendering is
            // cancelled, since that will unnecessarily delay re-rendering when (for
            // partially parsed pages) e.g. zooming/rotation occurs in the viewer.
            //TODO sreeni

        }
        intentState.streamReader
            .cancel(new AbortException(reason.message))
            .catch(() => {
                // Avoid "Uncaught promise" messages in the console.
            });
        intentState.streamReader = null;

        if (this._transport.destroyed) {
            return; // Ignore any pending requests if the worker was terminated.
        }
        // Remove the current `intentState`, since a cancelled `getOperatorList`
        // call on the worker-thread cannot be re-started...
        for (const [curCacheKey, curIntentState] of this._intentStates) {
            if (curIntentState === intentState) {
                this._intentStates.delete(curCacheKey);
                break;
            }
        }
        // ... and force clean-up to ensure that any old state is always removed.
        this.cleanup();
    }


    get stats() {
        return this._stats;
    }
}


class RenderTask {
    constructor(internalRenderTask) {
        this._internalRenderTask = internalRenderTask;


        this.onContinue = null;
    }


    get promise() {
        return this._internalRenderTask.capability.promise;
    }


    cancel() {
        this._internalRenderTask.cancel();
    }
}
class InternalRenderTask {
    static get canvasInUse() {
        return shadow(this, "canvasInUse", new WeakSet());
    }

    constructor({
                    callback,
                    params,
                    objs,
                    commonObjs,
                    annotationCanvasMap,
                    operatorList,
                    pageIndex,
                    canvasFactory,
                    useRequestAnimationFrame = false,
                    pdfBug = false,
                }) {
        this.callback = callback;
        this.params = params;
        this.objs = objs;
        this.commonObjs = commonObjs;
        this.annotationCanvasMap = annotationCanvasMap;
        this.operatorListIdx = null;
        this.operatorList = operatorList;
        this._pageIndex = pageIndex;
        this.canvasFactory = canvasFactory;
        this._pdfBug = pdfBug;

        this.running = false;
        this.graphicsReadyCallback = null;
        this.graphicsReady = false;
        this._useRequestAnimationFrame = false;
        this.cancelled = false;
        this.capability = createPromiseCapability();
        this.task = new RenderTask(this);
        // caching this-bound methods
        this._cancelBound = this.cancel.bind(this);
        this._continueBound = this._continue.bind(this);
        this._scheduleNextBound = this._scheduleNext.bind(this);
        this._nextBound = this._next.bind(this);
        this._canvas = params.canvasContext.canvas;
    }

    get completed() {
        return this.capability.promise.catch(function () {
            // Ignoring errors, since we only want to know when rendering is
            // no longer pending.
        });
    }

    initializeGraphics({ transparency = false, optionalContentConfig }) {
        if (this.cancelled) {
            return;
        }
        if (this._canvas) {
            if (InternalRenderTask.canvasInUse.has(this._canvas)) {
                throw new Error(
                    "Cannot use the same canvas during multiple render() operations. " +
                    "Use different canvas or ensure previous operations were " +
                    "cancelled or completed."
                );
            }
            InternalRenderTask.canvasInUse.add(this._canvas);
        }

        if (this._pdfBug && globalThis.StepperManager?.enabled) {
            this.stepper = globalThis.StepperManager.create(this._pageIndex);
            this.stepper.init(this.operatorList);
            this.stepper.nextBreakPoint = this.stepper.getNextBreakPoint();
        }
        const { canvasContext, viewport, transform, imageLayer, background } =
            this.params;

        this.gfx = new CanvasGraphics(
            canvasContext,
            this.commonObjs,
            this.objs,
            this.canvasFactory,
            imageLayer,
            optionalContentConfig,
            this.annotationCanvasMap
        );
        this.gfx.beginDrawing({
            transform,
            viewport,
            transparency,
            background,
        });
        this.operatorListIdx = 0;
        this.graphicsReady = true;
        if (this.graphicsReadyCallback) {
            this.graphicsReadyCallback();
        }
    }

    cancel(error = null) {
        this.running = false;
        this.cancelled = true;
        if (this.gfx) {
            this.gfx.endDrawing();
        }
        if (this._canvas) {
            InternalRenderTask.canvasInUse.delete(this._canvas);
        }
        this.callback(
            error ||
            new RenderingCancelledException(
                `Rendering cancelled, page ${this._pageIndex + 1}`,
                "canvas"
            )
        );
    }

    operatorListChanged() {
        if (!this.graphicsReady) {
            if (!this.graphicsReadyCallback) {
                this.graphicsReadyCallback = this._continueBound;
            }
            return;
        }

        if (this.stepper) {
            this.stepper.updateOperatorList(this.operatorList);
        }

        if (this.running) {
            return;
        }
        this._continue();
    }

    _continue() {
        this.running = true;
        if (this.cancelled) {
            return;
        }
        if (this.task.onContinue) {
            this.task.onContinue(this._scheduleNextBound);
        } else {
            this._scheduleNext();
        }
    }

    _scheduleNext() {
            Promise.resolve().then(this._nextBound).catch(this._cancelBound);
    }

    async _next() {
        if (this.cancelled) {
            return;
        }
        this.operatorListIdx = this.gfx.executeOperatorList(
            this.operatorList,
            this.operatorListIdx,
            this._continueBound,
            this.stepper
        );
        if (this.operatorListIdx === this.operatorList.argsArray.length) {
            this.running = false;
            if (this.operatorList.lastChunk) {
                this.gfx.endDrawing();
                if (this._canvas) {
                    InternalRenderTask.canvasInUse.delete(this._canvas);
                }
                this.callback();
            }
        }
    }
}

export {PDFWorker, getDocument,PDFWorkerUtil};