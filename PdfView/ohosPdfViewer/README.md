#jsPDF

##简介

pdf用于读取和写入pdf文件，支持图片转pdf文件，方便使用。

## 下载安装

````
npm install @ohos/jspdf --save
````
参考安装教程 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md)
## 接口说明
#### 1、读取pdf。

|方法名|入参|接口描述|
|:---:|:---:|:---:|
|getDocument|src|加载PDF并与PDF交互的主要入口|
|setPDFNetworkStreamFactory|pdfNetworkStreamFactory|返回一个解析为{IPDFStream}实例的回调|
|annotationStorage| |存储表单中的注解数据|
|getPageLayout| |返回一个包含页面布局名称的字符串的promise|


## 兼容性

```
要求DevEco studio 3.2 Beta及以上 ，SDK版本号为Version 7以上。
```
## 目录结构

````
|---- jsPDF
|     |---- entry  # 示例代码文件夹
|     |---- pdfjs   # gson库文件夹
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法      
````