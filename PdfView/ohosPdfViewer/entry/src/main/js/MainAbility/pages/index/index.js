/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {getDocument,jsPDF} from 'pdfjs';
import http from '@ohos.net.http';
import resourceManager from '@ohos.resourceManager';

export default {
    data: {
        title: "",
        cclass:"rc"
    },
    onInit() {
        this.title = this.$t('strings.world');
    },
    onclick: function () {
        this.getDocumentFromResource("rawfile/invoice_02.pdf");
        /*const el =this.$refs.canvas1;
        const ctx = el.getContext('2d');
        ctx.strokeStyle = '#0000ff';
        ctx.moveTo(140, 10);
        ctx.lineTo(140, 160);
        ctx.stroke();

        ctx.font = '18 sans-serif';

        // Show the textAlign values.
        ctx.textAlign = 'start';
        ctx.fillText('textAlign=start', 140, 60);
        ctx.textAlign = 'end';
        ctx.fillText('textAlign=end', 140, 80);
        ctx.textAlign = 'left';
        ctx.fillText('textAlign=left', 140, 100);
        ctx.textAlign = 'center';
        ctx.fillText('textAlign=center',140, 120);
        ctx.textAlign = 'right';
        ctx.fillText('textAlign=right',140, 140);*/
    },
    onStyleChangeClick(){
        this.cclass = "gc";
    },
    stringToBytes(str) {
        const length = str.length;
        const bytes = new Uint8Array(length);
        for (let i = 0; i < length; ++i) {
            bytes[i] = str.charCodeAt(i) & 0xff;
        }
        return bytes;
    },
    getDocumentFromResource(pdfFilePath){
        var that=this;
        resourceManager.getResourceManager((error, mgr) => {
            if (error != null) {
                console.log("error occurs" + error);
                return;
            }
            mgr.getRawFile(pdfFilePath,(error, value) => {
                if (error != null) {
                    console.log(error);
                } else {
                    that.getPDFDocument(value);
                }
            })
        });
    },
    getDocumentNetwork(){
        let httpRequest = http.createHttp();
        var that=this;
        httpRequest.request("http://www.africau.edu/images/default/sample.pdf",
            {
                method: 'GET',
                readTimeout: 60000,
                connectTimeout: 60000
            },(err, data) => {
                if (!err) {
                    //that.getDocument();
                    console.info('Result:' + data.result);
                    console.info('code:' + data.responseCode);
                    console.info('header:' + data.header);
                } else {
                    console.info('error:' + err.data);
                }
            });
    },
    getPDFDocument(pdfFile){
        var loadingTask = getDocument(pdfFile);
        var that=this;

        loadingTask.promise.then(function(pdf) {
            that.pdf = pdf;
            console.log('sreeni : PDF loaded');
            that.pageNumber = 1;
            pdf.getPage(that.pageNumber).then(function(page) {
                console.log('sreeni : PDF page loaded');
                var el = that.$refs.canvas1;
                that.ctx = el.getContext('2d', {
                    antialias: true
                });
                that.ctx.canvas = el;
                that.ctx.canvas.width = that.ctx.canvas.getBoundingClientRect().width;
                that.ctx.canvas.height = that.ctx.canvas.getBoundingClientRect().height;
                var scale = that.ctx.canvas.width/page.getViewport({scale: 1}).width;//page PDFPageProxy
                var viewport = page.getViewport({scale: scale});
                var renderContext = {
                    canvasContext: that.ctx,
                    viewport: viewport
                };
                console.log('sreeni : PDF render');
                var renderTask = page.render(renderContext);
                renderTask.promise.then(function () {
                    console.log('Page rendered');
                });
                console.log('sreeni : PDF end');
            });
        });
        console.log("=======================================PDF-E======================================");
    },
    onNextPageClick(){
        var that=this;
        this.pageNumber+=1;
        if(this.pageNumber>this.pdf.numPages){
            this.pageNumber = 1;
        }
        this.pdf.getPage(this.pageNumber).then(function(page) {
            console.log('sreeni : PDF page loaded');
            var scale = that.ctx.canvas.width/page.getViewport({scale: 1}).width;//page PDFPageProxy
            var viewport = page.getViewport({scale: scale});
            var renderContext = {
                canvasContext: that.ctx,
                viewport: viewport
            };
            console.log('sreeni : PDF render');
            var renderTask = page.render(renderContext);
            renderTask.promise.then(function () {
                console.log('Page rendered');
            });
            console.log('sreeni : PDF end');
        });
    },
    onImageToPDF(){
        this.getImageFromResource("rawfile/1.jpg");
    },

    bin2String(array) {
        var result = "";
        for (var i = 0; i < array.length; i++) {
            result += String.fromCharCode(parseInt(array[i], 2));
        }
        return result;
    },

    imageToPDF(imageData){
        var jsPdf = new jsPDF({"orientation":"p","format":"a4"});
        jsPdf.addImage(imageData, 0, 0, 150, 150);
        //PDF content
        let content = jsPdf.buildDocument();
        //let bContent = jsPdf.lbtoa(content);
        //console.log(bContent);
    },

    getImageFromResource(imageFilePath){
        var that=this;
        resourceManager.getResourceManager((error, mgr) => {
            if (error != null) {
                console.log("error occurs" + error);
                return;
            }
            mgr.getRawFile(imageFilePath,(error, value) => {
                if (error != null) {
                    console.log(error);
                } else {
                    that.imageToPDF(value);
                }
            })
        });
    },
}