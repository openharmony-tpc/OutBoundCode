/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {RequestOption} from '@ohos/imageknife'
import {AllCacheInfo,IAllCacheInfoCallback} from '@ohos/imageknife'
import {ImageKnifeComponent} from '@ohos/imageknife'
import {TransformType} from '@ohos/imageknife'
import {PixelMapPack} from '@ohos/imageknife'
import {ImageKnifeOption} from '@ohos/imageknife'
import {RotateImageTransformation} from '@ohos/imageknife'

@Entry
@Component
struct TestAllCacheInfoPage {
  @State nativePixelMap: PixelMapPack = new PixelMapPack();
  @State networkPixelMap: PixelMapPack = new PixelMapPack();
  allCacheInfoCallback1 =(allCacheInfo)=>{
    let info = allCacheInfo as AllCacheInfo;
    console.log("AllCacheInfoCallback imageknifecomponent1 memory ="+JSON.stringify(info.memoryCacheInfo))
    console.log("AllCacheInfoCallback imageknifecomponent1 resource ="+JSON.stringify(info.resourceCacheInfo))
    console.log("AllCacheInfoCallback imageknifecomponent1 data ="+JSON.stringify(info.dataCacheInfo))
    this.cacheinfo3 = "memory="+JSON.stringify(info.memoryCacheInfo)+
    "\n resource ="+JSON.stringify(info.resourceCacheInfo)+
    "\n data ="+JSON.stringify(info.dataCacheInfo)
  }
  allCacheInfoCallback2 =(allCacheInfo)=>{
    let info = allCacheInfo as AllCacheInfo;
    console.log("AllCacheInfoCallback imageknifecomponent2 memory ="+JSON.stringify(info.memoryCacheInfo))
    console.log("AllCacheInfoCallback imageknifecomponent2 resource ="+JSON.stringify(info.resourceCacheInfo))
    console.log("AllCacheInfoCallback imageknifecomponent2 data ="+JSON.stringify(info.dataCacheInfo))
    this.cacheinfo4 = "memory="+JSON.stringify(info.memoryCacheInfo)+
    "\n resource ="+JSON.stringify(info.resourceCacheInfo)+
    "\n data ="+JSON.stringify(info.dataCacheInfo)
  }
  @State imageKnifeOption1: ImageKnifeOption =
    {
      loadSrc: $r('app.media.pngSample'),
      size: { width: 300, height: 300 },
      placeholderSrc: $r('app.media.icon_loading'),
      errorholderSrc: $r('app.media.icon_failed'),
      margin:{top:15},
      transform: {
        transformType:TransformType.RotateImageTransformation,
        rotateImage:180
      },
      allCacheInfoCallback:this.allCacheInfoCallback1
    };
  @State imageKnifeOption2: ImageKnifeOption =
    {
      loadSrc: "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132",
      size: { width: 300, height: 300 },
      placeholderSrc: $r('app.media.icon_loading'),
      errorholderSrc: $r('app.media.icon_failed'),
      margin:{top:15},
      transform: {
        transformType:TransformType.RotateImageTransformation,
        rotateImage:180
      },
      allCacheInfoCallback:this.allCacheInfoCallback2
    };
  imageKnifeComponentAngle:number = 90;

  @State cacheinfo1:string = "观察本地资源获取缓存信息输出"
  @State cacheinfo2:string = "观察网络资源获取缓存信息输出"
  @State cacheinfo3:string = "观察ImageKnifeComponent本地缓存输出"
  @State cacheinfo4:string = "观察ImageKnifeComponent网络缓存输出"

  build() {
    Scroll() {
      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button("加载本地资源获取缓存信息")
          .width(300).height(25)
          .onClick(() => {
            this.testAllCacheInfoNative();
          }).margin({ top: 15 })
        Scroll() {
          Text(this.cacheinfo1).fontSize(15)
        }.width(300).height(200)
        Image(this.nativePixelMap.pixelMap)
          .width(300)
          .height(300)
          .objectFit(ImageFit.Contain)
          .backgroundColor(Color.Green)
          .margin({ top: 15 })
        Button("加载网络资源获取缓存信息").width(300).height(25)
          .onClick(() => {
            this.testAllCacheInfoNetwork();
          }).margin({ top: 15 })
        Scroll() {
          Text(this.cacheinfo2).fontSize(15)
        }.width(300).height(200)
        Image(this.networkPixelMap.pixelMap)
          .width(300)
          .height(300)
          .objectFit(ImageFit.Contain)
          .backgroundColor(Color.Green)
          .margin({ top: 15 })

        Button("ImageKnifeComponent加载本地资源获取缓存信息").width(300).height(25)
          .onClick(() => {
            this.imageKnifeComponentAngle = this.imageKnifeComponentAngle + 45;
            this.imageKnifeOption1 =
            {
              loadSrc: $r('app.media.pngSample'),
              size: { width: 300, height: 300 },
              placeholderSrc: $r('app.media.icon_loading'),
              errorholderSrc: $r('app.media.icon_failed'),
              margin: { top: 15 },
              transform: {
                transformType:TransformType.RotateImageTransformation,
                rotateImage:this.imageKnifeComponentAngle
              },
              allCacheInfoCallback:this.allCacheInfoCallback1
            };
          }).margin({ top: 15 })
        Scroll() {
          Text(this.cacheinfo3).fontSize(15)
        }.width(300).height(200)
        ImageKnifeComponent({ imageKnifeOption: $imageKnifeOption1 })

        Button("ImageKnifeComponent加载网络资源获取缓存信息").width(300).height(25)
          .onClick(() => {
            this.imageKnifeComponentAngle = this.imageKnifeComponentAngle + 45;
            this.imageKnifeOption2 =
            {
              loadSrc: "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132",
              size: { width: 300, height: 300 },
              placeholderSrc: $r('app.media.icon_loading'),
              errorholderSrc: $r('app.media.icon_failed'),
              margin: { top: 15 },
              transform: {
                transformType:TransformType.RotateImageTransformation,
                rotateImage:this.imageKnifeComponentAngle
              },
              allCacheInfoCallback:this.allCacheInfoCallback2
            };
          })
        Scroll() {
          Text(this.cacheinfo4).fontSize(15)
        }.width(300).height(200)
        ImageKnifeComponent({ imageKnifeOption: $imageKnifeOption2 })

      }
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {

  }

  private testAllCacheInfoNative() {
    let imageKnifeOption = new RequestOption();
    imageKnifeOption.load($r('app.media.pngSample'))
      .setImageViewSize({width:300,height:300})
      .addListener((err, data) => {
        let pack = new PixelMapPack();
        pack.pixelMap = data.imageKnifeValue as PixelMap;;
        this.nativePixelMap = pack;
        return false;
    }).addAllCacheInfoCallback((allCacheInfo)=>{
        let info = allCacheInfo as AllCacheInfo;
        console.log("AllCacheInfoCallback memory ="+JSON.stringify(info.memoryCacheInfo))
        console.log("AllCacheInfoCallback resource ="+JSON.stringify(info.resourceCacheInfo))
        console.log("AllCacheInfoCallback data ="+JSON.stringify(info.dataCacheInfo))
      this.cacheinfo1 = "memory="+JSON.stringify(info.memoryCacheInfo)+
      "\n resource ="+JSON.stringify(info.resourceCacheInfo)+
      "\n data ="+JSON.stringify(info.dataCacheInfo)
    })
    ImageKnife.call(imageKnifeOption);
  }

  private testAllCacheInfoNetwork() {
    let imageKnifeOption2 = new RequestOption();
    imageKnifeOption2.load("https://hbimg.huabanimg.com/0ef60041445edcfd6b38d20e19024b2cd9281dcc3525a4-Vy8fYO_fw658/format/webp")
      .addListener((err, data) => {
        let pack = new PixelMapPack();
        pack.pixelMap = data.imageKnifeValue as PixelMap;
        this.networkPixelMap = pack;
        console.log("imageknife2 图片2 赋值！")
        return false;
    }).addAllCacheInfoCallback((allCacheInfo)=>{
      let info = allCacheInfo as AllCacheInfo;
      console.log("AllCacheInfoCallback memory ="+JSON.stringify(info.memoryCacheInfo))
      console.log("AllCacheInfoCallback resource ="+JSON.stringify(info.resourceCacheInfo))
      console.log("AllCacheInfoCallback data ="+JSON.stringify(info.dataCacheInfo))
      this.cacheinfo2 = "memory="+JSON.stringify(info.memoryCacheInfo)+
      "\n resource ="+JSON.stringify(info.resourceCacheInfo)+
      "\n data ="+JSON.stringify(info.dataCacheInfo)
    })
      .setImageViewSize({width:300,height:300})
      .rotateImage(180)
    ImageKnife.call(imageKnifeOption2);
  }
}


var ImageKnife = globalThis.ImageKnife