/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {AUTOMATIC} from "../cache/diskstrategy/enum/AUTOMATIC"
import {DiskStrategy} from "../cache/diskstrategy/DiskStrategy"
import {BaseTransform} from "../imageknife/transform/BaseTransform"
import {TransformType} from "../imageknife/transform/TransformType"
import {CropType} from "../imageknife/transform/CropTransformation"
import {AllCacheInfo, IAllCacheInfoCallback} from "../imageknife/interface/IAllCacheInfoCallback"

export class ImageKnifeOption {

  // 主图资源
  loadSrc: string | PixelMap | Resource;

  // 磁盘缓存策略
  strategy?: DiskStrategy = new AUTOMATIC();

  // gif加载展示一帧
  dontAnimateFlag? = false;

  // 占位图
  placeholderSrc?: PixelMap | Resource;

  // 失败占位图
  errorholderSrc?: PixelMap | Resource;

  // 重试占位图
  retryholderSrc?: Resource;

  // 缩略图,范围(0,1)
  thumbSizeMultiplier?: number;

  // 进度条
  displayProgress?: boolean;

  // 进度条回调
  displayProgressListener?;

  // 重试图层
  retryLoad?: boolean;

  // 动画时长
  animateDuration?: number = 500;

  // 仅使用缓存加载数据
  onlyRetrieveFromCache?: boolean = false;

  // 是否开启第一级内存缓存
  isCacheable?: boolean = true;

  // 变换相关
  transform?:{
    transformType:number,
    blur?:number,
    roundedCorners?:{ top_left: number, top_right: number, bottom_left: number, bottom_right: number }
    cropCircleWithBorder?:{border:number, obj:{ r_color: number, g_color: number, b_color: number }}
    crop?:{width: number, height: number, cropType: CropType}
    brightnessFilter?:number,
    contrastFilter?:number,
    pixelationFilter?:number,
    swirlFilter?:number,
    mask?:Resource,
    rotateImage?:number
  }

  transformation?:BaseTransform<PixelMap>;

  transformations?:Array<BaseTransform<PixelMap>>;

  // 输出缓存相关内容和信息
  allCacheInfoCallback?: IAllCacheInfoCallback;
  size: {
    width: number,
    height: number
  };
  imageFit?: ImageFit;
  backgroundColor?: Color | number | string | Resource;
  margin?: {
    top?: number | string | Resource,
    right?: number | string | Resource,
    bottom?: number | string | Resource,
    left?: number | string | Resource
  } | number | string | Resource



  constructor() {

  }
}