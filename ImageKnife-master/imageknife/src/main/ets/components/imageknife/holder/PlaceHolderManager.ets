/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {RequestOption} from "../../imageknife/RequestOption"
import {ResourceTypeEts} from "../../imageknife/constants/ResourceTypeEts"
import {Base64} from "../../cache/Base64"
import {FileTypeUtil} from "../../imageknife/utils/FileTypeUtil"
import {ImageKnifeData} from "../ImageKnifeData"
import {ParseImageUtil} from '../utils/ParseImageUtil'
import {ParseResClient} from '../resourcemanage/ParseResClient'
import resourceManager from '@ohos.resourceManager';
import image from "@ohos.multimedia.image"

export class PlaceHolderManager {
  private options: RequestOption;

  constructor(option: RequestOption) {
    this.options = option;
  }

  static execute(option: RequestOption) {
    let manager = new PlaceHolderManager(option);
    return new Promise(manager.process.bind(manager))
    .then(option.placeholderOnComplete.bind(option)).catch(option.placeholderOnError.bind(option));
  }

  process(onComplete, onError) {
    this.displayPlaceholder(onComplete, onError);
  }

  private displayPlaceholder(onComplete, onError) {
    console.log("displayPlaceholder")
    if ((typeof (this.options.placeholderSrc as image.PixelMap).isEditable) == 'boolean') {
      let imageKnifeData = new ImageKnifeData();
      imageKnifeData.imageKnifeType = ImageKnifeData.PIXELMAP
      imageKnifeData.imageKnifeValue = this.options.placeholderSrc as PixelMap
      onComplete(imageKnifeData);
    } else if (typeof this.options.placeholderSrc == 'string') {

    } else {
      let res = this.options.placeholderSrc as Resource;
      if (typeof res.id != 'undefined' && typeof res.id != 'undefined') {
        let resourceFetch = new ParseResClient();
        let suc = (arraybuffer) => {
          let fileTypeUtil = new FileTypeUtil();
          let typeValue = fileTypeUtil.getFileType(arraybuffer);
          if ("gif" == typeValue || "svg" == typeValue) {
            let imageKnifeData = this.createImageKnifeData("Resource", this.options.placeholderSrc as Resource, typeValue);
            onComplete(imageKnifeData);
          } else {
            let parseImageUtils = new ParseImageUtil();
            let success = (value: PixelMap) => {
              let imageKnifeData = this.createImageKnifeData('PixelMap', value, typeValue);
              onComplete(imageKnifeData);
            }
            parseImageUtils.parseImage(arraybuffer, success, onError)
          }
        }
        resourceFetch.loadResource(res, suc, onError)
      } else {
        onError("PlaceHolderManager 输入参数有问题!")
      }
    }
  }
    private createImageKnifeData(imageKnifeType:string, imageKnifeValue:PixelMap|string|Resource, imageKnifeSourceType:string):ImageKnifeData{
    let imageKnifeData = new ImageKnifeData();
    imageKnifeData.imageKnifeType = imageKnifeType;
    imageKnifeData.imageKnifeValue = imageKnifeValue;
    imageKnifeData.imageKnifeSourceType = imageKnifeSourceType;
    return imageKnifeData;
  }
}