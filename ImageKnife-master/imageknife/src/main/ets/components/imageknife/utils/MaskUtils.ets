/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {PixelEntry} from "../entry/PixelEntry"
import {AsyncTransform} from "../transform/AsyncTransform"
import {ColorUtils} from "./ColorUtils"
import {CalculatePixelUtils} from "./CalculatePixelUtils"

export class MaskUtils {
  static async mask(bitmap: any, maskBitmap: any, func?: AsyncTransform<PixelMap>) {
    let imageInfo = await bitmap.getImageInfo();
    let size = {
      width: imageInfo.size.width,
      height: imageInfo.size.height
    }
    if (!size) {
      return;
    }
    let width = size.width;
    let height = size.height;

    let rgbData = CalculatePixelUtils.createInt2DArray(height, width);

    let bufferData = new ArrayBuffer(bitmap.getPixelBytesNumber());
    await bitmap.readPixelsToBuffer(bufferData);
    let dataArray = new Uint8Array(bufferData);

    let ph = 0;
    let pw = 0;
    for (let index = 0; index < dataArray.length; index += 4) {
      const r = dataArray[index];
      const g = dataArray[index+1];
      const b = dataArray[index+2];
      const f = dataArray[index+3];

      let entry = new PixelEntry();
      entry.a = 0;
      entry.b = b;
      entry.g = g;
      entry.r = r;
      entry.f = f;
      entry.pixel = ColorUtils.rgb(entry.r, entry.g, entry.b);
      rgbData[ph][pw] = ColorUtils.rgb(entry.r, entry.g, entry.b);
      if (pw == width - 1) {
        pw = 0;
        ph++;
      } else {
        pw++;
      }
    }


    let imageInfoMask = await maskBitmap.getImageInfo();
    let sizeMask = {
      width: imageInfoMask.size.width,
      height: imageInfoMask.size.height
    }
    if (!sizeMask) {
      return;
    }
    let widthMask = sizeMask.width;
    let heightMask = sizeMask.height;
    let rgbDataMask = CalculatePixelUtils.createInt2DArray(heightMask, widthMask);
    let pixEntry: Array<PixelEntry> = new Array();

    let bufferData_m = new ArrayBuffer(maskBitmap.getPixelBytesNumber());
    await maskBitmap.readPixelsToBuffer(bufferData_m);
    let dataArray_m = new Uint8Array(bufferData_m);

    let ph_m = 0;
    let pw_m = 0;

    for (let index = 0; index < dataArray_m.length; index += 4) {
      const r = dataArray_m[index];
      const g = dataArray_m[index+1];
      const b = dataArray_m[index+2];
      const f = dataArray_m[index+3];

      let entry = new PixelEntry();
      entry.a = 0;
      entry.b = b;
      entry.g = g;
      entry.r = r;
      entry.f = f;
      entry.pixel = ColorUtils.rgb(entry.r, entry.g, entry.b);
      pixEntry.push(entry);
      if (entry.r == 0 && entry.g == 0 && entry.b == 0) {
        rgbDataMask[ph_m][pw_m] = rgbData[ph_m][pw_m];
      } else {
        rgbDataMask[ph_m][pw_m] = ColorUtils.rgb(entry.r, entry.g, entry.b);
      }

      if (pw_m == widthMask - 1) {
        pw_m = 0;
        ph_m++;
      } else {
        pw_m++;
      }
    }


    let bufferNewData = new ArrayBuffer(maskBitmap.getPixelBytesNumber());
    let dataNewArray = new Uint8Array(bufferNewData);
    let index = 0;
    let mh = 0;
    let nw = 0;

    for (let i = 0; i < dataNewArray.length; i += 4) {
      let pixel_1 = rgbDataMask[mh][nw];

      if (nw == widthMask - 1) {
        nw = 0;
        mh++;
      } else {
        nw++;
      }

      let p_r = ColorUtils.red(pixel_1);
      let p_g = ColorUtils.green(pixel_1);
      let p_b = ColorUtils.blue(pixel_1);

      dataNewArray[i] = p_r;
      dataNewArray[i+1] = p_g;
      dataNewArray[i+2] = p_b;
      dataNewArray[i+3] = pixEntry[index].f;
      index++;
    }
    await maskBitmap.writeBufferToPixels(bufferNewData);
    if (func) {
      func("", maskBitmap);
    }
  }
}