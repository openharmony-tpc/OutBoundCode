/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BaseTransform } from "../transform/BaseTransform"
import { AsyncTransform } from "../transform/AsyncTransform"
import { Constants } from "../constants/Constants"
import { RequestOption } from "../../imageknife/RequestOption"
import { TransformUtils } from "../transform/TransformUtils"
import image from "@ohos.multimedia.image"

export class CropCircleTransformation implements BaseTransform<PixelMap> {
  private static TAG: string = "CropCircleTransformation";
  private mCenterX: number = 0;
  private mCenterY: number = 0;
  private mRadius: number = 0;

  getName() {
    return CropCircleTransformation.TAG + ";mCenterX:" + this.mCenterX
    + ";mCenterY:" + this.mCenterY
    + ";mRadius:" + this.mRadius;
  }

  transform(buf: ArrayBuffer, request: RequestOption, func?: AsyncTransform<PixelMap>) {
    if (!buf || buf.byteLength <= 0) {
      console.error(Constants.PROJECT_TAG + CropCircleTransformation.TAG + " buf is empty");
      if (func) {
        func(Constants.PROJECT_TAG + CropCircleTransformation.TAG + " buf is empty", null);
      }
      return;
    }
    var imageSource = image.createImageSource(buf as any);
    var that = this;
    TransformUtils.getPixelMapSize(imageSource, (error, size: {
      width: number,
      height: number
    }) => {
      if (!size) {
        func(error, null)
        return;
      }
      var pixelMapWidth = size.width;
      var pixelMapHeight = size.height;
      var targetWidth = request.size.width;
      var targetHeight = request.size.height;
      if (pixelMapWidth < targetWidth) {
        targetWidth = pixelMapWidth;
      }
      if (pixelMapHeight < targetHeight) {
        targetHeight = pixelMapHeight;
      }
      that.updatePixelMapSize(imageSource, targetWidth, targetHeight, func);
    })
  }

  private updatePixelMapSize(imageSource: any, outWith: number, outHeight: number, func?: AsyncTransform<PixelMap>) {
    var options = {
      editable: true,
      desiredSize: {
        width: outWith,
        height: outHeight
      }
    }
    imageSource.createPixelMap(options)
      .then(p => {
        this.transformCircle(p, func);
      })
      .catch(e => {
        console.error(Constants.PROJECT_TAG + CropCircleTransformation.TAG + " transform e:" + e);
        if (func) {
          func(Constants.PROJECT_TAG + CropCircleTransformation.TAG + "e" + e, null);
        }
      })
  }

  private async transformCircle(data: any, func?: AsyncTransform<PixelMap>) {
    let imageInfo = await data.getImageInfo();
    let size = {
      width: imageInfo.size.width,
      height: imageInfo.size.height
    }

    if (!size) {
      func(new Error("CropCircleTransformation The image size does not exist."), null)
      return;
    }
    var height = size.height;
    var width = size.width;
    this.mRadius = 0;
    if (width > height) {
      this.mRadius = height / 2;
    } else {
      this.mRadius = width / 2;
    }
    this.mCenterX = width / 2;
    this.mCenterY = height / 2;

    let bufferData = new ArrayBuffer(data.getPixelBytesNumber());
    await data.readPixelsToBuffer(bufferData);

    var dataArray = new Uint8Array(bufferData);

    for (var h = 0;h <= height; h++) {
      for (var w = 0;w <= width; w++) {
        if (this.isContainsCircle(w, h)) {
          continue;
        }
        //针对的点
        let index = (h * width + w) * 4;
        dataArray[index] = 0;
        dataArray[index+1] = 0;
        dataArray[index+2] = 0;
        dataArray[index+3] = 0;
      }
    }
    await data.writeBufferToPixels(bufferData);
    if (func) {
      func("", data);
    }
  }

  isContainsCircle(x: number, y: number): boolean {
    var a = Math.pow((this.mCenterX - x), 2);
    var b = Math.pow((this.mCenterY - y), 2);
    var c = Math.sqrt((a + b));
    return c <= this.mRadius;
  }
}