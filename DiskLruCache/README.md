# DiskLruCache

**专门为OpenHarmony打造的一款磁盘缓存库，通过LRU算法进行磁盘数据存取。**

## 简介

本项目基于开源库 [DiskLruCache](https://github.com/JakeWharton/DiskLruCache) 进行OpenHarmony的自研版本：

- 支持自定义存储文件夹名称和路径（默认在当前app私有路径下，如果使用其他路径需要注意权限问题）。
- 支持存储ArrayBuffer数据类型和File文件类型数据。
- 支持存储容量的动态设置。

## 下载安装

```typescript
npm install @ohos/disklrucache --save
```

OpenHarmony npm环境配置等更多内容，参考安装教程 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md))

## 使用说明

##### 步骤1：

在index.ets页面中 import {DiskLruCache} from '@ohos/disklrucache'

##### 步骤2：

在build中声明对象

```typescript
testDiskLruCache:DiskLruCache = undefined
```

在使用之前初始化对象

```typescript
// 使用应用路径创建文件夹名称为TestDiskLruCache，设置磁盘缓存大小为3M(可选参数，默认设置缓存大小为30M)
this.testDiskLruCache = DiskLruCache.create('/data/storage/el2/base/com.example.disklrucache/entry/files/TestDiskLruCache', 3 * 1024 * 1024)
```

##### 步骤3：

在build中添加按钮，将图片文件存入磁盘缓存

同步设置字符串磁盘缓存数据
```typescript
let data: string = "Hello World Simple Example.";
this.testDiskLruCache.set('test', data);
```

同步读取字符串磁盘缓存数据
```typescript
let data: ArrayBuffer = this.testDiskLruCache.get('test');
console.log(String.fromCharCode.apply(null, new Uint8Array(data)));
```

同步设置文件磁盘缓存数据
```typescript
import fileio from '@ohos.fileio';

let path = '/data/storage/el2/base/com.example.disklrucache/entry/files/testFile.txt';
let fd = fileio.openSync(path, 0o2);
let length = fileio.statSync(path).size;
let data = new ArrayBuffer(length);
fileio.readSync(fd, data);       
this.testDiskLruCache.set('testFile', data);
```

同步读取文件磁盘缓存数据
```typescript
let data: ArrayBuffer = this.testDiskLruCache.get('testFile');
```

异步设置字符串磁盘缓存数据和异步获取字符串磁盘缓存数据
```typescript
let value: string = "Hello World Simple Example."
this.testDiskLruCache.setAsync('test', value).then(() => {
    this.testDiskLruCache.getAsync('test').then((data) => {
        console.log(String.fromCharCode.apply(null, new Uint8Array(data)));
    })
}).catch((err) => {
    console.log('err = ' + err)
});
```

异步设置文件磁盘缓存数据和异步获取文件磁盘缓存数据
```typescript
import fileio from '@ohos.fileio';

let path = '/data/storage/el2/base/com.example.disklrucache/entry/files/testFile.txt';
let fd = fileio.openSync(path, 0o2);
let length = fileio.statSync(path).size;
let value = new ArrayBuffer(length);
fileio.readSync(fd, value);
this.testDiskLruCache.setAsync('test', value).then(() => {
    this.testDiskLruCache.getAsync('test').then((data) => {
        console.log(String.fromCharCode.apply(null, new Uint8Array(data)));
    })
}).catch((err) => {
    console.log('err = ' + err)
});
```

##### 步骤4：

更多细节设置请参考index.ets示例文件。

## 接口说明

### DiskLruCache接口

| 方法名                                                       | 入参                                                     | 接口描述                                               |
| ------------------------------------------------------------ | -------------------------------------------------------- | ------------------------------------------------------ |
| create(path: string, maxSize?: number): DiskLruCache         | path: string, maxSize?: number                           | 构造器创建对象，设置磁盘缓存路径，磁盘缓存大小                    |
| setMaxSize(max: number):void                                 | max: number                                              | 重置磁盘缓存大小                                       |
| set(key: string, content: ArrayBuffer):void                  | key: string, content: ArrayBuffer                        | 磁盘缓存存入数据                                       |
| setAsync(key: string, content: ArrayBuffer):void             | key: string, content: ArrayBuffer                        | 异步磁盘缓存存入数据                                     |
| setFileByPath(key: string, path: string):void                | key: string, path: string                                | 文件路径磁盘缓存存入数据                                  |
| setFileByPathAsync(key: string, path: string):void           | key: string, path: string                                | 异步文件路径磁盘缓存存入数据                               |
| get(key: string): ArrayBuffer                                | key: string                                              | 磁盘缓存获取ArrayBuffer                                |
| getAsync(key: string): Promise<ArrayBuffer>                  | key: string                                              | 异步磁盘缓存获取ArrayBuffer                                |
| getFileToPath(key: string): string                           | key: string                                              | 磁盘缓存获取文件路径                                   |
| getFileToPathAsync(key: string): Promise<string>             | key: string                                              | 异步磁盘缓存获取文件路径                                   |
| getPath(): string                                            |                                                          | 获取缓存路径                                          |
| deleteCacheDataByKey(key: string): DiskCacheEntry            | key: string                                              | 删除当前缓存key对应的DiskCacheEntry                    |
| cleanCacheData()                                             |                                                          | 清除所有磁盘缓存数据                                   |

## 兼容性

支持 OpenHarmony API version 9 及以上版本。

## 目录结构

```
/disklrucache/src/
- main/ets/components
    - cache               # 缓存相关内容
    	- CustomMap.ets		# 自定义Map封装
    	- DiskCacheEntry.ets# 磁盘缓存entry
    	- DiskLruCache.ets  # 磁盘LRU缓存策略
    	- FileReader.ets  	# 文件读取相关
    	- FileUtils.ets		# 文件工具类
    	
/entry/src/
- main/ets/MainAbility    	
    - pages               # 测试page页面列表
    	- index.ets								# 测试磁盘缓存页面
```

## 贡献代码

使用过程中发现任何问题都可以提 [issue] 给我们，当然，我们也非常欢迎你给我们发 [PR] 。

## 开源协议

本项目基于 [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) ，请自由的享受和参与开源。