# Gson

## 简介

Gson用于对象与JSON字符串之间的互相转换，并支持JsonElement对象类型，使JSON字符串与对象之间的转换更高效、灵活，并且易于使用。

## 下载安装

````
npm install @ohos/gson --save
````

参考安装教程 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md)

## 使用说明

#### 1、JSON字符串转Object。

```typescript
// 引入Gson
import { Gson } from '@ohos/gson'
// JSON字符串
let str = '{"name":"张三"}'
// JSON字符串转对象
var obj1 = new Gson().fromJson(str);
// { name:"张三" }
```

#### 2、Object转JSON字符串。

```typescript
// 引入Gson
import { Gson } from '@ohos/gson'
// 对象
let str = { name: "张三" }
// 对象转JSON字符串
var obj1 = new Gson().toJson(str);
// '{"name":"张三"}'
```

#### 3、JSON字符串转JsonElement对象。

```typescript
// 引入parseString和JsonElement
import { parseString, JsonElement } from '@ohos/gson'
// JSON字符串
let str = '{"name":"张三"}'
// JSON字符串转JsonElement对象
var jsonElement:JsonElement = parseString(str)
// 类型是JsonElement的对象 { name:"张三" }
```

#### 4、Object转JsonElement对象。

```typescript
// 引入Gson和JsonElement
import { Gson, JsonElement } from '@ohos/gson'
// 对象
let str = { name: "张三" }
// Object转JsonElement对象
let jsonElement:JsonElement = new Gson().toJsonTree(str)
// 类型是JsonElement的对象 { name:"张三" }
```

#### 5、Object转JsonElement对象再转JSON字符串。

```typescript
// 引入Gson和JsonElement
import { Gson, JsonElement } from '@ohos/gson'
// 对象
let str = { name: "张三" }
// Object转JsonElement对象
let jsonElement:JsonElement = new Gson().toJsonTree(str)
// JsonElement对象转JSON字符串
let str2 = new Gson().toJson(jsonElement)
// '{"name":"张三"}'
```

#### 6、JSON字符串转JsonElement对象再转Object。

```typescript
// 引入Gson、parseString和JsonElement
import { Gson, JsonElement, parseString } from '@ohos/gson'
// JSON字符串
let str = '{"name":"张三"}'
// JSON字符串转JsonElement对象
let jsonElement:JsonElement = parseString(str)
// JsonElement对象转Object
let str2 = new Gson().fromJson(jsonElement)
// 类型是JsonElement的对象 { name:"张三" }
```

## 接口说明

### Gson
|方法名|入参|接口描述|
|:---:|:---:|:---:|
|fromJson|string|JSON字符串转对象|
|toJson|object|对象转JSON字符串|
|toJsonTree|object|对象转JsonElement|
|parseString|string|字符串转JsonElement|
## 兼容性

```
要求DevEco studio 3.2 Beta及以上 ，SDK版本号为Version 7以上。
```

## 目录结构

````
|---- Gson 
|     |---- entry  # 示例代码文件夹
|     |---- gson   # gson库文件夹
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法      
````
