/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import arkWorker from '@ohos.worker';

import { decompressFrames, parseGIF } from 'gifuct-js'

arkWorker.parentPort.onmessage = (e) => {
    var data = e.data;
    switch (data.type) {
        case 'asyncDecodeGIF':
            console.log('asyncDecodeGIF child Thread is come in!')
            let gifBuffer = data.data;
            let gif = parseGIF(gifBuffer)
            console.log('gif frames length=' + gif.frames.length)
            fixLoseGCE(gif)
            let origins = decompressFrames(gif, false)
            let images = prePatchGIF(origins);
            console.log('asyncDecodeGIF child Thread images.length=' + images.length)


            let dimss = [];

            let delays = [];

            let disposalTypes = [];

            let patchs = [];

            let transparentIndexs = [];


            for (let i = 0; i < images.length; i++) {
                dimss.push(images[i].dims)
                delays.push(images[i].delay)
                disposalTypes.push(images[i].disposalType)
                patchs.push(images[i].patch.buffer)
                transparentIndexs.push(images[i].transparentIndex)
            }
            let frame = {
                // 显示帧 width 宽  height 高 top上边距 left左边距
                dims: dimss,

                // 当前帧的像素数据指向的颜色数组 由于已经生成了patch colorTable可以删除
                //                colorTable: images[0].colorTable,

                // 当前帧到下一帧的间隔时长
                delay: delays,

                // 当前帧绘制要求 0保留 1在上一帧绘制此帧 2恢复画布背景 3.将画布恢复到绘制当前图像之前的先前状态
                disposalType: disposalTypes,

                // Uint8CampedArray颜色转换后的补片信息用于绘制
                patch: patchs,

                // 当前帧每个像素的颜色表查找索引 由于已经生成了patch pixels可以删除
                //                pixels: images[0].pixels,

                // 表示透明度的可选颜色索引
                transparentIndex: transparentIndexs
            }
            let a = new Uint8ClampedArray()
        //            let colorTable = images[0].colorTable

        //            let array = png.data;
        //            let arrayData = array.buffer.slice(array.byteOffset, array.byteLength + array.byteOffset)
        //            png.data = arrayData;
            let dataObj = { type: 'asyncDecodeGIF', data: frame }
            arkWorker.parentPort.postMessage(dataObj, patchs);
            break;
        default:

            break
    }


    function prePatchGIF(frames) {
        //初次绘制
        let filterCriteria = (item) => {
            if (!item['patch']) {
                return true
            }
        }

        frames.filter(filterCriteria, frames)
            .flatMap((frame) => {
                let uint8ClampedArray = undefined;
                uint8ClampedArray = generatePatch(frame);
                frame['patch'] = uint8ClampedArray;
            })
        return frames;
    }

    function generatePatch(image) {
        if (!image) {
            return undefined;
        }
        var totalPixels = image.pixels.length;
        var patchData = new Uint8ClampedArray(totalPixels * 4);
        for (var i = 0; i < totalPixels; i++) {
            var pos = i * 4;
            var colorIndex = image.pixels[i];
            var color = image.colorTable[colorIndex] || [0, 0, 0];
            // note: gifuct decode is RGBA,but image.createPixelMap encode is BGRA, so we need swap R & B
            // note: gifuct normal decode  ===== patchData[pos] = color[0]; patchData[pos + 1] = color[1]; patchData[pos + 2] = color[2];
            patchData[pos] = color[2];
            patchData[pos + 1] = color[1];
            patchData[pos + 2] = color[0];
            patchData[pos + 3] = colorIndex !== image.transparentIndex ? 255 : 0;
        }
        return patchData;
    }


    function fixLoseGCE(gif) {
        let currentGce = null;
        for (const frame of gif.frames) {
            currentGce = frame.gce ? frame.gce : currentGce;
            // fix loosing graphic control extension for same frames
            if ("image" in frame && !("gce" in frame)) {
                frame.gce = currentGce;
            }
        }
    }

}







