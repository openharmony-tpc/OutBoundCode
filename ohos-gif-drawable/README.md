# libgif_js

## 简介

本项目是OpenHarmony系统的一款GIF图像渲染库，基于Canvas进行绘制，并支持和gifuct-js配合使用，主要特性：

- 支持播放GIF图片。
- 支持控制GIF播放/暂停。
- 支持重置GIF播放动画。
- 支持调节GIF播放速率。
- 支持监听GIF所有帧显示完成后的回调。
- 支持设置显示大小。
- 支持7种ScaleType的显示类型。
- 支持设置显示区域背景颜色。

<img src="screenshot/g1.gif" width="100%"/>

## 下载安装

```typescript
npm install @ohos/libgif --save
```

OpenHarmony npm环境配置等更多内容，参考安装教程 [如何安装OpenHarmony npm包]()。

## 使用说明

1.在MainAbility中的MainAbility.ts中获取context

```typescript
export default class MainAbility extends Ability {
    onCreate(want, launchParam) {
        globalThis.Ctx = this.context;
    }
```

2.在需要展示的位置使用GIFComponent自定义组件

```typescript
// gif绘制组件用户属性设置
@State model1:GIFComponent.ControllerOptions = new GIFComponent.ControllerOptions();
// 是否自动播放/false只显示第一帧
@State gifAutoPlay1:boolean = true;
// 重置GIF播放,每次取反都能生效
@State gifReset1:boolean = false;

GIFComponent({ model: this.model1, autoPlay: this.gifAutoPlay1,resetGIF:this.gifReset1})
```

3.配置 GIFComponent.Model达到更多的效果

```
  let modelx = new GIFComponent.ControllerOptions()
   // 配置用户参数参数
   modelx
   // 设置监听循环结束回调
   .setLoopFinish(()=>{
         this.gif1LoopCount++;
         this.hint11 = '当前gif循环了'+this.gif1LoopCount+'次'
         })
   // 设置自定义组件展示大小      
   .setSize({width:200,height:200})
   // 设置ScaleType 7种展示类型
   .setScaleType(GIFComponent.ScaleType.FIT_CENTER)
   // 设置播放速率因子 2倍速播放
   .setSpeedFactor(2)
   // 设置组件背景颜色
   .setBackgroundColor(Color.Orange)
   
   // 加载一张网络图片
   ResourceLoader.downloadDataWithContext(globalThis.Ctx,{url:'https://pic.ibaotu.com/gif/18/17/16/51u888piCtqj.gif!fwpaa70/fw/700'},(sucBuffer)=>{
                modelx.loadBuffer(sucBuffer,()=>{
                 console.log('网络加载解析成功回调绘制！')
   				// 给自定义组件model1赋值，触发重新绘制
   				this.model1 = modelx;
                })
              },(err)=>{})
```

## 接口说明

### GIFComponent 自定义组件

| 方法名                                                       | 入参                                                         | 接口描述                |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ----------------------- |
| GIFComponent({ model: GIFComponent.ControllerOptions, autoPlay: boolean,resetGIF:boolean}) | model:GIFComponent.ControllerOptions, autoPlay:boolean,resetGIF:boolean | 自定义GIF渲染组件构造器 |

### GIFComponent.ControllerOptions自定义组件配置

| 使用方法                                                     | 入参                                                         | 接口描述                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| getFrames()                                                  |                                                              | 获取所有解析图像数据帧                                       |
| setFrames(images?: GIFFrame[])                               | images?: GIFFrame[]                                          | 设置解析图像数据帧集合                                       |
| getLoopFinish()                                              |                                                              | 获取循环结束最后一帧的回调方法                               |
| setLoopFinish(fn:(loopTime?)=>void)                          | fn:(loopTime?)=>void                                         | 设置循环结束最后一帧的回调方法                               |
| getSize()                                                    |                                                              | 获取用户设置组件大小                                         |
| setSize(size: { width: Length,   height: Length})            | size: { width: Length,   height: Length}                     | 设置组件大小                                                 |
| getScaleType()                                               |                                                              | 获取用户设置组件展示类型                                     |
| setScaleType(scaletype: ScaleType)                           | scaletype: ScaleType                                         | 设置组件展示类型                                             |
| getBackgroundColor()                                         |                                                              | 获取用户设置组件背景色                                       |
| setBackgroundColor(resColor:ResourceColor)                   | resColor:ResourceColor                                       | 设置组件背景色                                               |
| getPositionFinish()                                          |                                                              | 获取帧回调方法                                               |
| setPositionFinish(position: number, fn: (posTime?) => void)  | position: number, fn: (posTime?) => void                     | 设置帧位置和回调方法                                         |
| getSpeedFactor()                                             |                                                              | 获取当前播放速率                                             |
| setSpeedFactor(speed: number)                                | speed: number                                                | 设置播放速率                                                 |
| getSeekTo()                                                  |                                                              | 获取当前播放某一静态帧                                       |
| setSeekTo(gifPosition: number)                               | gifPosition: number                                          | 设置显示GIF某一静态帧                                        |
| loadBuffer(buffer: ArrayBuffer, readyRender: (err?) => void, worker?:any) | buffer: ArrayBuffer, readyRender: (err?) => void, worker?:any | 加载一个arraybuffer数据，展示GIF.如果由worker参数，则启动worker线程加载 |

### GIFComponent.ScaleType

| 展示类型                | 展示类型描述                                                 |
| ----------------------- | ------------------------------------------------------------ |
| ScaleType.FIT_START     | 显示内容缩放至能全部显示，然后将其放置于自定义组件的左上方   |
| ScaleType.FIT_END       | 显示内容缩放至能全部显示，然后将其放置于自定义组件的右下方   |
| ScaleType.FIT_CENTER    | 显示内容缩放至能全部显示，然后将其放置于自定义组件的居中位置 |
| ScaleType.CENTER        | 不进行缩放，然后将其放置于自定义组件的居中位置               |
| ScaleType.CENTER_CROP   | 取（内容/自定义组件）的宽高比值的小值进行缩放，缩放至自定义组件大小，然后将其放置于自定义组件的居中位置 |
| ScaleType.FIT_XY        | 将内容宽度和高度拉伸/压缩至自定义组件的大小                  |
| ScaleType.CENTER_INSIDE | 当内容宽高任意一个大于自定义组件宽高表现为ScaleType.FIT_CENTER，如果内容都小于自定义组件表现为ScaleType.CENTER |



### GIFFrame 解析帧数据格式

```typescript
export class GIFFrame{

  // 显示帧 width 宽  height 高 top上边距 left左边距
  dims: { width: number; height: number; top: number; left: number}

  // 当前帧的像素数据指向的颜色数组
  colorTable?: [number, number, number][]

  // 当前帧到下一帧的间隔时长
  delay: number

  // 当前帧绘制要求 0保留 1在上一帧绘制此帧 2恢复画布背景 3.将画布恢复到绘制当前图像之前的先前状态
  disposalType: number

  // Uint8CampedArray颜色转换后的补片信息用于绘制
  patch: Uint8ClampedArray

  // 当前帧每个像素的颜色表查找索引
  pixels?: number[]

  // 表示透明度的可选颜色索引
  transparentIndex: number
}
```



## 兼容性

支持 OpenHarmony API version 9 及以上版本。

## 目录结构

```
/imageknife/src/
- main/ets/components
    - gif               # gif渲染相关
    	- DownloadClient    # 网络文件下载
    	- FileUtils			# 文件操作工具
    	- GIFComponent		# gif渲染组件
    	- GIFFrame			# gif渲染解析帧数据
    	- LibGif			# 管理context,方便后续扩展
        - ResourceClient	# 本地资源文件加载
        
/entry/src/
- main/ets
	-Application
		-AbilityStage 	# app启动入口
	-MainAbility    
    	-MainAbility    # ability入口
    - pages               # 测试page页面列表
    	- index.ets		# 测试页面列表入口
    	- sampleTestCasePage.ets		# 测试gif解析和播放
    	- frameLoadTestCasePage.ets		# 测试单帧加载
    	- greyingTestCasePage.ets		# 测试灰白单帧加载
    	- reversesColorTestCasePage.ets		# 测试颜色反转单帧加载
    	- transformTestCasePage.ets		# setTransform测试
    	- scaleTypeTestCasePage.ets		# 测试scaleType
    	- gifComponentWithScaleTypePage.ets		# 测试GIF播放和scaleType结合
    	- networkLoadTestCasePage.ets		# 测试网络文件下载、GIF播放和scaleType结合
    	- loadResourceAndRawFilePage.ets		# 测试工程文件加载、GIF播放和scaleType结合
    	- loadStringAndBufferPage.ets	# 测试app文件路径加载、arraybuffer加载、GIF播放和scaleType结合
```

## 贡献代码

使用过程中发现任何问题都可以提 [issue]() 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-tpc/ImageKnife/issues) 。

## 开源协议

本项目基于 [Apache License 2.0]() ，请自由的享受和参与开源。
