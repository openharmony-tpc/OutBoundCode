﻿# protobuf

### 介绍

ProtoBuf(protocol buffers) 是一种语言无关、平台无关、可扩展的序列化结构数据的方法，它可用于（数据）通信协议、数据存储等。,是一种灵活，高效，自动化机制的结构数据序列化方法比XML更小,更快,更为简单。

本项目主要是OpenHarmony系统下以[protobufjs](https://github.com/protobufjs/protobuf.js)为主要依赖开发，主要接口针对OpenHarmony系统进行合理的适配研发。

### 下载安装

1.安装

```
npm install @ohos/protobuf
```

2.在需要使用的页面导入protobufjs

```
import { ProtoBuf } from '@ohos/protobuf'
```

### 使用说明

1. 先定一个proto的格式协议字符串或.proto文件

```
const protoStr = 'syntax = "proto3"; package com.user;message UserLoginResponse{string sessionId = 1;string userPrivilege = 2;bool isTokenType = 3;string formatTimestamp = 4;}';
```

或 在resources->base->media下按照proto格式定义xxx.proto文件

```
syntax = "proto3";  
package User;
message UserLoginResponse{
   string sessionId = 1;
   string userPrivilege = 2;
   bool isTokenType = 3;
   string formatTimestamp = 4;
}
```

2. 通过loadProto方法加载proto的格式协议字符串并解析协议,或使用loadProtoFile即可解析.proto文件

```
var builder = ProtoBuf.newBuilder();
    ProtoBuf.loadProto(proto,builder,"bench.proto");
```

或

```
var builder = ProtoBuf.newBuilder();
    var root = ProtoBuf.loadProtoFile(context, id, builder, fileName);
```

3. 通过builder找到协议名后会产生Message，并创建一个相同协议结构的数据对象，放入已实例的Message

```
 var UserLoginResponse = root.build("com.user.UserLoginResponse");

    const userLogin = {
      sessionId: "xd3sdfsd22",
      userPrivilege: "John123",
      isTokenType: false,
      formatTimestamp: "12342222"
    };

    var msg = new UserLoginResponse(userLogin);
```

4. 将Message序列化,可进行通信传递或存储

```
var arrayBuffer = msg.toArrayBuffer();
```

5. 对方拿到传递或存储的数据再按照1，2，3步骤拿到UserLoginResponse对象后再进行反序列化即可得到数据

```
var decodeMsg = UserLoginResponse.decode(arrayBuffer);
```

### 接口说明

1， public static newBuilder(): any 

Constructs a new empty Builder.

2， public static loadProto(proto: string, builder: any, fileName: string): any 

Loads a proto string and returns the Builder.

3，public static loadProtoFile(context: any, protoFileResId: Number, builder: any, fileName: string): any 

Loads a .proto file and returns the Builder.

4，public static loadJson(json: string, builder: any, fileName: string): any 

Loads a jsonStr definition and returns the Builder.

5，public static loadJsonFile(context: any, jsonFileResId: Number, builder: any, fileName: string): any 

Loads a .json file definition and returns the Builder.

6，public static protoBufJS():any support a protobufJS

7，.toArrayBuffer(); 将Message序列化

8，.decode(buffer); 将buffer数据反序列化

### 兼容性

支持OpenHarmony API Version 9 及以上版本。

### 软件架构

```
|-ets
|   |-ProtoBuf.ets #ProtoBuf的核心代码


```

### 版本

当前版本：

```
1.2.0
```

