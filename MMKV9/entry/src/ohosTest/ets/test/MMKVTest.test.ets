/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import featureability from '@ohos.ability.featureAbility'
import {MMKV} from '@ohos/MMKV'
import dataStorage from '@ohos.data.storage';


export default function mmkvRootJsunit() {

  describe('mmkvTest', function () {
    let filePath = '/data/storage/el2/base/haps/entry/files'
    let cachePath = '/data/storage/el2/base/haps/entry/cache'

    let mmapID = 'testMMKV'
    let cryptKey = 'Tencent MMKV'
    let version = 'v1.2.11'
    let pageSize = 4096
    beforeAll(function () {
      MMKV.initialize(filePath+'/mmkv',cachePath)
    });

    it('mmkv_version_test_001', 0, function () {
      let result = MMKV.version()
      expect(result).assertEqual(version)
    })
    it('mmkv_root_test_001', 0, function () {
      let result = MMKV.getRootDir()
      expect(result).assertEqual(filePath+'/mmkv')
    })
    it('mmkv_pageSize_test_001', 0, function () {
      let result = MMKV.pageSize()
      expect(result).assertEqual(pageSize)
    })
    it('mmkv_cons_default_test_001', 0, function () {
      let kv = MMKV.getDefaultMMKV(MMKV.SINGLE_PROCESS_MODE,null)
      kv.clearAll()
      let result = kv.totalSize()
      expect(result).assertEqual(pageSize)
    })
    it('mmkv_cons_default_test_001', 0, function () {
      let kv = MMKV.getDefaultMMKV(MMKV.SINGLE_PROCESS_MODE,null)
      kv.clearAll()
      let result = kv.totalSize()
      expect(result).assertEqual(pageSize)
    })
    it('mmkv_totalSize_test_001', 0, function () {
      let kv = MMKV.getMMKVWithAshmemID(mmapID,pageSize,MMKV.SINGLE_PROCESS_MODE,cryptKey)
      kv.clearAll()
      let result = kv.totalSize()
      expect(result).assertEqual(pageSize)
    })
    it('mmkv_encode_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      kv.clearAll()
      let key = 'keystring'
      let result = kv.encode(key,'value1')
      expect(result).assertEqual(true)
    })
    it('mmkv_decode_string_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let key = 'keystrsss'
      let value = 'value1'
      kv.clearAll()
      kv.encode(key,value)
      let result = kv.decodeString(key)
      expect(result).assertEqual(value)
    })
    it('mmkv_decode_boolean_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let key = 'keybool'
      let value = false
      kv.clearAll()
      kv.encode(key,value)
      let result = kv.decodeBool(key)
      expect(result).assertEqual(0)
    })
    it('mmkv_decode_bumber_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let key = 'keynumber'
      let value = 111
      kv.clearAll()
      kv.encode(key,value)
      let result = kv.decodeNumber(key)
      expect(result).assertEqual(value)
    })
    it('mmkv_decode_set_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let key = 'keyset'
      let set1 = new Set<string>()
      set1.add('ss1')
      set1.add('ss2')
      set1.add('ss3')
      kv.clearAll()
      kv.encode(key, set1)
      let result:Set<string> = kv.decodeSet(key)
      expect(result.size).assertEqual(3)
    })
    it('mmkv_containkey_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let key = 'keynumber'
      let value = 111
      kv.clearAll()
      kv.encode(key,value)
      let result = kv.containsKey(key)
      expect(result).assertEqual(true)
    })
    it('mmkv_getCryptKey_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let result = kv.getCryptKey()
      expect(result).assertEqual(cryptKey)
    })
    it('mmkv_getMMapID_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let result = kv.getMMapID()
      expect(result).assertEqual(mmapID)
    })
    it('mmkv_removeonekey_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let key = 'keynumber1'
      let value = 111
      kv.clearAll()
      kv.encode(key,value)
      kv.removeValueForKey(key)
      let result = kv.getAllKeys()
      expect(result.length).assertEqual(0)
    })
    it('mmkv_removemulkey_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let key = 'keynumber3'
      let key1 = 'keynumber1'
      let key2 = 'keynumber2'
      let value = 111
      kv.clearAll()
      kv.encode(key,value)
      kv.encode(key1,value)
      kv.encode(key2,value)
      kv.removeValuesForKeys([key1, key2])
      let result = kv.getAllKeys()
      expect(result.length).assertEqual(1)
    })
    it('mmkv_clearallkey_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let key = 'keynumber'
      let key1 = 'keynumber1'
      let key2 = 'keynumber2'
      let value = 111
      kv.encode(key,value)
      kv.encode(key1,value)
      kv.encode(key2,value)
      kv.clearAll()
      let result:string[] = kv.getAllKeys()
      expect(result.length).assertEqual(0)
    })
    it('mmkv_count_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let key = 'keynumber'
      let key1 = 'keynumber1'
      let key2 = 'keynumber2'
      let value = 111
      kv.clearAll()
      kv.encode(key,value)
      kv.encode(key1,value)
      kv.encode(key2,value)
      let result = kv.count()
      expect(result).assertEqual(3)
    })
    it('mmkv_recrypt_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let result = kv.reCryptKey('Key_seq_2')
      kv.clearMemoryCache()
      expect(result).assertEqual(true)
    })
    it('mmkv_isFileValid_test_001', 0, function () {
      let kv = MMKV.getMMKVWithMMapID(mmapID,MMKV.SINGLE_PROCESS_MODE,cryptKey,null)
      let result = MMKV.isFileValid(kv.getMMapID())
      expect(result).assertEqual(true)
    })
    it('mmkv_backone_test_001', 0, function () {
      let backupRootDir = filePath + '/mmkv_backup_3'
      let mmapID = 'test/AES'
      let otherDir = filePath + '/mmkv_3'
      let result = MMKV.backupOneToDirectory(mmapID, backupRootDir, otherDir);
      expect(result).assertEqual(false)
    })
    it('mmkv_backall_test_001', 0, function () {
      let backupRootDir = filePath + '/mmkv_backup'
      let result = MMKV.backupAllToDirectory(backupRootDir)
      let mmkv = MMKV.getMMKVWithMMapID(mmapID, MMKV.SINGLE_PROCESS_MODE, null, null);
      console.debug('MMKV - resortDir check on backup file[' + mmkv.getMMapID() + '] allKeys: ' + mmkv.getAllKeys())
      expect(result).assertEqual(0)
    })
    it('mmkv_restorone_test_001', 0, function () {
      let backupRootDir = filePath + '/mmkv_backup_3'
      let mmapID = 'test/AES'
      let otherDir = filePath + '/mmkv_3'
      let result = MMKV.restoreOneMMKVFromDirectory(mmapID, backupRootDir, otherDir);
      console.debug("MMKV - mmkv_restorone_test_001 "+result)
      expect(result).assertEqual(false)
    })
    it('mmkv_restoreall_test_001', 0, function () {
      let backupRootDir = filePath + '/mmkv_backup'
      let result = MMKV.restoreAllFromDirectory(backupRootDir)

      let mmkv = MMKV.getMMKVWithMMapID(mmapID, MMKV.SINGLE_PROCESS_MODE, null, null);
      console.debug('MMKV - resortDir check on restore file[' + mmkv.getMMapID() + '] allKeys: ' + mmkv.getAllKeys())
      expect(result).assertEqual(0)
    })
  })
}