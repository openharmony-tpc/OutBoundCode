##1.0.6
hvigor工程结构整改

## 1.0.4
一款小型键值对存储框架
- 支持存储number、boolean、string、Set<String>类型数据存储
- 支持继承组件中SerializeBase.ets的class类对象的序列化反序列化
- 支持存储数据备份
- 支持存储数据恢复
- 支持系统dataStorage的API的存储数据转移存到mmkv存储文件中
