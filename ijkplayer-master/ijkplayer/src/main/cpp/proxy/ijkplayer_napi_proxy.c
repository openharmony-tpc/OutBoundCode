/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ijkplayer_napi_proxy.h"

typedef struct player_fields_t {
    pthread_mutex_t mutex;
} player_fields_t;
static player_fields_t g_clazz;
IjkMediaPlayer *GLOBAL_IJKMP;
void *GLOBAL_NATIVE_WINDOW;
bool IJKMP_GLOABL_INIT;


//TODO 发送消息
static void (*post_event)(void *weak_this, int what, int arg1, int arg2);

/**
 * 注册接口回调
 */
void message_loop_callback(void (*pe)(void *weak_this, int what, int arg1, int arg2)){
     post_event = pe;
}



static void message_loop_n(IjkMediaPlayer *mp)
{
    LOGI("napi_proxy-->message_loop_n");
    void *weak_thiz = ijkmp_get_weak_thiz(mp);

    while (1) {
        AVMessage msg;
        LOGI("napi_proxy-->message_loop_n-->go");
        int retval = ijkmp_get_msg(mp, &msg, 1);
        if (retval < 0)
            break;

        // block-get should never return 0
        assert(retval > 0);

        switch (msg.what) {
            LOGI("napi_proxy-->message_loop_n-->go-->msg:%d",msg.what);
            case FFP_MSG_FLUSH:
            MPTRACE("FFP_MSG_FLUSH:\n");
            post_event(weak_thiz, MEDIA_NOP, 0, 0);
            break;
            case FFP_MSG_ERROR:
            MPTRACE("FFP_MSG_ERROR: %d\n", msg.arg1);
            post_event(weak_thiz, MEDIA_ERROR, MEDIA_ERROR_IJK_PLAYER, msg.arg1);
            break;
            case FFP_MSG_PREPARED:
            MPTRACE("FFP_MSG_PREPARED:\n");
            post_event(weak_thiz, MEDIA_PREPARED, 0, 0);
            break;
            case FFP_MSG_COMPLETED:
            MPTRACE("FFP_MSG_COMPLETED:\n");
            post_event(weak_thiz, MEDIA_PLAYBACK_COMPLETE, 0, 0);
            break;
            case FFP_MSG_VIDEO_SIZE_CHANGED:
            MPTRACE("FFP_MSG_VIDEO_SIZE_CHANGED: %d, %d\n", msg.arg1, msg.arg2);
            post_event(weak_thiz, MEDIA_SET_VIDEO_SIZE, msg.arg1, msg.arg2);
            break;
            case FFP_MSG_SAR_CHANGED:
            MPTRACE("FFP_MSG_SAR_CHANGED: %d, %d\n", msg.arg1, msg.arg2);
            post_event(weak_thiz, MEDIA_SET_VIDEO_SAR, msg.arg1, msg.arg2);
            break;
            case FFP_MSG_VIDEO_RENDERING_START:
            MPTRACE("FFP_MSG_VIDEO_RENDERING_START:\n");
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_VIDEO_RENDERING_START, 0);
            break;
            case FFP_MSG_AUDIO_RENDERING_START:
            MPTRACE("FFP_MSG_AUDIO_RENDERING_START:\n");
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_AUDIO_RENDERING_START, 0);
            break;
            case FFP_MSG_VIDEO_ROTATION_CHANGED:
            MPTRACE("FFP_MSG_VIDEO_ROTATION_CHANGED: %d\n", msg.arg1);
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_VIDEO_ROTATION_CHANGED, msg.arg1);
            break;
            case FFP_MSG_AUDIO_DECODED_START:
            MPTRACE("FFP_MSG_AUDIO_DECODED_START:\n");
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_AUDIO_DECODED_START, 0);
            break;
            case FFP_MSG_VIDEO_DECODED_START:
            MPTRACE("FFP_MSG_VIDEO_DECODED_START:\n");
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_VIDEO_DECODED_START, 0);
            break;
            case FFP_MSG_OPEN_INPUT:
            MPTRACE("FFP_MSG_OPEN_INPUT:\n");
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_OPEN_INPUT, 0);
            break;
            case FFP_MSG_FIND_STREAM_INFO:
            MPTRACE("FFP_MSG_FIND_STREAM_INFO:\n");
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_FIND_STREAM_INFO, 0);
            break;
            case FFP_MSG_COMPONENT_OPEN:
            MPTRACE("FFP_MSG_COMPONENT_OPEN:\n");
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_COMPONENT_OPEN, 0);
            break;
            case FFP_MSG_BUFFERING_START:
            MPTRACE("FFP_MSG_BUFFERING_START:\n");
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_BUFFERING_START, msg.arg1);
            break;
            case FFP_MSG_BUFFERING_END:
            MPTRACE("FFP_MSG_BUFFERING_END:\n");
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_BUFFERING_END, msg.arg1);
            break;
            case FFP_MSG_BUFFERING_UPDATE:
            // MPTRACE("FFP_MSG_BUFFERING_UPDATE: %d, %d", msg.arg1, msg.arg2);
            post_event(weak_thiz, MEDIA_BUFFERING_UPDATE, msg.arg1, msg.arg2);
            break;
            case FFP_MSG_BUFFERING_BYTES_UPDATE:
            break;
            case FFP_MSG_BUFFERING_TIME_UPDATE:
            break;
            case FFP_MSG_SEEK_COMPLETE:
            MPTRACE("FFP_MSG_SEEK_COMPLETE:\n");
            post_event(weak_thiz, MEDIA_SEEK_COMPLETE, 0, 0);
            break;
            case FFP_MSG_ACCURATE_SEEK_COMPLETE:
            MPTRACE("FFP_MSG_ACCURATE_SEEK_COMPLETE:\n");
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_MEDIA_ACCURATE_SEEK_COMPLETE, msg.arg1);
            break;
            case FFP_MSG_PLAYBACK_STATE_CHANGED:
            break;
            case FFP_MSG_TIMED_TEXT:
            post_event(weak_thiz, MEDIA_TIMED_TEXT, 0, 0);
            break;
            case FFP_MSG_GET_IMG_STATE:
            post_event(weak_thiz, MEDIA_GET_IMG_STATE, msg.arg1, msg.arg2);
            break;
            case FFP_MSG_VIDEO_SEEK_RENDERING_START:
            MPTRACE("FFP_MSG_VIDEO_SEEK_RENDERING_START:\n");
            post_event(weak_thiz, MEDIA_INFO, MEDIA_INFO_VIDEO_SEEK_RENDERING_START, msg.arg1);
            break;
            case FFP_MSG_AUDIO_SEEK_RENDERING_START:
            MPTRACE("FFP_MSG_AUDIO_SEEK_RENDERING_START:\n");
            post_event( weak_thiz, MEDIA_INFO, MEDIA_INFO_AUDIO_SEEK_RENDERING_START, msg.arg1);
            break;
            default:
            ALOGE("unknown FFP_MSG_xxx(%d)\n", msg.what);
            break;
        }
        msg_free_res(&msg);
    }

    LABEL_RETURN:
    ;
}

static int message_loop(void *arg);

static int message_loop(void *arg)
{
    LOGI("napi_proxy-->message_loop");
    IjkMediaPlayer *mp = (IjkMediaPlayer*) arg;
    message_loop_n(mp);
    return 0;
}

/**
 * 取对象
 */
static IjkMediaPlayer *get_media_player()
{
    LOGI("napi_proxy-->get_media_player");
    pthread_mutex_lock(&g_clazz.mutex);
    IjkMediaPlayer *mp = GLOBAL_IJKMP;
    if (mp) {
        ijkmp_inc_ref(mp);
    }
    pthread_mutex_unlock(&g_clazz.mutex);
    return mp;
}


/**
 * 设置MP对象
 * @param mp
 * @return
 */
static IjkMediaPlayer *set_media_player(IjkMediaPlayer *mp)
{
    LOGI("napi_proxy-->set_media_player");
    pthread_mutex_lock(&g_clazz.mutex);
    if (mp) {
        ijkmp_inc_ref(mp);
    }
    GLOBAL_IJKMP = mp;
    if(mp){
        LOGI("napi_proxy-->set_media_player success");
    }
    else{
        LOGI("napi_proxy-->set_media_player error");
    }
    pthread_mutex_unlock(&g_clazz.mutex);
    // NOTE: ijkmp_dec_ref may block thread
    return mp;
}


/**
 * 初始化配置，并传递native_window
 * @param weak_this
 * @param native_window
 */
void IjkMediaPlayer_native_setup(void *weak_this,void *native_window){
    LOGI("napi_proxy-->IjkMediaPlayer_native_setup");
    if(weak_this){
        LOGI("napi_proxy-->IjkMediaPlayer_native_setup-->weak_this success");
    }
    if(native_window){
        LOGI("napi_proxy-->IjkMediaPlayer_native_setup-->native_window success");
    }
    if(!IJKMP_GLOABL_INIT) {
        ijkmp_global_init();
    }
    IJKMP_GLOABL_INIT=true;
    GLOBAL_NATIVE_WINDOW=native_window;
    IjkMediaPlayer *mp = ijkmp_android_create(message_loop);
    set_media_player(mp);
    ijkmp_android_set_surface(mp, native_window);
    ijkmp_set_weak_thiz(mp, weak_this);
    ijkmp_set_inject_opaque(mp, ijkmp_get_weak_thiz(mp));
    ijkmp_set_ijkio_inject_opaque(mp, ijkmp_get_weak_thiz(mp));
}


/**
 * 设置视频源地址
 */
 void IjkMediaPlayer_setDataSource(char *url)
{
    char *c_path = NULL;
    IjkMediaPlayer *mp = get_media_player();
    c_path = url;
    ijkmp_set_data_source(mp, c_path);
}

 void IjkMediaPlayer_setOption(int category, char *name, char *value)
{
    IjkMediaPlayer *mp = get_media_player();
    ijkmp_set_option(mp, category, name, value);
}

void IjkMediaPlayer_setOptionLong(int category, char *name, int64_t value)
{
    IjkMediaPlayer *mp = get_media_player();
    ijkmp_set_option_int(mp, category, name, value);
}

/**
 * 加载
 */
 void IjkMediaPlayer_prepareAsync()
{
   IjkMediaPlayer *mp = get_media_player();
   ijkmp_prepare_async(mp);
}

/**
 * 开始播放
 */
 void  IjkMediaPlayer_start()
{
   IjkMediaPlayer *mp = get_media_player();
   ijkmp_start(mp);
}


/**
 * 暂停
 */
 void IjkMediaPlayer_pause()
{
   IjkMediaPlayer *mp = get_media_player();
   ijkmp_pause(mp);
}

/**
 * 快进、后退
 */
 void IjkMediaPlayer_seekTo(int64_t msec)
{
   IjkMediaPlayer *mp = get_media_player();
   ijkmp_seek_to(mp, msec);
}

/**
 * 判断播放状态
 */
 bool IjkMediaPlayer_isPlaying()
{
    bool retval=false;
   IjkMediaPlayer *mp = get_media_player();
   retval = ijkmp_is_playing(mp) ? true : false;
   return retval;
}

/**
 * 获取当前播放位置
 */
 int IjkMediaPlayer_getCurrentPosition()
{
    int retval = 0;
    IjkMediaPlayer *mp = get_media_player();
    retval=ijkmp_get_current_position(mp);
    return retval;
}

/**
 * 获取总的播放时间
 */
 int IjkMediaPlayer_getDuration()
{
    int retval = 0;
    IjkMediaPlayer *mp = get_media_player();
    retval = ijkmp_get_duration(mp);
    return retval;
}


/**
 * 停止播放
 */
void IjkMediaPlayer_stop()
{
    IjkMediaPlayer *mp = get_media_player();
    ijkmp_stop(mp);
}


/**
 * 释放
 */
 void IjkMediaPlayer_release()
{
    IjkMediaPlayer *mp = get_media_player();
    if (!mp)  return;
//    ijkmp_android_set_surface(mp, NULL);
    ijkmp_shutdown(mp);
//    void *weak_thiz = ijkmp_set_weak_thiz(mp, NULL);
//    set_media_player(NULL);
    ijkmp_dec_ref_p(&mp);
}

/**
 * 重置
 */
 void IjkMediaPlayer_reset()
{
     IjkMediaPlayer *mp = get_media_player();
     if (!mp) return;
     void *weak_thiz = ijkmp_set_weak_thiz(mp, NULL );
     IjkMediaPlayer_release();
     IjkMediaPlayer_native_setup(weak_thiz,GLOBAL_NATIVE_WINDOW);
     ijkmp_dec_ref_p(&mp);
}


/**
 * 设置音量
 */
 void IjkMediaPlayer_setVolume(float leftVolume, float rightVolume)
{
      LOGI("napi-->IjkMediaPlayer_setVolume-->leftVolume f:%f,rightVolume:%f",leftVolume, rightVolume);
      IjkMediaPlayer *mp = get_media_player();
      ijkmp_android_set_volume(mp, leftVolume, rightVolume);
}


void ijkMediaPlayer_setPropertyFloat(int id, float value)
{
    IjkMediaPlayer *mp = get_media_player();
    ijkmp_set_property_float(mp, id, value);
}


float ijkMediaPlayer_getPropertyFloat(int id, float default_value)
{
    float value = default_value;
    IjkMediaPlayer *mp = get_media_player();
    value = ijkmp_get_property_float(mp, id, default_value);
    return value;
}

void ijkMediaPlayer_setPropertyLong(int id, long value)
{
    IjkMediaPlayer *mp = get_media_player();
    ijkmp_set_property_int64(mp, id, value);
}


long ijkMediaPlayer_getPropertyLong(int id, long default_value)
{
    long value = default_value;
    IjkMediaPlayer *mp = get_media_player();
    value = ijkmp_get_property_int64(mp, id, default_value);
    return value;
}


int IjkMediaPlayer_getAudioSessionId()
{
    int audio_session_id = 0;
    IjkMediaPlayer *mp = get_media_player();
    audio_session_id = ijkmp_android_get_audio_session_id( mp);
    return audio_session_id;
}

void IjkMediaPlayer_setLoopCount(int loop_count)
{
    IjkMediaPlayer *mp = get_media_player();
    ijkmp_set_loop(mp, loop_count);
}

int IjkMediaPlayer_getLoopCount()
{
    IjkMediaPlayer *mp = get_media_player();
    int loop_count = ijkmp_get_loop(mp);
    return loop_count;
}

char* IjkMediaPlayer_getVideoCodecInfo(){
    LOGI("IjkMediaPlayer_getVideoCodecInfo");
    int ret = 0;
    char *codec_info = NULL;
    IjkMediaPlayer *mp = get_media_player();
    ret = ijkmp_get_video_codec_info(mp, &codec_info);
    LOGI("IjkMediaPlayer_getVideoCodecInfo codec_info:%s",codec_info);
    return codec_info;
}

char* IjkMediaPlayer_getAudioCodecInfo(){
    MPTRACE("%s\n", __func__);
    int ret = 0;
    char *codec_info = NULL;
    IjkMediaPlayer *mp = get_media_player();
    ret = ijkmp_get_audio_codec_info(mp, &codec_info);
    return codec_info;
}

void ijkMediaPlayer_setStreamSelected(int stream, bool selected)
{
    IjkMediaPlayer *mp = get_media_player();
    int ret = 0;
    ret = ijkmp_set_stream_selected(mp, stream, selected);
    if (ret < 0) {
        LOGI("failed to %s %d", selected ? "select" : "deselect", stream);
        goto LABEL_RETURN;
    }
    LABEL_RETURN:
    ijkmp_dec_ref_p(&mp);
    return;
}

char *getFromMediaMetaByKey(IjkMediaMeta *meta,char *key){
    char *value = ijkmeta_get_string_l(meta, key);
    return value;
}

HashMap  IjkMediaPlayer_getMediaMeta()
{
    IjkMediaMeta *meta = NULL;
    IjkMediaPlayer *mp = get_media_player();
    meta = ijkmp_get_meta_l(mp);
    size_t count = ijkmeta_get_children_count_l(meta);
    HashMap map = hashmap_create();
    for (size_t i = 0; i < count; ++i) {
        IjkMediaMeta * streamRawMeta = ijkmeta_get_child_l(meta, i);
        if (streamRawMeta) {
            map->put(map, IJKM_KEY_TYPE, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_TYPE));
            map->put(map, IJKM_KEY_TYPE, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_TYPE));
            map->put(map, IJKM_KEY_LANGUAGE, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_LANGUAGE));
            const char *type = ijkmeta_get_string_l(streamRawMeta, IJKM_KEY_TYPE);
            if (type) {
                map->put(map, IJKM_KEY_CODEC_NAME, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_CODEC_NAME));
                map->put(map, IJKM_KEY_CODEC_PROFILE, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_CODEC_PROFILE));
                map->put(map, IJKM_KEY_CODEC_LEVEL, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_CODEC_LEVEL));
                map->put(map, IJKM_KEY_CODEC_LONG_NAME, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_CODEC_LONG_NAME));
                map->put(map, IJKM_KEY_CODEC_PIXEL_FORMAT, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_CODEC_PIXEL_FORMAT));
                map->put(map, IJKM_KEY_BITRATE, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_BITRATE));
                map->put(map, IJKM_KEY_CODEC_PROFILE_ID, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_CODEC_PROFILE_ID));
                if (0 == strcmp(type, IJKM_VAL_TYPE__VIDEO)) {
                    map->put(map, IJKM_KEY_WIDTH, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_WIDTH));
                    map->put(map, IJKM_KEY_HEIGHT, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_HEIGHT));
                    map->put(map, IJKM_KEY_FPS_NUM, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_FPS_NUM));
                    map->put(map, IJKM_KEY_TBR_NUM, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_TBR_NUM));
                    map->put(map, IJKM_KEY_TBR_DEN, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_TBR_DEN));
                    map->put(map, IJKM_KEY_SAR_NUM, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_SAR_NUM));
                    map->put(map, IJKM_KEY_SAR_DEN, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_SAR_DEN));
                } else if (0 == strcmp(type, IJKM_VAL_TYPE__AUDIO)) {
                    map->put(map, IJKM_KEY_SAMPLE_RATE, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_SAMPLE_RATE));
                    map->put(map, IJKM_KEY_CHANNEL_LAYOUT, getFromMediaMetaByKey(streamRawMeta,IJKM_KEY_CHANNEL_LAYOUT));
                }
            }
        }
    }
    return map;
}


void IjkMediaPlayer_native_openlog()
{
    OHOS_LOG_ON = true;
    open_custom_ffmpeg_log_print();
}