# icu

## 简介
> 国际化 日期，时间，语言


![img1.png](img1.png)

## 下载安装
```shell
npm install @f-fjs/intl-messageformat-parser;
```

## 使用说明

### leven字符差异
```
  import { parse } from '@f-fjs/intl-messageformat-parser';
  
   Column() {
        Button("解析消息").fontSize(24).onClick(()=>{
          const ast=parse('On{takenDate, date, short} {name} took {numPhotos,plural, =0 {no photos.} =1{one photo.} other {# photos}}')
          console.log('llkkklk2 '+JSON.stringify(ast))
          setData(JSON.stringify(ast),this)
        })
      }
```



## 目录结构
````
|---- icu 
|     |---- entry  # 示例代码文件夹
|           |---- index.ets  # 对外接口
|     |---- README.MD  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue]() 给我们，当然，我们也非常欢迎你给我们发 [PR]() 。

## 开源协议
本项目基于 [Apache License 2.0]() ，请自由地享受和参与开源。