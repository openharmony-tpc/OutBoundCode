/**
 * Node.js module for Forge.
 *
 * @author Dave Longley
 *
 * Copyright 2011-2016 Digital Bazaar, Inc.
 */
module.exports = {
  // default options
  options: {
    //true:纯js；false：用node框架/浏览器框架库;默认是false 202207
    usePureJavaScript: false
  }
};
