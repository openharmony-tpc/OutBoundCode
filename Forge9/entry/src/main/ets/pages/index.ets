/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import forge from '@ohos/forge'

@Entry
@Component
struct Index {
  @State message: string = 'Hello World'

  build() {
    Row() {
      Column() {
        Button('rsa')
          .fontSize(35)
          .onClick(() => {
            this.rsa();
          })
        Button('rsa2')
          .fontSize(35)
          .margin({ top: 15 })
          .onClick(() => {
            this.rsa2();
          })
        Button('rsa3')
          .fontSize(35)
          .margin({ top: 15 })
          .onClick(() => {
            this.rsa3();
          })
        Button('randomTest')
          .fontSize(35)
          .margin({ top: 15 })
          .onClick(() => {
                        this.test();
//            this.RC2();
          })
        Button('aes')
          .fontSize(35)
          .margin({ top: 15 })
          .onClick(() => {
            this.aes();
          })
        Button('aes2')
          .fontSize(35)
          .margin({ top: 15 })
          .onClick(() => {
            this.aes2();
          })
        Button('x509')
          .fontSize(35)
          .margin({ top: 15 })
          .onClick(() => {
//            this.x509();
            this.x509Test();
          })
      }
      .width('100%')
    }
    .height('100%')
  }

  test() {
    var n = forge.util.createBuffer('4414734108537352243564967743135050183830173163413767804004587355' +
    '008117253822963933982662815593261455378535526982862575108699387083329998966112318180278923')
      .toHex();
    var e = forge.util.createBuffer('88294682170747044871299354862701003676603463268275356080091747100162345' +
    '07645927867965325631186522910757071053965725150217398774166659997932224636360557847')
      .toHex();
    var big1 = new forge.jsbn.BigInteger(n, 16);
    var big2 = new forge.jsbn.BigInteger(e, 16);
    big2.modPow(big1, big2);
  }

  rsa() {
    const time1 = Date.now();
    forge.pki.rsa.generateKeyPair({ bits: 2048, workers: 1 }, function (err, keypair) {
      const time2 = Date.now();
      console.log('Forge rsa1: Key-pair created=' + (time2 - time1));
      console.log('Forge rsa2: Key-pair created.');
      var encrypted = keypair.publicKey.encrypt('Hello');
      console.log('Forge rsa3:' + forge.util.encode64(encrypted));
      var content = keypair.privateKey.decrypt(encrypted);
      console.log('Forge rsa4:' + content);
    });
  }

  rsa2() {
    const time1 = Date.now();
    var keys = forge.pki.rsa.generateKeyPair(1024);
    const time2 = Date.now();
    console.log('Forge rsa21: Key-pair created=' + (time2 - time1));
    //     encrypt data with a public key using RSAES-OAEP/SHA-256/MGF1-SHA-1
    //     compatible with Java's RSA/ECB/OAEPWithSHA-256AndMGF1Padding
    var encrypted = keys.publicKey.encrypt('Hello', 'RSA-OAEP', {
      md: forge.md.sha256.create(),
      mgf1: {
        md: forge.md.sha1.create()
      }
    });
    const time3 = Date.now();
    console.log('Forge rsa22: encrypt finish=' + (time3 - time2));
    console.log('Forge rsa23:' + forge.util.encode64(encrypted));
    var content = keys.privateKey.decrypt(encrypted, 'RSA-OAEP', {
      md: forge.md.sha256.create(),
      mgf1: {
        md: forge.md.sha1.create()
      }
    });
    console.log('Forge rsa24:' + content);
  }

  rsa3() {
    const time1 = Date.now();
    forge.pki.rsa.generateKeyPair({ bits: 1024, workers: 2 }, function (err, keypair) {
      const time2 = Date.now();
      console.log("Forge rsa31: saisai==" + (time2 - time1));
      console.log('Forge rsa32: Key-pair created...');
      var encrypted = keypair.publicKey.encrypt('Helloyuihgfdcxsertyuihgfvdsetyhnvcdsaaa', 'RSA-OAEP');
      console.log(forge.util.encode64(encrypted));
      var content = keypair.privateKey.decrypt(encrypted, 'RSA-OAEP');
      console.log('Forge rsa33:' + content);
    });
  }

  randomTest() {
    var bytes = forge.random.getBytesSync(32);
    console.log('Forge randomTest:' + forge.util.bytesToHex(bytes));
  }

  aes() {
    var key = forge.random.getBytesSync(16);
    var iv = forge.random.getBytesSync(16);
    var cipher = forge.cipher.createCipher('AES-CBC', key);
    cipher.start({ iv: iv });
    cipher.update(forge.util.createBuffer('Hello'));
    cipher.finish();
    var encrypted = cipher.output;
    // outputs encrypted hex
    console.log('Forge aes1:' + encrypted.toHex());

    var decipher = forge.cipher.createDecipher('AES-CBC', key);
    decipher.start({ iv: iv });
    decipher.update(encrypted);
    decipher.finish();
    console.log('Forge aes2:' + decipher.output.toString());
  }

  aes2() {
    var key = forge.random.getBytesSync(16);
    var iv = forge.random.getBytesSync(16);
    var cipher = forge.cipher.createCipher('AES-GCM', key);
    cipher.start({
      iv: iv, // should be a 12-byte binary-encoded string or byte buffer
      additionalData: 'binary-encoded string', // optional
      tagLength: 128 // optional, defaults to 128 bits
    });
    cipher.update(forge.util.createBuffer('Hello'));
    cipher.finish();
    var encrypted = cipher.output;
    var tag = cipher.mode.tag;
    // outputs encrypted hex
    console.log('Forge aes21:' + encrypted.toHex());
    // outputs authentication tag
    console.log('Forge aes22:' + tag.toHex());

    // decrypt some bytes using GCM mode
    var decipher = forge.cipher.createDecipher('AES-GCM', key);
    decipher.start({
      iv: iv,
      additionalData: 'binary-encoded string', // optional
      tagLength: 128, // optional, defaults to 128 bits
      tag: tag // authentication tag from encryption
    });
    decipher.update(encrypted);
    var pass = decipher.finish();
    // pass is false if there was a failure (eg: authentication tag didn't match)
    if (pass) {
      // outputs decrypted hex
      console.log('Forge aes23:' + decipher.output.toString());
    }
  }

  RC2(){
    // generate a random key and IV
    var key = forge.random.getBytesSync(16);
    var iv = forge.random.getBytesSync(8);

    // encrypt some bytes
    var cipher = forge.rc2.createEncryptionCipher(key);
    cipher.start(iv);
    cipher.update(forge.util.createBuffer('Hellohello'));
    cipher.finish();
    var encrypted = cipher.output;
    // outputs encrypted hex
    console.log(encrypted.toHex());

    // decrypt some bytes
    var cipher = forge.rc2.createDecryptionCipher(key);
    cipher.start(iv);
    cipher.update(encrypted);
    cipher.finish();
    // outputs decrypted hex
    console.log(cipher.output.toHex());
    console.log(cipher.output.toString());
  }

  x509Test() {
    var pki = forge.pki;
    var keys = pki.rsa.generateKeyPair(2048);
    var cert = pki.createCertificate();
    cert.publicKey = keys.publicKey;
    console.log('Forge cert.version: ' + cert.version)

    cert.serialNumber = '01';
    cert.validity.notBefore = new Date();
    cert.validity.notAfter = new Date();
    cert.validity.notAfter.setFullYear(cert.validity.notBefore.getFullYear() + 1);
    var attrs = [{
                   name: 'commonName',
                   value: 'example.org'
                 }, {
                   name: 'countryName',
                   value: 'US'
                 }, {
                   shortName: 'ST',
                   value: 'Virginia'
                 }, {
                   name: 'localityName',
                   value: 'Blacksburg'
                 }, {
                   name: 'organizationName',
                   value: 'Test'
                 }, {
                   shortName: 'OU',
                   value: 'Test'
                 }];
    cert.setSubject(attrs);
    // alternatively set subject from a csr
    //cert.setSubject(csr.subject.attributes);
    cert.setIssuer(attrs);
    cert.setExtensions([{
                          name: 'basicConstraints',
                          cA: true
                        }, {
                          name: 'keyUsage',
                          keyCertSign: true,
                          digitalSignature: true,
                          nonRepudiation: true,
                          keyEncipherment: true,
                          dataEncipherment: true
                        }, {
                          name: 'extKeyUsage',
                          serverAuth: true,
                          clientAuth: true,
                          codeSigning: true,
                          emailProtection: true,
                          timeStamping: true
                        }, {
                          name: 'nsCertType',
                          client: true,
                          server: true,
                          email: true,
                          objsign: true,
                          sslCA: true,
                          emailCA: true,
                          objCA: true
                        }, {
                          name: 'subjectAltName',
                          altNames: [{
                                       type: 6, // URI
                                       value: 'http://example.org/webid#me'
                                     }, {
                                       type: 7, // IP
                                       ip: '127.0.0.1'
                                     }]
                        }, {
                          name: 'subjectKeyIdentifier'
                        }]);


    // self-sign certificate
    cert.sign(keys.privateKey);

    // convert a Forge certificate to PEM
    var pem = pki.certificateToPem(cert);
    console.log('Forge pem: ' + pem )
    // convert a Forge certificate from PEM
    var cert = pki.certificateFromPem(pem);
    console.log('Forge publicKey: ' + JSON.stringify(cert.publicKey))
    console.log('Forge privateKey: ' + cert.version)
  }

  x509() {
    var pki = forge.pki;
    // generate a keypair and create an X.509v3 certificate
    var keys = pki.rsa.generateKeyPair(512);
    var cert = pki.createCertificate();
    cert.publicKey = keys.publicKey;
//    console.debug("Forge9 - : "+JSON.stringify(keys.publicKey))
//    console.debug("Forge9 - : "+JSON.stringify(keys.privateKey))
    // alternatively set public key from a csr
    //cert.publicKey = csr.publicKey;
    // NOTE: serialNumber is the hex encoded value of an ASN.1 INTEGER.
    // Conforming CAs should ensure serialNumber is:
    // - no more than 20 octets
    // - non-negative (prefix a '00' if your value starts with a '1' bit)
    cert.serialNumber = '01';
    cert.validity.notBefore = new Date();
    cert.validity.notAfter = new Date();
    cert.validity.notAfter.setFullYear(cert.validity.notBefore.getFullYear() + 1);
    var attrs = [{
                   name: 'commonName',
                   value: 'example.org'
                 }, {
                   name: 'countryName',
                   value: 'US'
                 }, {
                   shortName: 'ST',
                   value: 'Virginia'
                 }, {
                   name: 'localityName',
                   value: 'Blacksburg'
                 }, {
                   name: 'organizationName',
                   value: 'Test'
                 }, {
                   shortName: 'OU',
                   value: 'Test'
                 }];
    cert.setSubject(attrs);
    // alternatively set subject from a csr
    //cert.setSubject(csr.subject.attributes);
    cert.setIssuer(attrs);
    cert.setExtensions([{
                          name: 'basicConstraints',
                          cA: true
                        }, {
                          name: 'keyUsage',
                          keyCertSign: true,
                          digitalSignature: true,
                          nonRepudiation: true,
                          keyEncipherment: true,
                          dataEncipherment: true
                        }, {
                          name: 'extKeyUsage',
                          serverAuth: true,
                          clientAuth: true,
                          codeSigning: true,
                          emailProtection: true,
                          timeStamping: true
                        }, {
                          name: 'nsCertType',
                          client: true,
                          server: true,
                          email: true,
                          objsign: true,
                          sslCA: true,
                          emailCA: true,
                          objCA: true
                        }, {
                          name: 'subjectAltName',
                          altNames: [{
                                       type: 6, // URI
                                       value: 'http://example.org/webid#me'
                                     }, {
                                       type: 7, // IP
                                       ip: '127.0.0.1'
                                     }]
                        }, {
                          name: 'subjectKeyIdentifier'
                        }]);
    cert.sign(keys.privateKey);

    // convert a Forge certificate to PEM
    var pem = pki.certificateToPem(cert);
//    var publicKey = pki.publicKeyFromPem(pem)
//    console.log('Forge x509:' + publicKey )
    console.log('Forge x509:' + pem + '')

    // convert a Forge certificate from PEM
//    var cert = pki.certificateFromPem(pem);
//    console.log('Forge x509:' + pki.publicKeyToPem(publicKey))
  }
}