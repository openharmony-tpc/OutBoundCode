/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Encoding } from './Encoding';
import { WordArray } from './lib-WordArray';

export class Utf16LE implements Encoding {
  /**
   * Converts a word array to a UTF-16 LE string.
   *
   * @param wordArray The word array.
   *
   * @return The UTF-16 LE string.
   *
   * @example
   *
   *     let utf16String = Utf16LE.stringify(wordArray);
   */
  public stringify(wordArray: WordArray): string {
    // Shortcuts
    var words = wordArray.words;
    var sigBytes = wordArray.sigBytes;

    // Convert
    var utf16Chars = [];
    for (var i = 0; i < sigBytes; i += 2) {
      var codePoint = this.swapEndian((words[i >>> 2] >>> (16 - (i % 4) * 8)) & 0xffff);
      utf16Chars.push(String.fromCharCode(codePoint));
    }

    return utf16Chars.join('');
  }

  /**
   * Converts a UTF-16 LE string to a word array.
   *
   * @param utf16Str The UTF-16 LE string.
   *
   * @return The word array.
   *
   * @example
   *
   *     let wordArray = Utf16LE.parse(utf16String);
   */
  public parse(utf16Str: string): WordArray {
    // Shortcut
    var utf16StrLength = utf16Str.length;

    // Convert
    var words = [];
    for (var i = 0; i < utf16StrLength; i++) {
      words[i >>> 1] |= this.swapEndian(utf16Str.charCodeAt(i) << (16 - (i % 2) * 16));
    }

    return WordArray.create(words, utf16StrLength * 2);
  }

  swapEndian(word) {
    return ((word << 8) & 0xff00ff00) | ((word >>> 8) & 0x00ff00ff);
  }
}