/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { WordArray } from './lib-WordArray';
import { RC4,generateKeystreamWord } from './RC4';
import { BufferedBlockAlgorithmConfig } from './BufferedBlockAlgorithmConfig';

export class RC4Drop extends RC4 {

  public constructor(xformMode: number, key: WordArray, cfg?: BufferedBlockAlgorithmConfig) {
    super(xformMode, key, cfg);
    this.cfg = Object.assign({
      drop: 192
    }, cfg);
    this._doReset()
  }

  _doReset() {
    super._doReset();
    // Drop
    for (var i = this.cfg.drop; i > 0; i--) {
      generateKeystreamWord.call(this);
    }
  }
}