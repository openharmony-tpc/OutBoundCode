
import { BlockCipherMode } from './AES/mode/BlockCipherMode';
import { WordArray } from './lib-WordArray';
import { KDF } from './AES/kdf/KDF';
import {Local_Formatter} from "./AES/format/Local_Formatter"
import { EPadding } from '../ets/AES/pad/EPadding';

export interface BufferedBlockAlgorithmConfig {
    // requires at least a blockSize
    blockSize?: number;

    iv?: WordArray;

    format?: Local_Formatter;

    kdf?: KDF;

    mode?: typeof BlockCipherMode;

    padding?: EPadding;

    drop?: number
}