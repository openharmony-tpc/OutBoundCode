
import { WordArray } from './lib-WordArray';

export interface KDF {
    execute1(password: string, keySize: number, ivSize: number, salt?: WordArray | string) ;

}