
import { Hasher } from './AES/lib/Hasher'
import { Utf8 } from './Utf8'
import { WordArray } from './lib-WordArray'
export class HMAC{
	_hasher:Hasher
	_oKey:WordArray
	_iKey:WordArray
	constructor(hasher:Hasher, key){
		this._hasher =hasher;

		// Convert string to WordArray, else assume WordArray already
		if (typeof key == 'string') {
			key = Utf8.parse(key);
		}

		// Shortcuts
		var hasherBlockSize = hasher.cfg.blockSize;
		var hasherBlockSizeBytes = hasherBlockSize * 4;

		// Allow arbitrary length keys
		if (key.sigBytes > hasherBlockSizeBytes) {
			key = hasher.finalize(key);
		}

		// Clamp excess bits
		key.clamp();

		// Clone key for inner and outer pads
		var oKey = this._oKey = key.clone();
		var iKey = this._iKey = key.clone();

		// Shortcuts
		var oKeyWords = oKey.words;
		var iKeyWords = iKey.words;

		// XOR keys with pad constants
		for (var i = 0; i < hasherBlockSize; i++) {
			oKeyWords[i] ^= 0x5c5c5c5c;
			iKeyWords[i] ^= 0x36363636;
		}
		oKey.sigBytes = iKey.sigBytes = hasherBlockSizeBytes;
		// Set initial values
		this.reset();
	}
	static create(hasher:Hasher, key):HMAC{
		return new HMAC(hasher,key)
	}
	reset() {
		// Shortcut
		var hasher = this._hasher;

		// Reset
		hasher.reset();
		hasher.update(this._iKey);
	}
	update(messageUpdate: WordArray | string):HMAC {
		this._hasher.update(messageUpdate);
		// Chainable
		return this;
	}

	/**
   * Finalizes the HMAC computation.
   * Note that the finalize operation is effectively a destructive, read-once operation.
   *
   * @param {WordArray|string} messageUpdate (Optional) A final message update.
   *
   * @return {WordArray} The HMAC.
   *
   * @example
   *
   *     var hmac = hmacHasher.finalize();
   *     var hmac = hmacHasher.finalize('message');
   *     var hmac = hmacHasher.finalize(wordArray);
   */
	finalize(messageUpdate: WordArray | string): WordArray {
		// Shortcut
		var hasher = this._hasher;

		// Compute HMAC
		var innerHash = hasher.finalize(messageUpdate);
		hasher.reset();
		var hmac = hasher.finalize(this._oKey.clone().concat(innerHash));

		return hmac;
	}

}