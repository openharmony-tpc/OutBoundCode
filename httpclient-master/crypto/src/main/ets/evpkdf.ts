
import { WordArray } from './lib-WordArray';
import { Hasher } from './AES/lib/Hasher';
import { MD5 } from './MyMD5';

export interface OptionalEvpKDFConfig {
    keySize?: number;
    hasher?: typeof Hasher;
    iterations?: number;
}

export interface EvpKDFConfig extends OptionalEvpKDFConfig {
    keySize: number;
    hasher: typeof Hasher;
    iterations: number;
}

export class EvpKDF {
    public cfg: EvpKDFConfig;

    /**
     * Initializes a newly created key derivation function.
     *
     * @param cfg (Optional) The configuration options to use for the derivation.
     *
     * @example
     *
     *     let kdf = EvpKDF.create();
     *     let kdf = EvpKDF.create({ keySize: 8 });
     *     let kdf = EvpKDF.create({ keySize: 8, iterations: 1000 });
     */
    constructor(cfg?: OptionalEvpKDFConfig) {
        this.cfg = Object.assign({
            keySize: 128 / 32,
            hasher: MD5,
            iterations: 1
        }, cfg);
    }
    public static create(cfg?: OptionalEvpKDFConfig):EvpKDF{
        return new EvpKDF(cfg)
    }

    /**
     * Derives a key from a password.
     *
     * @param password The password.
     * @param salt A salt.
     *
     * @return The derived key.
     *
     * @example
     *
     *     let key = kdf.compute(password, salt);
     */
    compute(password: WordArray | string, salt: WordArray | string): WordArray {
        // Shortcut
        let cfg = this.cfg;

        // Init hasher
        const hasher:Hasher = new (<any> cfg.hasher)();

        // Initial values
        let derivedKey = WordArray.create();

        // Shortcuts
        let derivedKeyWords = derivedKey.words;
        let keySize = cfg.keySize;
        let iterations = cfg.iterations;

        // Generate key
        while (derivedKeyWords.length < keySize) {
            if (block) {
                hasher.update(block);
            }
            var block = hasher.update(password).finalize(salt);
            hasher.reset();

            // Iterations
            for (let i = 1; i < iterations; i++) {
                block = hasher.finalize(block);
                hasher.reset();
            }

            derivedKey.concat(block);
        }
        derivedKey.sigBytes = keySize * 4;

        return derivedKey;
    }
}