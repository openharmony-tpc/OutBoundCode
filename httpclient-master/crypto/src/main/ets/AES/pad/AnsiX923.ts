
import { WordArray } from '../../lib-WordArray';
import { EPadding } from '../pad/EPadding';

export class AnsiX923 {
  public static pad(data: WordArray, blockSize: number): void {
    // Shortcuts
    var dataSigBytes = data.sigBytes;
    var blockSizeBytes = blockSize * 4;

    // Count padding bytes
    var nPaddingBytes = blockSizeBytes - dataSigBytes % blockSizeBytes;

    // Compute last byte position
    var lastBytePos = dataSigBytes + nPaddingBytes - 1;

    // Pad
    data.clamp();
    data.words[lastBytePos >>> 2] |= nPaddingBytes << (24 - (lastBytePos % 4) * 8);
    data.sigBytes += nPaddingBytes;
  }

  public static unpad(data: WordArray): void {
    // Get number of padding bytes from last byte
    var nPaddingBytes = data.words[(data.sigBytes - 1) >>> 2] & 0xff;

    // Remove padding
    data.sigBytes -= nPaddingBytes;
  }
}
const _: EPadding = AnsiX923;