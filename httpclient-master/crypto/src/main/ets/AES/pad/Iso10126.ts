
import { WordArray } from '../../lib-WordArray';
import { EPadding } from '../pad/EPadding';

export class Iso10126 {
  public static pad(data: WordArray, blockSize: number): void {
    // Shortcut
    var blockSizeBytes = blockSize * 4;

    // Count padding bytes
    var nPaddingBytes = blockSizeBytes - data.sigBytes % blockSizeBytes;

    // Pad
    data.concat(WordArray.random(nPaddingBytes - 1)).
    concat(WordArray.create([nPaddingBytes << 24], 1));
  }

  public static unpad(data: WordArray): void {
    // Get number of padding bytes from last byte
    var nPaddingBytes = data.words[(data.sigBytes - 1) >>> 2] & 0xff;

    // Remove padding
    data.sigBytes -= nPaddingBytes;
  }
}
const _: EPadding = Iso10126;