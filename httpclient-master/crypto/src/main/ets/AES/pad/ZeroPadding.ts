
import { WordArray } from '../../lib-WordArray';
import { EPadding } from '../pad/EPadding';

export class ZeroPadding {
  public static pad(data: WordArray, blockSize: number): void {
    // Shortcut
    var blockSizeBytes = blockSize * 4;

    // Pad
    data.clamp();
    data.sigBytes += blockSizeBytes - ((data.sigBytes % blockSizeBytes) || blockSizeBytes);
  }

  public static unpad(data: WordArray): void {
    // Shortcut
    var dataWords = data.words;

    // Unpad
    var i = data.sigBytes - 1;
    for (var i = data.sigBytes - 1; i >= 0; i--) {
      if (((dataWords[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff)) {
        data.sigBytes = i + 1;
        break;
      }
    }
  }
}
const _: EPadding = ZeroPadding;