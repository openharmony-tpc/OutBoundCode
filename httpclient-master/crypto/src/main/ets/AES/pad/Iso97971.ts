
import { ZeroPadding } from './ZeroPadding';
import { WordArray } from '../../lib-WordArray';
import { EPadding } from '../pad/EPadding';

export class Iso97971{
  /**
   * Doesn't pad the data provided.
   *
   * @param data The data to pad.
   * @param blockSize The multiple that the data should be padded to.
   *
   * @example
   *
   *     NoPadding.pad(wordArray, 4);
   */
  public static pad(data: WordArray, blockSize: number): void {
    data.concat(WordArray.create([0x80000000], 1));
    // Zero pad the rest
    ZeroPadding.pad(data, blockSize);
  }

  /**
   * Doesn't unpad the data provided.
   *
   * @param data The data to unpad.
   *
   * @example
   *
   *     NoPadding.unpad(wordArray);
   */
  public static unpad(data: WordArray): void {
    // Remove zero padding
    ZeroPadding.unpad(data);

    // Remove one more byte -- the 0x80 byte
    data.sigBytes--;
  }
}

const _: EPadding = Iso97971;