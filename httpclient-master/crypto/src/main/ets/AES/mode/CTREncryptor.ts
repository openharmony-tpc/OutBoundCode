/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BlockCipherModeAlgorithm } from './BlockCipherModeAlgorithm';

export class CTREncryptor extends BlockCipherModeAlgorithm {
  private _counter
  public processBlock(words: Array<number>, offset: number) {
    // Shortcuts
    var cipher = this._cipher
    var blockSize = cipher.cfg.blockSize;
    var iv = this._iv;
    var counter = this._counter;

    if (blockSize == 0 || blockSize == undefined) {
      blockSize = 1;
    }
    // Generate keystream
    if (iv) {
      counter = this._counter = iv.slice(0);

      // Remove IV for subsequent blocks
      this._iv = undefined;
    }
    var keystream = counter.slice(0);
    cipher.encryptBlock(keystream, 0);

    // Increment counter
    counter[blockSize - 1] = (counter[blockSize - 1] + 1) | 0

    // Encrypt
    for (var i = 0; i < blockSize; i++) {
      words[offset + i] ^= keystream[i];
    }
  }
}