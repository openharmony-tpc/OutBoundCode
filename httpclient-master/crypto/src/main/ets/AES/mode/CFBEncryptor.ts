/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BlockCipherModeAlgorithm } from './BlockCipherModeAlgorithm';

export class CFBEncryptor extends BlockCipherModeAlgorithm {
  private _prevBlock
  /**
   * Processes the data block at offset.
   *
   * @param words The data words to operate on.
   * @param offset The offset where the block starts.
   *
   * @example
   *
   *     mode.processBlock(data.words, offset);
   */
  public processBlock(words: Array<number>, offset: number) {
    // Shortcuts
    var cipher = this._cipher;
    var blockSize = this._cipher.cfg.blockSize;

    if (blockSize == 0 || blockSize == undefined) {
      blockSize = 1;
    }

    this.generateKeystreamAndEncrypt.call(this, words, offset, blockSize, cipher);

    // Remember this block to use with next block
    this._prevBlock = words.slice(offset, offset + blockSize);
  }

  generateKeystreamAndEncrypt(words, offset, blockSize, cipher) {
    var keystream;

    // Shortcut
    var iv = this._iv;

    // Generate keystream
    if (iv) {
      keystream = iv.slice(0);

      // Remove IV for subsequent blocks
      this._iv = undefined;
    } else {
      keystream = this._prevBlock;
    }
    cipher.encryptBlock(keystream, 0);

    // Encrypt
    for (var i = 0; i < blockSize; i++) {
      words[offset + i] ^= keystream[i];
    }
  }
}