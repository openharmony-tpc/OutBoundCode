/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BlockCipherModeAlgorithm } from './BlockCipherModeAlgorithm';

export class CTRGladmanEncryptor extends BlockCipherModeAlgorithm {
  private _counter
  /**
   * Processes the data block at offset.
   *
   * @param words The data words to operate on.
   * @param offset The offset where the block starts.
   *
   * @example
   *
   *     mode.processBlock(data.words, offset);
   */
  public processBlock(words: Array<number>, offset: number) {
    // Shortcuts
    var cipher = this._cipher
    var blockSize = this._cipher.cfg.blockSize;
    var iv = this._iv;
    var counter = this._counter;

    if (blockSize == 0 || blockSize == undefined) {
      blockSize = 1;
    }

    // Generate keystream
    if (iv) {
      counter = this._counter = iv.slice(0);

      // Remove IV for subsequent blocks
      this._iv = undefined;
    }

    this.incCounter(counter);

    var keystream = counter.slice(0);
    cipher.encryptBlock(keystream, 0);

    // Encrypt
    for (var i = 0; i < blockSize; i++) {
      words[offset + i] ^= keystream[i];
    }
  }

  incCounter(counter) {
    if ((counter[0] = this.incWord(counter[0])) === 0) {
      // encr_data in fileenc.c from  Dr Brian Gladman's counts only with DWORD j < 8
      counter[1] = this.incWord(counter[1]);
    }
    return counter;
  }

  incWord(word) {
    if (((word >> 24) & 0xff) === 0xff) { //overflow
      var b1 = (word >> 16) & 0xff;
      var b2 = (word >> 8) & 0xff;
      var b3 = word & 0xff;

      if (b1 === 0xff) // overflow b1
      {
        b1 = 0;
        if (b2 === 0xff) {
          b2 = 0;
          if (b3 === 0xff) {
            b3 = 0;
          }
          else {
            ++b3;
          }
        }
        else {
          ++b2;
        }
      }
      else {
        ++b1;
      }

      word = 0;
      word += (b1 << 16);
      word += (b2 << 8);
      word += b3;
    }
    else {
      word += (0x01 << 24);
    }
    return word;
  }
}