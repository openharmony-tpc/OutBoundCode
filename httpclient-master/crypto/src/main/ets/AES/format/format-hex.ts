
import { Hex } from '../../enc-hex';
import { CipherParams } from '../lib/CipherParams';
export class HexFormatter{
   static stringify(cipherParams:CipherParams):string {
        return cipherParams.ciphertext.toString(Hex);
    }
   static parse(input:string) :CipherParams{
        var ciphertext = Hex.parse(input);
        return CipherParams.create({ ciphertext: ciphertext });
    }
}


