
import { CipherParams } from '../lib/CipherParams';

export interface Local_Formatter {
  stringify: (cipherParams: CipherParams) => string;

  parse: (paramsStr: string) => CipherParams;
}