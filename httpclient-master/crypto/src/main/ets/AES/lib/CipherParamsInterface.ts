
import { WordArray } from '../../lib-WordArray';
import { Cipher } from '../lib/Cipher';
import { BlockCipherMode } from '../mode/BlockCipherMode';
import { EPadding } from '../pad/EPadding';
import { Local_Formatter } from '../format/Local_Formatter';

export interface CipherParamsInterface {
  ciphertext?: WordArray;

  key?: WordArray | string;

  iv?: WordArray;

  salt?: WordArray | string;

  algorithm?: typeof Cipher;

  mode?: typeof BlockCipherMode;

  padding?: EPadding;

  blockSize?: number;

  formatter?: Local_Formatter;
}