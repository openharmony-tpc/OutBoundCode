
import jsSHA from "jssha"

export class sha384 {
  hex_sha384(s) {
    const shaObj = new jsSHA("SHA-384", "TEXT", { encoding: "UTF8" });
    shaObj.update(s);
    const hash = shaObj.getHash("HEX");
    return hash;
  }

  hex_hmac_sha384(k, d) {
    const shaObj = new jsSHA("SHA-384", "TEXT", {
      hmacKey: { value: d, format: "TEXT", encoding: "UTF8" },
    });
    shaObj.update(k);
    const hmac = shaObj.getHash("HEX");
    return hmac;
  }
}