import { WordArray } from "./lib-WordArray"
class X64Word {
     high: number
     low: number

     constructor(high?, low?) {
        this.high = high;
        this.low = low;
    }
}

class X64WordArray {
    words: WordArray
    sigBytes: number

    init(words, sigBytes) {
        words = this.words = words || [];
        if (sigBytes != undefined) {
            this.sigBytes = sigBytes;
        } else {
            this.sigBytes = words.length * 8;
        }
    }

    toX32() {
        // Shortcuts
        var x64Words = this.words;
        var x64WordsLength = x64Words.words.length;

        // Convert
        var x32Words = [];
        for (var i = 0; i < x64WordsLength; i++) {
            var x64Word = x64Words[i];
            x32Words.push(x64Word.high);
            x32Words.push(x64Word.low);
        }

        return new WordArray(x32Words, this.sigBytes);
    }
}

export  {X64Word,X64WordArray}



