
import jsSHA from "jssha"

export class sha224 {
  hex_sha224(s) {
    const shaObj = new jsSHA("SHA-224", "TEXT", { encoding: "UTF8" });
    shaObj.update(s);
    const hash = shaObj.getHash("HEX");
    return hash;
  }

  hex_hmac_sha224(k, d) {
    const shaObj = new jsSHA("SHA-224", "TEXT", {
      hmacKey: { value: d, format: "TEXT", encoding: "UTF8" },
    });
    shaObj.update(k);
    const hmac = shaObj.getHash("HEX");
    return hmac;
  }
}