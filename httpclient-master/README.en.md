

## httpclient

## Introduction

HTTP is the primary way that modern applications exchange data and media over the network. httpclient is an efficient HTTP client in OpenHarmony, using it makes your content load faster and saves your traffic. httpclient is based on the well-known OKHTTP, integrates the functional characteristics of android-async-http, AutobahnAndroid, OkGo and other libraries, and is committed to creating an efficient, easy-to-use and comprehensive network request library in OpenHarmony. The current version of httpclient relies on the network request capabilities and upload and download capabilities provided by the system, and expands and develops on this basis. The implemented functional characteristics are as follows：

1.Support global configuration debugging switch, timeout, public request header and request parameters, etc., and support chain calls.

2.You can use the okio library to optimize data transmission, and use annotations to define interfaces with retrofit to make your code look more concise and beautiful.

3.The custom task scheduler maintains the task queue to process synchronous/asynchronous requests, and supports tag cancellation requests.

4.Support setting custom interceptor.

5.Support redirection.

6.Support client decompression.

7.Support file upload and download.

8.Support cookie management.



## Download and install

```javascript
npm install @ohos/httpclient --save
```

For more information on OpenHarmony npm environment configuration, please refer to [How to install OpenHarmony npm package](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md)



## Instructions for use
```
import httpclient from '@ohos/httpclient';
```

Get the HttpClient object and configure the timeout

```javascript
this.client =new httpclient.HttpClient.Builder()
    .setConnectTimeout(10000)
    .setReadTimeout(10000)
    .setWriteTimeout(10000)
    .build();

var status :string= '' // response code
var content:string= '' // response content
```

### GET request method example

  ```javascript
//Configure request parameters.
let request = new httpclient.Request.Builder()
              .get("https://postman-echo.com/get?foo1=bar1&foo2=bar2")
              .addHeader("Content-Type", "application/json")
              .params("testKey1", "testValue1")
              .params("testKey2", "testValue2")
              .build();
//Initiate a request.
  this.client.newCall(request).enqueue((result) => {
              if (result.response) {
                this.status = ""+result.response.responseCode;
              }
              if (result.response.result) {
                this.content = result.response.result;
              } else {
                this.content = JSON.stringify(result.response);
              }
              Log.showInfo("onComplete -> Status : " + this.status);
              Log.showInfo("onComplete -> Content : " + JSON.stringify(this.content));
            }, (error)=> {
              this.status = "";
              this.content = error.data;
              Log.showInfo("onError -> Error : " + this.content);
            });
          }) 
  ```

### POST request method example

```javascript
 let request = new httpclient.Request.Builder()
              .url("https://postman-echo.com/post")
              .post(httpclient.RequestBody.create("test123"))
              .addHeader("Content-Type", "application/json")
              .build();

 this.client.newCall(request).execute().then((result) => {
              if (result.response) {
                this.status = ""+result.response.responseCode;
              }
              if (result.response.result) {
                this.content = result.response.result;
              } else {
                this.content = JSON.stringify(result.response);
              }
              Log.showInfo("onComplete -> Status : " + this.status);
              Log.showInfo("onComplete -> Content : " + JSON.stringify(this.content));
            }).catch((error)=> {
              this.status = "";
              this.content = error.data;
              Log.showInfo("onError -> Error : " + this.content);
            });
        })
```

### POST request method with two parameters example

  ```javascript
  let request = new httpclient.Request.Builder()
              .url("https://postman-echo.com/post")
              .post(httpclient.RequestBody.create({
                a: 'a1', b: 'b1'
              }, new httpclient.Mime.Builder()
              .contentType('application/json', 'charset', 'utf8').build().getMime()))
              .build();
  //Initiate a synchronization request.
  this.client.newCall(request).execute().then((result) => {
              if (result.response) {
                this.status = ""+result.response.responseCode;
              }
              if (result.response.result) {
                this.content = result.response.result;
              } else {
                this.content = JSON.stringify(result.response);
              }
              Log.showInfo("onComplete -> Status : " + this.status);
              Log.showInfo("onComplete -> Content : " + JSON.stringify(this.content));
            }).catch((error)=> {
              this.status = "";
              this.content = error.data;
              Log.showInfo("onError -> Error : " + this.content);
            });
  ```

### POST request method using FormEncoder example

  ```javascript
  let formEncoder = new httpclient.FormEncoder.Builder()
              .add('key1', 'value1')
              .add('key2', 'value2')
              .build();
            let feBody = formEncoder.createRequestBody();
            var request = new httpclient.Request.Builder()
              .url("https://postman-echo.com/post")
              .post(feBody)
              .build();

  this.client.newCall(request).execute().then((result) => {
              if (result.response) {
                this.status = ""+result.response.responseCode;
              }
              if (result.response.result) {
                this.content = result.response.result;
              } else {
                this.content = JSON.stringify(result.response);
              }
              Log.showInfo("onComplete -> Status : " + this.status);
              Log.showInfo("onComplete -> Content : " + JSON.stringify(this.content));
            }).catch((error)=> {
              this.status = "";
              this.content = error.data;
              Log.showInfo("onError -> Error : " + this.content);
            });
  ```

### Asynchronous upload example

You need to get the context object used globally in AbilityStage or Ability

```javascript
export default class MainAbility extends Ability {
    onCreate(want, launchParam) {
        console.log("[Demo] MainAbility onCreate")
        globalThis.abilityWant = want;
    }
    onWindowStageCreate(windowStage) {
      // Main window is created, set main page for this ability
      console.log("[Demo] MainAbility onWindowStageCreate")
      // Define the Context used globally 
      globalThis.abilityContext = this.context ; // Define the cache path used globally /data/storage/el2/base/haps/entry/cache 
      // Define the global Cache path to use /data/storage/el2/base/haps/entry/files 
      globalThis.cacheDir = this.context.cacheDir 
      globalThis.filesDir = this.context.filesDir;
      windowStage.setUIContent ( this.context , "pages/index", null)
    }
};
```

Obtain the path of the uploaded file and generate the uploaded file (this step can be omitted or imported into the device through the command), and the path of the uploaded file can be processed at the same time

  ```javascript
	 const ctx = this
     Log.showInfo(" cacheDir   " + globalThis.cacheDir)
     let filePath = globalThis.cacheDir + this.fileName;
     Log.showInfo("   filePath   " + filePath)
     let fd = fileIO.openSync(filePath, 0o102, 0o666)
     fileIO.ftruncateSync(fd)
     fileIO.writeSync(fd, "test httpclient")
     fileIO.fsyncSync(fd)
     fileIO.closeSync(fd)

    Log.showInfo(" writeSync    ");
    Log.showInfo( "create file success   ")
	
   // Since file upload currently only supports "dataability" and "internal" protocol       //types,  but "internal" only supports temporary directories, Example        //internal://cache/path/to/file.txt , So you need to convert the obtained file 
// path into a path in internal format

    filePath = filePath.replace(globalThis.cacheDir, "internal://cache");

  ```

start upload

```javascript
    //Generate file upload object and wrap parameters 
    let fileUploadBuilder = new httpclient.FileUpload.Builder()
      .addFile(filePath)
      .addData("name2", "value2")
      .build();
    let fileObject = fileUploadBuilder.getFile();
    let dataObject = fileUploadBuilder.getData();
    
    Log.showInfo( "fileObject:   "+JSON.stringify(fileObject));
    Log.showInfo( "dataObject:   "+JSON.stringify(dataObject));
    Log.showInfo('cacheDir  = ' + globalThis.abilityContext.cacheDir);
    Log.showInfo('filesDir  = ' + globalThis.abilityContext.filesDir);
    Log.showInfo("type of :"+ typeof globalThis.abilityContext)

    //Generate upload parameters.
    let request = new httpclient.Request.Builder()
      .url(this.fileServer)
      .addFileParams(fileObject, dataObject)
      .setAbilityContext(globalThis.abilityContext)
      .build();

    this.client.newCall(request).execute().then((data) => {
        // upload progress callback to monitor
      data.on('progress', (uploadedSize, totalSize) => {
        Log.showInfo('progress--->uploadedSize: ' + uploadedSize 
                     + ' ,totalSize--->' + totalSize);
        if (uploadedSize == totalSize){
			 Log.showInfo("upload success")
        }
      })
      //Upload complete callback to monitor
      data.on('headerReceive', (headers) => {
        Log.showInfo( 'progress--->uploadSize: ' + JSON.stringify(headers));
      })
    }).catch((error)=> {
      this.status = "";
      this.content = error.data;
      Log.showInfo("onError -> Error : " + this.content);
    });
```

### Example of PUT request

  ```javascript
let request = new httpclient.Request.Builder()
              .url("https://postman-echo.com/put")
              .put(httpclient.RequestBody.create({
                a: 'a1', b: 'b1'
              }, new httpclient.Mime.Builder()
              .contentType('application/json', 'charset', 'utf8').build()))
              .build();

 this.client.newCall(request).execute().then((result) => {
              if (result.response) {
                this.status = ""+result.response.responseCode;
              }
              if (result.response.result) {
                this.content = result.response.result;
              } else {
                this.content = JSON.stringify(result.response);
              }
              Log.showInfo("onComplete -> Status : " + this.status);
              Log.showInfo("onComplete -> Content : " + JSON.stringify(this.content));
            }).catch((error) => {
              this.status = "";
              this.content = error.data;
              Log.showInfo("onError -> Error : " + this.content);
            });
  ```

### DELETE request example

  ```javascript
let request = new httpclient.Request.Builder()
              .url("https://reqres.in/api/users/2")
              .delete()
              .build();

            this.client.newCall(request).execute().then((result) => {
              if (result.response) {
                this.status = ""+result.response.responseCode;
              }
              if (result.response.result) {
                this.content = result.response.result;
              } else {
                this.content = JSON.stringify(result.response);
              }
              Log.showInfo("onComplete -> Status : " + this.status);
              Log.showInfo("onComplete -> Content : " + JSON.stringify(this.content));
            }).catch((error) => {
              this.status = "";
              this.content = error.data;
              Log.showInfo("onError -> Error : " + this.content);
            });
  ```

### File Download Request Example

  ```javascript
 try {
      this.status = "";
      let fName ="sampleEnqueue.jpg";
     
  //request can  set the download name fName,  the downloaded files are saved in the files  folder by default
      var request = new httpclient.Request.Builder()
      .download("https://imgkub.com/images/2022/03/09/pexels-francesco-ungaro-15250411.jpg", fName)
      .setAbilityContext(globalThis.abilityContext)
      .build();
      //Initiate a request
      this.client.newCall(request).enqueue((downloadTask) => {
           //Set the download completion monitor
           	downloadTask.on('complete', () => {
           	Log.showInfo(" download complete");
           	this.content = "Download Task Completed";
           	});
            //Configure download progress monitoring.
          	 downloadTask.on("progress", ( receivedSize, totalSize)=>{
          	 Log.showInfo(" downloadSize : "+receivedSize+" totalSize : "+totalSize);
           	 this.content = ""+(receivedSize/totalSize)*100;
           	});
           }, (error)=> {
                this.status = "";
                this.content = error.data;
                Log.showError("onError -> Error : " + this.content);
              });
            } catch (err) {
              Log.showError(" execution failed - errorMsg : "+err);
            }
  ```

### Examples of cookie usage

Initialize CookieJar, CookieManager, CookieStore related to cookie management

```javascript
cookieJar = new httpclient.CookieJar(); // Initialize cookie access verification tool 
// Initialize cookie policy management tool
cookieManager = new httpclient.CookieManager(); 
// Initialize cookie Storage
store = new httpclient.CookieStore(globalThis.cacheDir); 
```

Set the cookie policy to ACCEPT_ALL or ACCEPT_ORIGINAL_SERVER

```javascript
this.cookieManager.setCookiePolicy(httpclient.CookiePolicy.ACCEPT_ALL);
                this.cookieJar.setCookieStore(this.store);
// this.cookieManager.setCookiePolicy(httpclient.CookiePolicy.ACCEPT_ORIGINAL_SERVER);
```

Configure cookie management and start requests

```javascript
 		var self = this;
          //Set request parameters and configure cookie management tools
          let request1 = new httpclient.Request.Builder()
           .get()
           .url(this.yourServer)
           .tag("tag_cookie1")
           .setCookieJar(this.cookieJar)
           .setAbilityContext(globalThis.abilityContext)
           .setCookieManager(this.cookieManager)
           .addHeader("Content-Type", "application/json")
           .build();
                  
           self.client.newCall(request1).enqueue((result) => {
               if (result.response) {
                   this.status = "" + result.response.responseCode;
               }
               if (result.response.result) {
                   this.content = result.response.result;
               } else {
                    this.content = JSON.stringify(result.response);
               }
               Log.showInfo("onComplete cookie-> Status : " + this.status);
               Log.showInfo("onComplete cookie-> Content : " 
                  + JSON.stringify(this.content));
            }, (error) => {
               this.status = "";
               this.content = error.data;
               Log.showInfo("onError -> Error : " + this.content);
           });
```

###  Interceptor usage example

```javascript
// Add interceptors through addInterceptor, you can add one or two, and allow to be //empty. The first one is the request interceptor, the second one is the response //interceptor 
// addInterceptor allows multiple calls, adding multiple interceptors, the calling order //of the interceptors is determined according to the order of addition 

let request = new httpclient.Request.Builder()
           		.url('https://postman-echo.com/post')
            	.post()
         		.body(httpclient.RequestBody.create('test123'))
         		.setDefaultConfig(defaultConfigJSON)
         		.addInterceptor((req) => {
                  var route = new httpclient.Route(this.info);
                  // Now we have got the interceptor, now we can connect to
                  // IPAddress one after the other if previous one fails
                  Log.showInfo("onDNSRequest and route" + route);
                  return req;
                }, (res) => {
                  return res;
                })
           		.build();
           		
   this.client.newCall(request).execute().then((result) => {
              if (result.response) {
                this.status = ""+result.response.responseCode;
              }
              if (result.response.result) {
                this.content = result.response.result;
              } else {
                this.content = JSON.stringify(result.response);
              }
              Log.showInfo("onComplete -> Status : " + this.status);
              Log.showInfo("onComplete -> Content : " + JSON.stringify(this.content));
            }).catch((error) => {
              this.status = "";
              this.content = error.data;
              Log.showInfo("onError -> Error : " + this.content);
            }); 
```
### Examples of Encryption/Decryption usage

Initialize cipher, initialization vector (or IV), key and configuration parameters related to encryption  management

```javascript
 aes = BlockCipher._createHelper(AES);
 iv = Hex.parse('101112131415161718191a1b1c1d1e1f');
 key= Hex.parse('000102030405060708090a0b0c0d0e0f');
 aesConfig = {
     mode: CBC,
     padding: PKCS7,
     iv: this.iv
 }
```



Use encryption/Decryption in requests/response

```javascript
    var request = new httpclient.Request.Builder()
    .post()
    .body(httpclient.RequestBody.create("test123"))
    .url(this.echoServer)
    .addInterceptor((req) => {
        Log.showInfo("inside AES interceptor request" + JSON.stringify(req.body.content))
        let body = Utf8.prototype.parse(req.body.content)
        let value = this.aes.encrypt(
        Hex.parse(body.toString()),
            this.key, this.aesConfig
        ).ciphertext!.toString()
        Log.showInfo("AES encrypt = " + value);
        req.body.content = value;
        return req;
    }, (res) => {
        Log.showInfo("inside AES interceptor response")
        let body = Hex.parse(res.result)
        let value = this.aes.decrypt(
            new CipherParams({
                ciphertext: body
            }),
            this.key, this.aesConfig
        ).toString()
        console.log("AES decrypt = " + this.hex_to_ascii(value));
        res.result = this.hex_to_ascii(value);
        return res;
    })
    .build();

        this.client.newCall(request).execute().then((result) => {
            if (result.response) {
                this.status = result.response.responseCode + "";
            }
            if (result.response.result)
            this.content = result.response.result;
            else
            this.content = JSON.stringify(result.response);
        }).catch((error) => {
            this.content = JSON.stringify(error);
        });
```
### Examples of Multipart/form-data usage

Create the RequestBody and initialize the MultiPart builder with the requestBody created 

```javascript
let requestBody1 = httpclient.RequestBody.create({Title: 'Multipart', Color: 'Brown'},new httpclient.Mime.Builder().contentDisposition('form-data; name="myfile"').contentType('text/plain', 'charset', 'utf8').build().getMime())
let requestBody2 = httpclient.RequestBody.create("Okhttp",new httpclient.Mime.Builder().contentDisposition('form-data; name="http"').contentType('text/plain', 'charset', 'utf8').build().getMime())
let requestBody3 = httpclient.RequestBody.create(data,new httpclient.Mime.Builder().contentDisposition('form-data; name="file";filename="httpclient.txt"').contentType('text/plain', 'charset', 'utf8').build().getMime())
let boundary = "AaB03x";

let multiPartObj = new httpclient.MultiPart.Builder()
    .type(httpclient.MultiPart.FORMDATA)
    .addPart(requestBody1)
    .addPart(requestBody2)
    .addPart(requestBody3)
    .build();
let body = multiPartObj.createRequestBody();
```



Use multipart body in requests/response

```javascript
var request =  new httpclient.Request.Builder()
    .url(this.echoServer)
    .post(body)
    .addHeader("Content-Type", "multipart/form-data")
    .params("LibName", "Okhttp-ohos")
    .params("Request", "MultiData")
    .build()
```

### Examples of Socket usage

Create socket
```javascript
 client: any = new httpclient.HttpClient
.Builder()
    .setConnectTimeout(10, httpclient.TimeUnit.SECONDS)
    .setReadTimeout(10, httpclient.TimeUnit.SECONDS)
    .build();
 var request = new httpclient.Request.Builder()
    .url(this.defaultIpAddress)
    .build();
 this.webSocket= this.client.newWebSocket(request);

```
Use socket connection and  callback methods for data transfer
```javascript
this.webSocket.send(this.sendMessage, (err, value) => {
    if (!err) {
        console.info('-----send success--' + value)
    } else {
        console.error("------------send fail, err:" + JSON.stringify(err))
    }
});
this.webSocket.on('open', (err, value) => {
    console.info("on open, status:" + value.status + ", message:" + value.message);
});
this.webSocket.on('message', (err, value) => {
    console.info("on open,  message---value:" + value);
    this.message ="received from server:" +value;
});
this.webSocket.close((err, value) => {
    if (!err) {
        console.log("close success");
    } else {
        console.log("close fail, err is " + JSON.stringify(err));
    }
});
```
## Interface Description

### RequestBody

| interface name | parameter                                      | return value | illustrate                  |
| -------------- | ---------------------------------------------- | ------------ | --------------------------- |
| create         | content : String/JSON Object of Key:Value pair | RequestBody  | Create a RequestBody object |

### RequestBuilder

| interface name  | parameter                 | return value   | illustrate                                        |
| --------------- | ------------------------- | -------------- | ------------------------------------------------- |
| buildAndExecute | none                      | void           | Build and execute RequestBuilder                  |
| newCall         | none                      | void           | execute request                                   |
| header          | name:String, value:String | RequestBuilder | Pass in key and value to construct request header |
| connectTimeout  | timeout:Long              | RequestBuilder | Set connection timeout                            |
| url             | value:String              | RequestBuilder | set request url                                   |
| GET             | none                      | RequestBuilder | Build a GET request method                        |
| PUT             | body:RequestBody          | RequestBuilder | Build the PUT request method                      |
| DELETE          | none                      | RequestBuilder | Build the DELETE request method                   |
| POST            | none                      | RequestBuilder | Build the POST request method                     |
| UPLOAD          | files:Array, data:Array   | RequestBuilder | Build the UPLOAD request method                   |
| CONNECT         | none                      | RequestBuilder | Build the CONNECT request method                  |

### MimeBuilder

| interface name | parameter    | return value | illustrate                   |
| -------------- | ------------ | ------------ | ---------------------------- |
| contentType    | value:String | void         | Add MimeBuilder contentType. |

### FormEncodingBuilder

| interface name | parameter                | return value | illustrate                        |
| -------------- | ------------------------ | ------------ | --------------------------------- |
| add            | name:String,value:String | void         | Add parameters as key-value pairs |
| build          | none                     | void         | Get the RequestBody object        |

### FileUploadBuilder

| interface name | parameter                | return value | illustrate                           |
| -------------- | ------------------------ | ------------ | ------------------------------------ |
| addFile        | furi : String            | void         | Add file URI to parameter for upload |
| addData        | name:String,value:String | void         | Add request data as key-value pairs  |
| buildFile      | none                     | void         | Generate a file object for upload    |
| buildData      | none                     | void         | Build data for upload                |

## compatibility

Support OpenHarmony API version 8 and above

## Directory Structure

```javascript
|----  httpclient   
|      |----  entry    
#Example code folder |      |----  httpclient     
# httpclient library folder |            |----  index . ets    
# httpclient external interface |      |----  README .MD #Installation   and usage                   
```

##  Contribute code

If you find any problems during use, you can submit an[Issue](https://gitee.com/openharmony-tpc/httpclient/issues) to us. Of course, we also welcome you to submit [PR](https://gitee.com/openharmony-tpc/httpclient/pulls) to us.

## Open source protocol

This project is based on [Apache License 2.0](https://gitee.com/openharmony-tpc/httpclient/blob/master/LICENSE)，please enjoy and participate in open source freely.