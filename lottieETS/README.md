#lottieETS

## 简介

lottieETS是一个适用于OpenHarmony的动画库，它可以使用Bodymovin解析以json格式导出的Adobe After Effects动画，并在移动设备上进行本地渲染。

![showlottie](./screenshot/showlottie.gif)


## 下载安裝

```
 npm install @ohos/lottieETS --save
```

OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包]() 。

## 使用说明

1. 在相应的类中引入lottieETS：

```
import lottie from '@ohos/lottieETS'
```

2.构建渲染上下文

```
  private settings: RenderingContextSettings = new RenderingContextSettings(true)
  private context: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.settings)
```

3.將动画需要的json文件放到MainAbility目录下，然后引用(json路径为entry/src/main/ets/MainAbility/common/lottieETS/data.json)

```
  private path:string = "common/lottie/data.json"
  或
  private jsonData:string = {"v":"4.6.6","fr":24,"ip":0,"op":72,"w":1000,"h":1000,"nm":"Comp 2","ddd":0,"assets":[],...}
```

4.关联画布

```
       Canvas(this.context)
        .width(540 + 'px')
        .height(360 + 'px')
        .backgroundColor(Color.Gray)
```

5.加载动画

```
    let animationItem = lottieETS.loadAnimation({
            container: this.context,            // 渲染上下文
            renderer: 'canvas',                 // 渲染方式
            loop: true,                         // 是否循环播放,默认true
            autoplay: true,                     // 是否自动播放，默认true
            path: this.path,                    // json路径
          })
     或      
    lottieETS.loadAnimation({
            container: this.context,            // 渲染上下文
            renderer: 'canvas',                 // 渲染方式
            loop: true,                         // 是否循环播放,默认true
            autoplay: true,                     // 是否自动播放，默认true
            animationData: this.jsonData,       // json对象数据
          })
```

6.控制动画

- 播放动画

  ```
  lottie.play()
  或
  animationItem.play()
  ```

- 停止动画

  ```
  lottie.stop()
  或
  animationItem.stop()
  ```

- 暂停动画

  ```
  lottie.pause()
  或
  animationItem.pause()
  ```

- 切换暂停/播放

  ```
  lottie.togglePause()
  或
  animationItem.togglePause()
  ```

- 设置播放速度

  ```
  注意：speed>0正向播放, speed<0反向播放, speed=0暂停播放, speed=1.0/-1.0正常速度播放
  
  lottie.setSpeed(1)
  或
  animationItem.setSpeed(1)
  ```

- 设置动画播放方向

  ```
  注意：direction 1为正向，-1为反向
  
  lottie.setDirection(1)
  或
  animationItem.setDirection(1)
  ```

- 销毁动画

  ```
  注意：页面不显示或退出页面时，需要销毁动画
  lottie.destroy()
  ```

- 控制动画停止在某一帧或某一时刻

  ```
  注意：根据第二个参数判断按帧还是按毫秒控制，true 按帧控制，false 按时间控制，缺省默认为false
  animationItem.goToAndStop(250,true)
  或
  animationItem.goToAndStop(5000,false)
  ```

- 控制动画从某一帧或某一时刻开始播放

  ```
  注意:根据第二参数判断按帧还是按毫秒控制，true 按帧控制，false 按时间控制，缺省默认为false
  animationItem.goToAndPlay(250,true)
  或
  animationItem.goToAndPlay(12000,false)
  ```

- 限定动画资源播放时的整体帧范围，即设置动画片段

  ```
  animationItem.setSegment(5,15);
  ```

- 播放动画片段

  ```
  注意：第二参数值为true立刻生效, 值为false循环下次播放的时候生效
  animationItem.playSegments([5,15],[20,30],true)
  ```

- 重置动画播放片段，是动画从第一帧开始播放完整动画

  ```
  注意：参数值为true立刻生效, 值为false循环下次播放的时候生效
  animationItem.resetSegments(5,15);
  ```

- 获取动画时长/帧数

  ```
  注意：参数值为true时获取帧数，值为false时获取时间(单位ms)
  animationItem.getDuration();
  ```

- 添加侦听事件

  ```
  AnimationEventName = 'enterFrame' | 'loopComplete' | 'complete' | 'segmentStart' | 'destroy' | 'config_ready' | 'data_ready' | 'DOMLoaded' | 'error' | 'data_failed' | 'loaded_images';
  
  animationItem.addEventListener("enterFrame",function(){
  	// TODO something
  })
  ```

- 移除侦听事件

  ```
  animationItem.removeEventListener("enterFrame",function(){
  	// TODO something
  })
  ```



## 接口说明


| 使用方法              | 类型                   | 相关描述           |
| --------------------- | ---------------------- | ------------------ |
| play()                | name?                  | 播放               |
| stop()                | name?                  | 停止               |
| pause()               | name?                  | 暂停               |
| togglePause()         | name?                  | 切换暂停           |
| destroy()             | name?                  | 销毁动画           |
| goToAndStop()         | value, isFrame?, name? | 跳到某一时刻并停止 |
| goToAndPlay()         | value, isFrame?, name? | 跳到某一时刻并播放 |
| setSegment()          | init,end               | 设置动画片段       |
| playSegments()        | arr, forceFlag         | 播放指定片段       |
| resetSegments()       | forceFlag              | 重置动画           |
| setSpeed()            | speed                  | 设置播放速度       |
| setDirection()        | direction              | 设置播放方向       |
| getDuration()         | isFrames?              | 获取动画时长       |
| addEventListener()    | eventName,callback     | 添加监听状态       |
| removeEventListener() | name,callback?         | 移除监听状态       |

## 兼容性

支持 OpenHarmony API version 8 及以上版本。

## 目录结构

````
|---- lottieETS  
|     |---- entry  # 示例代码文件夹
|     |---- lottieETS  # lottieETS库文件夹
|           |---- index.ets  # 对外接口
|           └─src/main/js
|                       ├─build # 核心代码，事件总线处理类等
|                           ├─play # 核心代码，事件总线处理类等
|                               ├─lottie.js #核心代碼，包含json解析，动画绘制，操作动画
|                       ├─docs # 包含各种json效果     
|                       ├─player # 播放器
|                           ├─index.html #集合多个js文件，构建生成lottie.js
|                       └─lottie.d.ts # 对外接口,让ets能够调用js
|                     
|---- README.md  # 安装使用方法                    
````

## 贡献代码

使用过程中发现任何问题都可以提 [Issue]() 给我们，当然，我们也非常欢迎你给我们发 [PR]() 。

## 开源协议

本项目基于 [MIT License]() ，请自由地享受和参与开源。

