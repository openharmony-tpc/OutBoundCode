/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import mqttAsyncSo from 'libmqttasync.so';
import {
  MqttClientOptions,
  MqttConnectOptions,
  MqttSubscribeOptions,
  MqttPublishOptions,
  MqttResponse,
  MqttMessage,
  MQTTASYNC_TRACE_LEVELS,
  AsyncCallback,
  Client
} from './MqttOption'

class MqttAsyncClient implements Client {

  connect(options: MqttConnectOptions, callback: AsyncCallback<MqttResponse>): void {
    return mqttAsyncSo.connect(options,callback)
  };

  destroy(): void {
    return mqttAsyncSo.destroy()
  };

  disconnect(callback: AsyncCallback<MqttResponse>): void {
    return mqttAsyncSo.disconnect(callback)
  };

  messageArrived(callback: AsyncCallback<MqttMessage>): void {
    return mqttAsyncSo.messageArrived(callback)
  };

  connectLost(callback: AsyncCallback<MqttResponse>): void {
    return mqttAsyncSo.connectLost(callback)
  };

  publish(options: MqttPublishOptions, callback: AsyncCallback<MqttResponse>): void {
    return mqttAsyncSo.publish(options,callback)
  };

  subscribe(options: MqttSubscribeOptions, callback: AsyncCallback<MqttResponse>): void {
    return mqttAsyncSo.subscribe(options,callback)
  };

  unsubscribe(options: MqttSubscribeOptions, callback: AsyncCallback<MqttResponse>): void {
    return mqttAsyncSo.unsubscribe(options,callback)
  };

  isConnected(): boolean {
    return mqttAsyncSo.isConnected()
  };

  reconnect(): boolean {
    return mqttAsyncSo.reconnect()
  };

  setMqttTrace(level: MQTTASYNC_TRACE_LEVELS): void {
    return mqttAsyncSo.setMqttTrace(level)
  };
}

class MqttAsync  {

  public static createMqtt(options: MqttClientOptions): MqttAsyncClient {
    console.log('AsyncMqtt createMqtt_napi')
    return mqttAsyncSo.createMqttSync(options)
  };

}

export default MqttAsync

