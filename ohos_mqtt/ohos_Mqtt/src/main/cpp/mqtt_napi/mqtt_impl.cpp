/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mqtt_impl.h"
#include <algorithm>
#include <cstring>
#include <memory>
#include "constant.h"
#include "event_list.h"
#include "mqtt_log.h"
#include "mqtt_napi_utils.h"

namespace OHOS {
namespace PahoMqtt {
std::mutex MqttImpl::mutex_;

bool MqttImpl::initialized_ = false;

MQTTAsync MqttImpl::mqttAsync_;

int MqttImpl::SslErrorCallback(const char *str, size_t len, void *u)
{
    LOG("AsyncMqtt MqttSslErr: str:%{public}s, len:%{public}d, u:%{public}s",str,len,u);
    return 0;
}

void MqttImpl::MqttTraceCallback(enum MQTTASYNC_TRACE_LEVELS level, char *message)
{
    LOG("AsyncMqtt MqttTrace: level:%{public}d, msg:%{public}s",level,message);
}

bool MqttImpl::Initialize(MqttClientOptions options)
{
    std::lock_guard<std::mutex> lock(mutex_);
    if (initialized_) {
        return true;
    }
    std::string clientId = options.GetClientId();
    std::string url = options.GetUrl();
    int persistenceType = options.GetPersistenceType();
    int rc = MQTTAsync_create(&mqttAsync_, url.c_str(), clientId.c_str(), persistenceType, NULL);
    LOG("AsyncMqtt MQTTAsync_create url:%{public}s, rc = %{public}d",url.c_str(),rc);
    if (rc != MQTTASYNC_SUCCESS) {
        return false;
    }
    initialized_ = true;
    return initialized_;
}

void MqttImpl::OnConnectSuccess(void *context, MQTTAsync_successData *response)
{
    LOG("AsyncMqtt Connect Success");
    auto connectContext = reinterpret_cast<ConnectContext *>(context);
    int32_t responseCode = 0;
    std::string result = "Connect Success";
    MqttResponse *mqttResponse = new MqttResponse();
    mqttResponse->SetCode(responseCode);
    mqttResponse->SetMessage(result);
    connectContext->CallTsFunction(MQTT_CONNECT_EVENT, mqttResponse);
    connectContext->ReleaseTsFunction();
}

void MqttImpl::OnConnectFail(void *context, MQTTAsync_failureData *response)
{
    LOG("AsyncMqtt Connect Fail code: %{public}d, message:%{public}s",response->code,response->message);
    auto connectContext = reinterpret_cast<ConnectContext *>(context);
    int32_t responseCode = response->code;
    std::string result = response->message == nullptr ? "failed" : response->message;
    MqttResponse *mqttResponse = new MqttResponse();
    mqttResponse->SetCode(responseCode);
    mqttResponse->SetMessage(result);
    connectContext->CallTsFunction(MQTT_CONNECT_EVENT, mqttResponse);
    connectContext->ReleaseTsFunction();
}

int MqttImpl::MessageArrived(void *context, char *topicName, int topicLen, MQTTAsync_message *message)
{
    LOG("AsyncMqtt MessageArrived topicName:%{public}s, topicLen:%{public}d",topicName,topicLen);
    if (message == nullptr) {
        return 0;
    }
    auto publishContext = reinterpret_cast<PublishContext *>(context);
    MqttMessage *mqttMessage = new MqttMessage();
    mqttMessage->SetTopic(std::string(topicName));
    mqttMessage->SetPayload(std::string((char *)message->payload));
    mqttMessage->SetPayloadlen(message->payloadlen);
    mqttMessage->SetQos(message->qos);
    mqttMessage->SetRetained(message->retained);
    mqttMessage->SetDup(message->dup);
    mqttMessage->SetMsgid(message->msgid);
    publishContext->CallTsFunction(MQTT_MESSAGE_ARRIVED_EVENT, mqttMessage);
    MQTTAsync_freeMessage(&message);
    MQTTAsync_free(topicName);
    return 1;
}

void MqttImpl::ConnectionLost(void *context, char *cause)
{
    LOG("AsyncMqtt connectionLost cause:%{public}s",cause);
    auto baseContext = reinterpret_cast<BaseContext *>(context);
    MqttResponse *mqttResponse = new MqttResponse();
    mqttResponse->SetMessage(cause == nullptr ? "null" : cause);
    mqttResponse->SetCode(MQTTASYNC_SUCCESS);
    baseContext->CallTsFunction(MQTT_CONNECT_LOST_EVENT, mqttResponse);
}

void MqttImpl::OnSubscribeSuccess(void *context, MQTTAsync_successData *response)
{
    LOG("AsyncMqtt OnSubscribe Success");
    auto subscribeContext = reinterpret_cast<SubscribeContext *>(context);
    int32_t responseCode = 0;
    std::string result = "Subscribe Success";
    MqttResponse *mqttResponse = new MqttResponse();
    mqttResponse->SetCode(responseCode);
    mqttResponse->SetMessage(result);
    subscribeContext->CallTsFunction(MQTT_SUBSCRIBE_EVENT, mqttResponse);
    subscribeContext->ReleaseTsFunction();
}

void MqttImpl::OnSubscribeFail(void *context, MQTTAsync_failureData *response)
{
    LOG("AsyncMqtt OnSubscribeFail code:%{public}d, message:%{public}s", response->code, response->message);
    auto subscribeContext = reinterpret_cast<SubscribeContext *>(context);
    int32_t responseCode = response->code;
    std::string result = response->message == nullptr ? "failed" : response->message;
    MqttResponse *mqttResponse = new MqttResponse();
    mqttResponse->SetCode(responseCode);
    mqttResponse->SetMessage(result);
    subscribeContext->CallTsFunction(MQTT_SUBSCRIBE_EVENT, mqttResponse);
    subscribeContext->ReleaseTsFunction();
}

void MqttImpl::OnUnSubscribe(void *context, MQTTAsync_successData *response)
{
    LOG("AsyncMqtt OnUnSubscribe Success");
    auto subscribeContext = reinterpret_cast<SubscribeContext *>(context);
    int32_t responseCode = 0;
    std::string result = "UnSubscribe Success";
    MqttResponse *mqttResponse = new MqttResponse();
    mqttResponse->SetCode(responseCode);
    mqttResponse->SetMessage(result);
    subscribeContext->CallTsFunction(MQTT_UNSUBSCRIBE_EVENT, mqttResponse);
    subscribeContext->ReleaseTsFunction();
}

void MqttImpl::OnUnSubscribeFail(void *context, MQTTAsync_failureData *response)
{
    LOG("AsyncMqtt OnUnSubscribeFail code:%{public}d, message:%{public}s", response->code, response->message);
    auto subscribeContext = reinterpret_cast<SubscribeContext *>(context);
    int32_t responseCode = response->code;
    std::string result = response->message == nullptr ? "failed" : response->message;
    MqttResponse *mqttResponse = new MqttResponse();
    mqttResponse->SetCode(responseCode);
    mqttResponse->SetMessage(result);
    subscribeContext->CallTsFunction(MQTT_UNSUBSCRIBE_EVENT, mqttResponse);
    subscribeContext->ReleaseTsFunction();
}

void MqttImpl::OnDisconnect(void *context, MQTTAsync_successData *response)
{
    LOG("AsyncMqtt OnDisconnect Success");
    auto baseContext = reinterpret_cast<BaseContext *>(context);
    int32_t responseCode = 0;
    std::string result = "Disconnect Success";
    MqttResponse *mqttResponse = new MqttResponse();
    mqttResponse->SetCode(responseCode);
    mqttResponse->SetMessage(result);
    baseContext->CallTsFunction(MQTT_DISCONNECT_EVENT, mqttResponse);
    baseContext->ReleaseTsFunction();
}

void MqttImpl::OnPublishSuccess(void *context, MQTTAsync_successData *response)
{
    LOG("AsyncMqtt OnPublish Success");
    auto publishContext = reinterpret_cast<PublishContext *>(context);
    int32_t responseCode = 0;
    std::string result = "Publish Success";
    MqttResponse *mqttResponse = new MqttResponse();
    mqttResponse->SetCode(responseCode);
    mqttResponse->SetMessage(result);
    publishContext->CallTsFunction(MQTT_PUBLISH_EVENT, mqttResponse);
    publishContext->ReleaseTsFunction();
}

void MqttImpl::OnPublishFail(void *context, MQTTAsync_failureData *response)
{
    LOG("AsyncMqtt OnPublishFail code:%{public}d, message:%{public}s", response->code, response->message);
    auto publishContext = reinterpret_cast<PublishContext *>(context);
    int32_t responseCode = response->code;
    std::string result = response->message == nullptr ? "failed" : response->message;
    MqttResponse *mqttResponse = new MqttResponse();
    mqttResponse->SetCode(responseCode);
    mqttResponse->SetMessage(result);
    publishContext->CallTsFunction(MQTT_PUBLISH_EVENT, mqttResponse);
    publishContext->ReleaseTsFunction();
}

bool MqttImpl::MqttConnect(ConnectContext *context)
{
    if (context == nullptr) {
        return false;
    }
    LOG("AsyncMqtt MqttConnect start");
    int rc;
    MQTTAsync_connectOptions opts = context->options.GetConnectOptions();
    opts.onSuccess = OnConnectSuccess;
    opts.onFailure = OnConnectFail;
    opts.context = context;
    if (context->options.GetHasSslOptions()) {
        LOG("AsyncMqtt ssl connect");
        MQTTAsync_SSLOptions sslOptions = context->options.GetSslOptions();
        opts.ssl = &sslOptions;
        opts.ssl->ssl_error_cb = SslErrorCallback;
    }
    if (context->options.GetHasWillOptions()) {
        MQTTAsync_willOptions willOptions = context->options.GetWillOptions();
        opts.will = &willOptions;
    }
    LOG("AsyncMqtt Connecting");
    rc = MQTTAsync_connect(mqttAsync_, &opts);
    freeConnectcontext(context);
    LOG("AsyncMqtt MQTTAsync_connect rc = %{public}d",rc);
    if (rc != MQTTASYNC_SUCCESS) {
        LOG("AsyncMqtt MQTTAsync_connect failed:");
        return false;
    }
    return true;
}

bool MqttImpl::MqttDisconnect(BaseContext *context)
{
    if (context == nullptr) {
        return false;
    }

    MQTTAsync_disconnectOptions opts = MQTTAsync_disconnectOptions_initializer;
    int rc;
    opts.onSuccess = MqttImpl::OnDisconnect;
    opts.context = context;
    LOG("AsyncMqtt MqttDisconnect");
    rc = MQTTAsync_disconnect(mqttAsync_, &opts);
    if (rc != MQTTASYNC_SUCCESS) {
        LOG("AsyncMqtt MqttDisconnect res: %{public}d",rc);
        return false;
    }
    return true;
}

bool MqttImpl::MqttReConnect()
{
    LOG("AsyncMqtt MqttReConnect");
    int rc = MQTTAsync_reconnect(mqttAsync_);
    if (rc != MQTTASYNC_SUCCESS) {
        LOG("AsyncMqtt MqttReConnect res: %{public}d",rc);
        return false;
    }
    return true;
}

bool MqttImpl::IsConnected()
{
    if (mqttAsync_ == NULL) {
        return false;
    }
    return MQTTAsync_isConnected(mqttAsync_);
}

bool MqttImpl::MqttSubscribe(SubscribeContext *context)
{
    if (context == nullptr) {
        return false;
    }
    LOG("AsyncMqtt Starting - MqttSubscribe");
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    opts.onSuccess = OnSubscribeSuccess;
    opts.onFailure = OnSubscribeFail;
    opts.context = context;
    std::string topic = context->options.GetTopic();
    uint32_t qos = context->options.GetQos();
    int rc = MQTTAsync_subscribe(mqttAsync_, topic.c_str(), qos, &opts);
    LOG("AsyncMqtt MQTTAsync_subscribe rc = %{public}d",rc);
    if (rc != MQTTASYNC_SUCCESS) {
        LOG("AsyncMqtt MqttSubscribe failed:");
        return false;
    }
    return true;
}

bool MqttImpl::MqttUnsubscribe(SubscribeContext *context)
{
    if (context == nullptr) {
        return false;
    }
    LOG("AsyncMqtt Starting MqttUnsubscribe");
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    std::string topic = context->options.GetTopic();
    opts.onSuccess = OnUnSubscribe;
    opts.onFailure = OnUnSubscribeFail;
    opts.context = context;
    int rc;
    rc = MQTTAsync_unsubscribe(mqttAsync_, topic.c_str(), &opts);
    LOG("AsyncMqtt MQTTAsync_unsubscribe rc = %{public}d",rc);
    if (rc != MQTTASYNC_SUCCESS) {
        LOG("AsyncMqtt MqttUnsubscribe failed: ");
        return false;
    }
    return true;
}

bool MqttImpl::MqttPublish(PublishContext *context)
{
    if (context == nullptr) {
        return false;
    }

    MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;

    LOG("AsyncMqtt Starting - MqttPublish");
    std::string payload = context->mqttMessage.GetPayload();
    std::string topic = context->mqttMessage.GetTopic();
    pubmsg.payload = (char *)payload.c_str();
    pubmsg.payloadlen = context->mqttMessage.GetPayloadlen();
    pubmsg.qos = context->mqttMessage.GetQos();
    pubmsg.retained = context->mqttMessage.GetRetained();
    opts.onSuccess = OnPublishSuccess;
    opts.onFailure = OnPublishFail;
    opts.context = context;
    int rc1;
    int rc2;
    rc1 = MQTTAsync_sendMessage(mqttAsync_, topic.c_str(), &pubmsg, &opts);
//    if (rc != MQTTASYNC_SUCCESS) {
//        LOG("AsyncMqtt MqttPublish failed: rc = %{public}d",rc);
//        return false;
//    }
    rc2 = MQTTAsync_waitForCompletion(mqttAsync_, opts.token, WAIT_COMPLETION_TIME);
//    if (rc != MQTTASYNC_SUCCESS) {
//        LOG("AsyncMqtt MqttPublish failed: rc = %{public}d",rc);
//        return false;
//    }
    if (rc1 != MQTTASYNC_SUCCESS && rc2 != MQTTASYNC_SUCCESS) {
        LOG("AsyncMqtt MqttPublish failed: ");
        return false;
    }
    return true;
}

void MqttImpl::SetMqttTrace(enum MQTTASYNC_TRACE_LEVELS level)
{
    MQTTAsync_setTraceCallback(MqttTraceCallback);
    MQTTAsync_setTraceLevel(level);
}

int MqttImpl::SetMessageArrivedCallback(PublishContext *context)
{
    if (context == nullptr) {
        return MQTTASYNC_FAILURE;
    }
    return MQTTAsync_setMessageArrivedCallback(mqttAsync_, context, MqttImpl::MessageArrived);
}

int MqttImpl::SetConnectionLostCallback(BaseContext *context)
{
    if (context == nullptr) {
        return MQTTASYNC_FAILURE;
    }
    return MQTTAsync_setConnectionLostCallback(mqttAsync_, context, MqttImpl::ConnectionLost);
}

void MqttImpl::freeConnectcontext(ConnectContext *context)
{
    if (context == nullptr) {
        return;
    }
    if (context->options.GetConnectOptions().username) {
        delete[] context->options.GetConnectOptions().username;
    }
    if (context->options.GetConnectOptions().password) {
        delete[] context->options.GetConnectOptions().password;
    }
    if (context->options.GetConnectOptions().serverURIs) {
        for (int i = 0; i < context->options.GetConnectOptions().serverURIcount; i++) {
            delete[] context->options.GetConnectOptions().serverURIs[i];
        }
        delete[] context->options.GetConnectOptions().serverURIs;
    }
    if (context->options.GetWillOptions().topicName) {
        delete[] context->options.GetWillOptions().topicName;
    }
    if (context->options.GetWillOptions().message) {
        delete[] context->options.GetWillOptions().message;
    }
}

void MqttImpl::Destroy()
{
    LOG("AsyncMqtt MQTTAsync_destroy");
    std::lock_guard<std::mutex> lock(mutex_);
    if (initialized_) {
        initialized_ = false;
        MQTTAsync_destroy(&mqttAsync_);
    } else {
        LOG("AsyncMqtt not initialized");
    }
}
}// namespace PahoMqtt
} // namespace OHOS
