# OpenHarmonyMqtt

## 介绍
使应用程序能够连接到MQTT代理以发布消息、订阅主题和接收发布的消息。

## 使用
### 1、导入项目
    import { MqttAsync } from '@ohos/openHarmonyMqtt';
### 2、各项参数：
#### 2.1、创建客户端参数
    MqttAsyncClientOptions {
       /**
        * A null-terminated string specifying the server to which the client will connect.
        * It takes the form protocol://host:port.protocol must be tcp, ssl, ws or wss.
        * For host, you can specify either an IP address or a host name.
        * For instance,tcp://localhost:1883
        */
       url: string;
       /**
        * The client identifier passed to the server when the client connects to it.
        * It is a null-terminated UTF-8 encoded string.
        */
       clientId: string;
       /** The type of persistence to be used by the client. */
       persistenceType?: PersistenceType;
    }
#### 2.2、建立连接参数
    MqttConnectOptions {
      /**
       * This is a boolean value. The cleansession setting controls the behaviour
       * of both the client and the server at connection and disconnection time.
       * The client and server both maintain session state information. This
       * information is used to ensure "at least once" and "exactly once"
       * delivery, and "exactly once" receipt of messages. Session state also
       * includes subscriptions created by an MQTT client. You can choose to
       * maintain or discard state information between sessions.
       */
      cleanSession?: boolean; // default is true.
      connectTimeout?: number; // default is 30s.
      keepAliveInterval?: number; // default is 60s.
      /**
       * An array of null-terminated strings specifying the servers to which the client will connect.
       * Each string takes the form protocol://host:port.protocol must be tcp, ssl, ws or wss.
       * For host, you can specify either an IP address or a host name.
       */
      serverURIs?: Array<string>;
      retryInterval?: number; // default is 0.
      sslOptions?: {
        // true: enable server certificate authentication, false: disable,default is false.
        enableServerCertAuth?: boolean;
        // false for no verifying the hostname, true for verifying the hostname, default is false.
        verify?: boolean;
        /**
         * From the OpenSSL documentation:
         * If CApath is not NULL, it points to a directory containing CA certificates in PEM format.
         */
        caPath?: string;
        /**
         * The file in PEM format containing the public digital certificates trusted by the client.
         * It must set local file path in your device and the file must can be accessed.
         */
        trustStore?: string;
        /** The file in PEM format containing the public certificate chain of the client.
         * It may also include the client's private key.
         * It must set local file path in your device and the file must can be accessed.
         */
        keyStore?: string;
        /** If not included in the sslKeyStore, this setting points to
         * the file in PEM format containing the client's private key.
         * It must set local file path in your device and the file must can be accessed.
         */
        privateKey?: string;
        /** The password to load the client's privateKey if encrypted. */
        privateKeyPassword?: string;
        /**
         * The list of cipher suites that the client will present to the server during the SSL handshake.
         * If this setting is ommitted, its default value will be "ALL", that is, all the cipher suites -excluding
         * those offering no encryption- will be considered.
         * for example, TLSv1.2,RSA;More details please see https://www.openssl.org/docs/man1.1.1/man1/ciphers.html.
         */
        enabledCipherSuites?: string;
      };
      willOptions?: {
        topicName: string;
        message: string;
        retained?: boolean;
        qos?: QoS;
      };
      /**
       * 0 = default: start with 3.1.1, and if that fails, fall back to 3.1
       * 3 = only try version 3.1
       * 4 = only try version 3.1.1
       */
      MQTTVersion?: number;
      automaticReconnect?: boolean; // default is false.
      minRetryInterval?: number; // default is 1s.
      maxRetryInterval?: number; // default is 60s.
      /**
       * the username required by your broker
       */
     userName: string;
     /**
      * the password required by your broker
      */
     password: string;
    }
#### 2.3、订阅参数
    MqttSubscribeOptions {
      /** The topic associated with this message. */
      topic: string;
      /** The quality of service (QoS) assigned to the message. */
      qos: QoS;
    }
#### 2.4、发布参数
    MqttPublishOptions {
      /** The topic associated with this message. */
      topic: string;
      /** the payload of the MQTT message. */
      payload: string;
      /** The length of the MQTT message payload. */
      payloadLen: number;
      /** The quality of service (QoS) assigned to the message. */
      qos: QoS;
      /**
       * retained = true
       * For messages being published, a true setting indicates that the MQTT
       * server should retain a copy of the message. The message will then be
       * transmitted to new subscribers to a topic that matches the message topic.
       * 
       * retained = false
       * For publishers, this indicates that this message should not be retained
       * by the MQTT server. For subscribers, a false setting indicates this is
       * a normal message, received as a result of it being published to the
       * server.
       */
      retained?: boolean;
      /**
       * The dup flag indicates whether or not this message is a duplicate.
       * It is only meaningful when receiving QoS1 messages. When true, the
       * client application should take appropriate action to deal with the
       * duplicate message.  This is an output parameter only.
       */
      dup?: boolean;
      /** The message identifier is reserved for internal use by the
       * MQTT client and server.  It is an output parameter only - writing
       * to it will serve no purpose.  It contains the MQTT message id of
       * an incoming publish message.
       */
      msgid?: number;
    }
#### 2.5、响应参数
    MqttResponse {
      message: string;
      /** If the value 0 is returned, the operation is successful. */
      code: number;
    }
#### 2.6、接收消息响应参数
    MqttMessage {
      topic: string;
      payload: string;
      payloadLen: number;
      qos: QoS;
      retained: number;
      dup: number;
      msgid: number;
    }
### 3、创建客户端:
    this.mqttAsyncClient = MqttAsync.createMqtt({
        url: "120.24.193.35:1883",
        clientId: "e5fatos4jh3l79lndb0bs",
        persistenceType: 1,
    })

### 4、建立连接:
    let options = {
        userName: "domo",
        password: "123456",
        connectTimeout: 30,
        version: 0,
    };
    this.mqttAsyncClient.connect(options, (err, data) => {
        if (!err) {
            LogUtil.info(TAG, "connect result:" + JSON.stringify(data));
            this.showLog(data.message);
        } else {
            LogUtil.info(TAG, "connect error:" + JSON.stringify(err));
            this.showLog("connect error");
            this.showLog(JSON.stringify(err));
        }
    });
### 5、订阅主题:
    let subscribeOption = {
        topic: "domotopic",
        qos: 2
    }
    this.mqttAsyncClient.subscribe(subscribeOption, (err, data) => {
        if (!err) {
            LogUtil.info(TAG, "subscribe result:" + JSON.stringify(data));
            this.showLog(data.message);
        } else {
            LogUtil.info(TAG, "subscribe error:" + JSON.stringify(err));
            this.showLog("subscribe error");
            this.showLog(JSON.stringify(err));
        }
    });
### 6、取消订阅:
    let subscribeOption = {
        topic: "domotopic",
        qos: 2
    }
    this.mqttAsyncClient.unsubscribe(subscribeOption, (err, data) => {
        if (!err) {
            LogUtil.info(TAG, "unsubscribe result:" + JSON.stringify(data));
            this.showLog(data.message);
        } else {
            LogUtil.info(TAG, "unsubscribe error:" + JSON.stringify(err));
            this.showLog("unsubscribe error");
            this.showLog(JSON.stringify(err));
        }
    });
### 7、发布消息:
    let publishOption = {
        topic: "domotopic",
        qos: 1,
        payload: "haishangdebing",
        payloadLen: 14
    }
    this.mqttAsyncClient.publish(publishOption, (err, data) => {
        if (!err) {
            LogUtil.info(TAG, "publish result:" + JSON.stringify(data));
            this.showLog(data.message);
        } else {
            LogUtil.info(TAG, "publish error:" + JSON.stringify(err));
            this.showLog("publish error");
            this.showLog(JSON.stringify(err));
        }
    });
### 8、接收消息:
    this.mqttAsyncClient.messageArrived((err, data) => {
        if (!err) {
            LogUtil.info(TAG, "arrived message:" + JSON.stringify(data));
            let msg = "topic:" + data.topic + ", msg:" + data.payload;
            this.showLog(msg);
        } else {
            LogUtil.info(TAG, "messageArrived error:" + JSON.stringify(err));
            this.showLog("messageArrived error");
            this.showLog(JSON.stringify(err));
        }
    });
### 9、断开连接:
    this.mqttAsyncClient.disconnect((err, data) => {
        if (!err) {
            LogUtil.info(TAG, "disconnect result:" + JSON.stringify(data));
            this.showLog(data.message);
            this.connectLost()
        } else {
            LogUtil.info(TAG, "disconnect error:" + JSON.stringify(err));
            this.showLog("disconnect error");
            this.showLog(JSON.stringify(err));
        }
    });
### 10、重新连接（在已连接过的情况下使用）:
    let isReConnected:boolean = this.mqttAsyncClient.reconnect();
### 11、连接断开的回调（因网络等原因客户端与服务器的连接被断开时调用）:
    this.mqttAsyncClient.connectLost((err, data) => {
        if (!err) {
            LogUtil.info(TAG, "connect lost cause:" + JSON.stringify(data));
            this.showLog(data.message);
            //to do Something
        } else {
            LogUtil.info(TAG, "connect lost error:" + JSON.stringify(err));
            this.showLog("connect lost error");
            this.showLog(JSON.stringify(err));
        }
    });
### 12、判断是否连接:
    let isConnected:boolean = this.mqttAsyncClient.isConnected();
### 13、销毁客户端释放内存:
    this.mqttAsyncClient.destroy();
## 兼容性
支持 OpenHarmony API version 9 及以上版本。

## 目录结构
````
|----OpenHarmonyMqtt  
|     |---- entry  # 示例代码文件夹
|     |---- OpenHarmonyMqtt  # OpenHarmonyMqtt库文件夹
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-tpc/MMKV/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-tpc/MMKV/pulls) 。

## 开源协议
本项目基于 [BSD 3-Clause License](https://www.tizen.org/bsd-3-clause-license) ，请自由地享受和参与开源。 
    



