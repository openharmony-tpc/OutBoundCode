
if(NOT JSBIND_ROOT_PATH)
    message(FATAL_ERROR "-- [JSBIND]: JSBIND_ROOT_PATH must be setted up at first.")
endif()

set(JSBIND_SEARCH_PATH ${JSBIND_ROOT_PATH})
set(CMAKE_CXX_STANDARD 17)
# find base path
find_path(JSBIND_PATH
    NAMES include/jsbind/jsbind.h
    PATHS ${JSBIND_SEARCH_PATH}
    CMAKE_FIND_ROOT_PATH_BOTH
    )

# find includes
if(NOT JSBIND_INCLUDE_DIRS)
    find_path(JSBIND_INCLUDE_DIR
        NAMES jsbind/jsbind.h
        PATHS ${JSBIND_PATH}
        PATH_SUFFIXES include
        CMAKE_FIND_ROOT_PATH_BOTH
    )

    set(JSBIND_INCLUDE_DIRS
        ${JSBIND_INCLUDE_DIR})
endif()

# find link directories
if(NOT JSBIND_LINK_DIRS)
    # BIDL 库文件链接路径
    find_path(JSBIND_BIDL_LINK_DIRS
        NAMES libjsbind.so
        PATHS "${JSBIND_PATH}"
        PATH_SUFFIXES "libs/${CMAKE_OHOS_ARCH_ABI}"
        CMAKE_FIND_ROOT_PATH_BOTH
    )

    set(JSBIND_LINK_DIRS
        ${JSBIND_BIDL_LINK_DIRS})

endif()

# find librarys
if(NOT JSBIND_LIBRARIS)
    # BIDL 库文件
    find_library(JSBIND_LIBRARY
        NAMES jsbind
        PATHS "${JSBIND_PATH}"
        PATH_SUFFIXES "libs/${CMAKE_OHOS_ARCH_ABI}"
        CMAKE_FIND_ROOT_PATH_BOTH
    )

    set(JSBIND_LIBRARIS
        jsbind
        ace_napi.z
        hilog_ndk.z
        uv)

endif()

# 编译宏
if(NOT JSBIND_DEFINITIONS)
    if(OHOS)
        set(JSBIND_DEFINITIONS ${JSBIND_DEFINITIONS} "JSBIND_ENABLE_NAPI=1")
    endif()
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(JSBIND DEFAULT_MSG
    JSBIND_INCLUDE_DIRS
    JSBIND_LIBRARIS
)

if(JSBIND_FOUND AND NOT TARGET JSBind::libjsbind)
    # 创建接口lib
    add_library(JSBind::libjsbind INTERFACE IMPORTED)

    # 设置target 头文件依赖
    target_include_directories(JSBind::libjsbind INTERFACE ${JSBIND_INCLUDE_DIRS})

    # 设置target link依赖路径
    target_link_directories(JSBind::libjsbind INTERFACE ${JSBIND_LINK_DIRS})

    # 设置target lib依赖
    target_link_libraries(JSBind::libjsbind INTERFACE ${JSBIND_LIBRARIS})

    # 设置target 编译宏
    target_compile_definitions(JSBind::libjsbind INTERFACE ${JSBIND_DEFINITIONS})

    # 安全编译选项 NO Rpath 规则
    set(CMAKE_SKIP_RPATH TRUE)
endif()