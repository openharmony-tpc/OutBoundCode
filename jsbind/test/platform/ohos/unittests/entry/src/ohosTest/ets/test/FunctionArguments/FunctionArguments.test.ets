/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import testNapi from "libunittest.so"

export default function functionArgumentsTest() {
  describe('FunctionArgumentsTest', function () {
    it('001_PassingBoolReturnBool_True', 0, function () {
      expect(testNapi.PassingBoolReturnBool(true)).assertTrue()
    })
    it('002_PassingBoolReturnBool_False', 0, function () {
      expect(testNapi.PassingBoolReturnBool(false)).assertFalse()
    })
    it('003_PassingInt32ReturnInt32_10086', 0, function () {
      expect(testNapi.PassingInt32ReturnInt32(10086)).assertEqual(10086)
    })
    it('004_PassingInt32ReturnInt32_10086', 0, function () {
      expect(testNapi.PassingInt32ReturnInt32(-10086)).assertEqual(-10086)
    })
//    it('005_PassingUint32ReturnUint32_1314', 0, function () {
//      expect(testNapi.passingUint32ReturnUint32(1314)).assertEqual(1314) TODO 需要添加支持 uint32_t 特性
//    })
    it('006_PassingInt64ReturnInt64_2022', 0, function () {
      expect(testNapi.PassingInt64ReturnInt64(922337203685477580)).assertEqual(922337203685477580)
      expect(testNapi.PassingInt64ReturnInt64(-922337203685477580)).assertEqual(-922337203685477580)
    })
    it('007_PassingStringReturnString_PAF_BIDL', 0, function () {
      expect(testNapi.PassingStringReturnString("PAF BIDL")).assertEqual("PAF BIDL")
    })
    it('008_PassingStringRefReturnString_PAF_BIDL', 0, function () {
      expect(testNapi.PassingStringRefReturnString("PAF BIDL")).assertEqual("PAF BIDL")
    })
    it('009_PassingConstStringRefReturnString_PAF_BIDL', 0, function () {
      expect(testNapi.PassingConstStringRefReturnString("PAF BIDL")).assertEqual("PAF BIDL")
    })
    it('010_PassingConstStringRef2ReturnString_PAF_BIDL', 0, function () {
      expect(testNapi.PassingConstStringRef2ReturnString("PAF", " BIDL")).assertEqual("PAF BIDL")
    })
    it('011_PassingConstStringRef3ReturnString_PAF_BIDL', 0, function () {
      expect(testNapi.PassingConstStringRef3ReturnString("PAF", " ", "BIDL")).assertEqual("PAF BIDL")
    })
    it('012_PassingConstStringRef4ReturnString_PAF_BIDL', 0, function () {
      expect(testNapi.PassingConstStringRef4ReturnString("PAF", " ", "BI", "DL")).assertEqual("PAF BIDL")
    })
    it('013_PassingConstStringRef5ReturnString_PAF_BIDL', 0, function () {
      expect(testNapi.PassingConstStringRef5ReturnString("P", "AF", " ", "BI", "DL")).assertEqual("PAF BIDL")
    })
    it('014_PassingConstStringRef6ReturnString_PAF_BIDL', 0, function () {
      expect(testNapi.PassingConstStringRef6ReturnString("P", "A", "F", " ", "BI", "DL")).assertEqual("PAF BIDL")
    })
    it('015_PassingConstStringRef7ReturnString_PAF_BIDL', 0, function () {
      expect(testNapi.PassingConstStringRef7ReturnString("P", "A", "F", " ", "B", "I", "DL")).assertEqual("PAF BIDL")
    })
    it('016_PassingConstStringRef8ReturnString_PAF_BIDL', 0, function () {
      expect(testNapi.PassingConstStringRef8ReturnString("P", "A", "F", " ", "B", "I", "D", "L")).assertEqual("PAF BIDL")
    })
    it('017_PassingConstCStrPtrReturnConstCStrPtr', 0, function () {
      expect(testNapi.PassingConstCStrPtrReturnConstCStrPtr("hello PAF")).assertEqual("hello PAF")
    })
    it('018_PassingCStrPtrReturnConstCStrPtr', 0, function () {
      expect(testNapi.PassingCStrPtrReturnConstCStrPtr("hello PAF")).assertEqual("hello PAF")
    })
    it('019_PassingCStrPtrReturnCStrPtr', 0, function () {
      expect(testNapi.PassingCStrPtrReturnCStrPtr("hello PAF")).assertEqual("hello PAF")
    })
    it('020_PassingNoneReturnBool', 0, function () {
      expect(testNapi.PassingNoneReturnBool()).assertEqual(true)
    })
    it('021_PassingDoubleReturnDouble_4.14', 0, function () {
      expect(testNapi.PassingDoubleReturnDouble(3.14)).assertLess(5)
    })
    it('022_PassingDoubleReturnDouble_5.14', 0, function () {
      expect(testNapi.PassingDoubleReturnDouble(4.14)).assertEqual(5.14)
    })
    it('023_PassingFloatReturnFloat', 0, function () {
    let num = testNapi.PassingFloatReturnFloat(4.14); // add 1.1
      expect(num).assertLarger(5.23999); // float存在精度丢失
      expect(num).assertLess(5.24);
    })
    it('024_PassingVectorIntReturnBool', 0, function () {
      let array = [0, 1, 2, 3];
      let result = testNapi.PassingVectorIntReturnBool(array);
      expect(result).assertEqual(true);
    })
    it('025_PassingVectorIntReturnVectorInt', 0, function () {
      let array = [0, 1, 2, 3];
      let result = testNapi.PassingVectorIntReturnVectorInt(array);
      expect(result.toString()).assertEqual([0, 1, 2, 3, 4].toString());
    })
    it('026_PassingVectorInt8ReturnVectorInt8', 0, function () {
      let array = new Uint8Array([0, 1, 2, 3]);
      let result = testNapi.PassingVectorUint8ReturnVectorUint8(array);
      expect(result instanceof Uint8Array).assertEqual(true);
      expect(result.toString()).assertEqual((new Uint8Array([0, 1, 2, 3, 4])).toString());
    })
    it('027_PassingVectorBoolReturnVectorBool', 0, function () {
      let array = [false, false, false];
      let result = testNapi.PassingVectorBoolReturnVectorBool(array);
      expect(result.toString()).assertEqual([false, false, false, true].toString());
    })
    it('028_PassingVectorFloatReturnVectorFloat', 0, function () {
      let array = [1.1, 2.2, 3.3];
      let result = testNapi.PassingVectorFloatReturnVectorFloat(array);
      expect(result[3]).assertLarger(4.4); // float存在精度丢失
      expect(result[3]).assertLess(4.4000001);
    })
    it('029_PassingArrayIntReturnBool', 0, function () {
      let array = [0, 1, 2, 3];
      let result = testNapi.passingArrayIntReturnBool(array);
      expect(result).assertEqual(true);
    })
    it('030_PassingArrayIntReturnArrayInt', 0, function () {
      let array = [0, 1, 2, 3];
      let result = testNapi.passingArrayIntReturnArrayInt(array);
      expect(result.toString()).assertEqual([0, 1, 2, 3, 4].toString());
    })
    it('031_passingArrayStrReturnArrayStr', 0, function () {
      let array = ["0", "1", "2", "3"];
      let result = testNapi.passingArrayStrReturnArrayStr(array);
      expect(result.toString()).assertEqual(["0", "1", "2", "3", "4"].toString());
    })
    it('032_passingArrayBufferReturnArrayBuffer', 0, function () {
      let arrayBuffer = new ArrayBuffer(8);
      let certificate = new Int8Array([-1, 0, 0, 0, 0, 0, 0, 1]);
      let certificate1 = new Int8Array(arrayBuffer);
      certificate1[0] = -1;
      let result = testNapi.passingArrayBufferReturnArrayBuffer(arrayBuffer);
      let certificate2 = new Int8Array(result);
      expect(certificate.toString()).assertEqual(certificate2.toString());
    })
    it('033_passingInt8ArrayReturnInt8Array', 0, function () {
      const certificate1 = new Int8Array([-4, -3, -2, -1, 0, 1, 2, 3, 4]);
      const certificate2 = new Int8Array([4, 3, 2, 1, 0, -1, -2, -3, -4]);
      let result = testNapi.passingInt8ArrayReturnInt8Array(certificate1);
      expect(certificate1.toString()).assertEqual(certificate2.toString());
      expect(result.toString()).assertEqual(certificate2.toString());
    })
    it('034_passingUint8ArrayReturnUint8Array', 0, function () {
      let certificate1 = new Uint8Array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
      let certificate2 = new Uint8Array([10, 11, 12, 13, 14, 15, 16, 17, 18, 19]);
      let result = testNapi.passingUint8ArrayReturnUint8Array(certificate1);
      expect(certificate1.toString()).assertEqual(certificate2.toString());
      expect(result.toString()).assertEqual(certificate2.toString());
    })
    it('035_passingInt16ArrayReturnInt16Array', 0, function () {
      const certificate1 = new Int16Array([-4, -3, -2, -1, 0, 1, 2, 3, 4]);
      const certificate2 = new Int16Array([4, 3, 2, 1, 0, -1, -2, -3, -4]);
      let result = testNapi.passingInt16ArrayReturnInt16Array(certificate1);
      console.log('gzx JSBind: ' + result)
      expect(certificate1.toString()).assertEqual(certificate2.toString());
      expect(result.toString()).assertEqual(certificate2.toString());
    })
    it('036_passingUint16ArrayReturnUint16Array', 0, function () {
      let certificate1 = new Uint16Array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
      let certificate2 = new Uint16Array([10, 11, 12, 13, 14, 15, 16, 17, 18, 19]);
      let result = testNapi.passingUint16ArrayReturnUint16Array(certificate1);
      expect(certificate1.toString()).assertEqual(certificate2.toString());
      expect(result.toString()).assertEqual(certificate2.toString());
    })
    it('037_passingInt32ArrayReturnInt32Array', 0, function () {
      const certificate1 = new Int32Array([-4, -3, -2, -1, 0, 1, 2, 3, 4]);
      const certificate2 = new Int32Array([4, 3, 2, 1, 0, -1, -2, -3, -4]);
      let result = testNapi.passingInt32ArrayReturnInt32Array(certificate1);
      expect(certificate1.toString()).assertEqual(certificate2.toString());
      expect(result.toString()).assertEqual(certificate2.toString());
    })
    it('038_passingUint32ArrayReturnUint32Array', 0, function () {
      let certificate1 = new Uint32Array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
      let certificate2 = new Uint32Array([10, 11, 12, 13, 14, 15, 16, 17, 18, 19]);
      let result = testNapi.passingUint32ArrayReturnUint32Array(certificate1);
      expect(certificate1.toString()).assertEqual(certificate2.toString());
      expect(result.toString()).assertEqual(certificate2.toString());
    })
    it('039_passingBigInt64ArrayReturnBigInt64Array', 0, function () {
      const certificate1 = new BigInt64Array([BigInt(-4), BigInt(-3), BigInt(-2), BigInt(-1), BigInt(0), BigInt(1), BigInt(2), BigInt(3), BigInt(4)]);
      const certificate2 = new BigInt64Array([BigInt(4), BigInt(3), BigInt(2), BigInt(1), BigInt(0), BigInt(-1), BigInt(-2), BigInt(-3), BigInt(-4)]);
      let result = testNapi.passingBigInt64ArrayReturnBigInt64Array(certificate1);
      expect(certificate1.toString()).assertEqual(certificate2.toString());
      expect(result.toString()).assertEqual(certificate2.toString());
    })
    it('040_passingBigUint64ArrayReturnBigUint64Array', 0, function () {
      const certificate1 = new BigUint64Array([BigInt(0), BigInt(1), BigInt(2), BigInt(3), BigInt(4), BigInt(5), BigInt(6), BigInt(7), BigInt(8), BigInt(9)]);
      const certificate2 = new BigUint64Array([BigInt(10), BigInt(11), BigInt(12), BigInt(13), BigInt(14), BigInt(15), BigInt(16), BigInt(17), BigInt(18), BigInt(19)]);
      let result = testNapi.passingBigUint64ArrayReturnBigUint64Array(certificate1);
      expect(certificate1.toString()).assertEqual(certificate2.toString());
      expect(result.toString()).assertEqual(certificate2.toString());
    })
    it('041_P_PassingIntReturnInt', 0, async function (done) {
      // 异步任务调用模式
      testNapi.P_PassingIntReturnInt(888).then(result => {
        expect(result).assertEqual(888);
        done();
      })
    })
  })
}