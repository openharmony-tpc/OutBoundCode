/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef JSBIND_BINDING_H
#define JSBIND_BINDING_H

#include <forward_list>
#include <unordered_map>
#include <memory>

#include "jsbind/invoker/invoker.h"
#include "jsbind/function.h"
#include "jsbind/class/class.h"
#include "jsbind/config.h"
#include "jsbind/value/napi/js_function.h"

#if JSBIND_SUPPORT_DECLARATION
#include "jsbind/reflect/type_meta.h"
#endif

namespace Jsb {

class JSBIND_EXPORT Binding {
public:
    static std::forward_list<Function>& GetFunctionList();

    static void RegisterFunction(const char *name, int32_t invokerId, Binder* binder);

    static std::forward_list<ClassBase*>& GetClassList();

    static void RegisterClass(ClassBase* xlass);

    static std::unordered_map<std::string, std::unique_ptr<JSFunction>>& GetJSFunctionMap();

    static int RegisterJSFunction(const std::string& name, std::unique_ptr<JSFunction> func);

#if JSBIND_SUPPORT_DECLARATION
    static std::forward_list<const TypeMeta*>& GetBuildInTypeList();
#endif

    static bool SupportAsyncWork() {
#if JSBIND_ASYNC_WORK
        return true;
#else
        return false;
#endif
    }

private:
    Binding() = delete;

    Binding(const Binding&) = delete;
};
} // namespace Jsb

#endif //JSBIND_BINDING_H
