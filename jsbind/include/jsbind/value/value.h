/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef JSBIND_VALUE_H
#define JSBIND_VALUE_H

#include <string>

namespace Jsb {
class Value {
public:
    virtual ~Value() = default;

    operator bool() { return GetBool(); }

    operator int() { return GetInt(); }

    operator int64_t() { return GetInt64(); }

    operator double() { return GetDouble(); }

    operator float() { return GetFloat(); }

    operator std::string() { return GetString(); }

    template<typename T>
    operator T&()
    {
        return *(reinterpret_cast<T*>(GetDataReference()));
    }

    template<typename T>
    operator T*()
    {
        return *(reinterpret_cast<T**>(GetDataReference()));
    }

    virtual operator Value*() = 0;

    virtual bool GetBool() const { return std::numeric_limits<bool>::quiet_NaN(); }

    virtual int32_t GetInt() const { return std::numeric_limits<int32_t>::quiet_NaN(); }

    virtual int64_t GetInt64() const { return std::numeric_limits<int64_t>::quiet_NaN(); }

    virtual double GetDouble() const { return std::numeric_limits<double>::quiet_NaN(); };

    virtual float GetFloat() const { return std::numeric_limits<float>::quiet_NaN(); };

    virtual std::string GetString() const { return std::string(); }

    virtual char* GetCString() { return nullptr; }

protected:
    virtual void* GetDataReference() = 0;
};
} // namespace Jsb
#endif //JSBIND_VALUE_H
