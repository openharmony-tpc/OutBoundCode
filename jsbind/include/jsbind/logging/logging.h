/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef JSBIND_LOGGING_H
#define JSBIND_LOGGING_H
#include <sstream>

#include "log_level.h"
#include "jsbind/config.h"

namespace Jsb {

class EatLogMessage {
public:
    void operator&(std::ostream&) {}
};

class JSBIND_EXPORT ScopedLogMessage {
public:
    ScopedLogMessage(LogLevel level,
               const char* file,
               int line,
               const char* condition);
    ~ScopedLogMessage();

    std::ostream& stream() { return stream_; }

private:
    std::ostringstream stream_;
    const LogLevel level_;

    ScopedLogMessage(const ScopedLogMessage&) = delete;
    ScopedLogMessage& operator=(const ScopedLogMessage&) = delete;
};

JSBIND_EXPORT bool ShouldCreateLogMessage(LogLevel level);

[[noreturn]] void KillProcess();

} // namespace Jsb

#define JSB_LOG_STREAM(level) \
  ::Jsb::ScopedLogMessage(level, __FILE__, __LINE__, nullptr).stream()

#define JSB_LAZY_STREAM(stream, condition) \
  !(condition) ? (void)0 : ::Jsb::EatLogMessage() & (stream)

#define JSB_EAT_STREAM_PARAMETERS(ignored) \
    true || (ignored)                        \
        ? (void)0                            \
        : ::Jsb::EatLogMessage() &       \
            ::Jsb::ScopedLogMessage(::Jsb::LOG_FATAL, 0, 0, nullptr).stream()

#define JSB_LOG_IS_ON(level) \
    (::Jsb::ShouldCreateLogMessage(level))

#define JSB_LOG(level) \
    JSB_LAZY_STREAM(JSB_LOG_STREAM(::Jsb::LOG_##level), JSB_LOG_IS_ON(::Jsb::LOG_##level))

#define JSB_CHECK(condition)                                              \
    JSB_LAZY_STREAM(                                                        \
        ::Jsb::ScopedLogMessage(::Jsb::LOG_FATAL, __FILE__, __LINE__, #condition) \
            .stream(),                                                      \
            !(condition))

#ifndef NDEBUG
#define JSB_DLOG JSB_LOG
#define JSB_DCHECK(condition) JSB_CHECK(condition)
#else
#define JSB_DLOG(level) JSB_EAT_STREAM_PARAMETERS(true)
#define JSB_DCHECK(condition) JSB_EAT_STREAM_PARAMETERS(condition)
#endif

#endif //JSBIND_LOGGING_H
