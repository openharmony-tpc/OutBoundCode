- [项目介绍](#项目介绍)
- [快速接入](#quick_start)
- [用户指南](#user_guide)
- [如何反馈](#如何反馈)
- [如何贡献](#如何贡献)

# 项目介绍

**`JSBind` 面向OpenHarmony应用开发者、三方SDK开发者提供面向对象语义的Node-API 的 Helper。用于简化绑定 Native C++ SDK 能力，开放至JavaScript侧，开发者可无视Node-API规范，包括令人讨厌的异步调用。**

## 约束
* \>= C++17
* 已支持平台：OpenHarmony

## 优势

1. 完全无视 Node-API；
2. 使用简便，学习成本低；
3. 不必考虑 JS 线程资源管理，灵活使用 JS 回调；
4. 有效减少 Node-API 静态接口代码量；
   ![compare.gif](doc/compare.gif)

# <a id="quick_start"> 快速接入 </a>

- [1 依赖配置](#1-依赖配置)
- [2 用户自定义业务](#2-用户自定义业务)
- [3 关联绑定](#3-关联绑定)
- [4 编译构建使用](#4-编译构建使用)

## **1 依赖配置**
* **源码依赖（推荐）**

  指定路径下（如：项目根路径/entry/src/main/cpp）
    ```
    cd entry/src/main/cpp
    git clone
    ```
  CMakeLists.txt添加依赖:

    ```cmake
    add_subdirectory(jsbind)
    target_link_libraries(entry PUBLIC jsbind)
    ```

  CMakeLists.txt添加依赖:

    ```cmake
    # ...
    # 设置 JSBIND 路径依赖 && # 索引 JSBIND 依赖
    set(JSBIND_ROOT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../../../node_modules/@ohos/jsbind)
    set(CMAKE_MODULE_PATH ${JSBIND_ROOT_PATH})
    find_package(JSBIND REQUIRED)
    
    # ... target 编译构建配置
    
    # ${TARGET_NAME} 为用户target
    target_link_libraries(${TARGET_NAME} PUBLIC JSBind::libjsbind)
    # ...
    ```

## **2 用户自定义业务**

用户业务 C++ 代码 hello.cpp:<br>
全程无感`node-API`

```C++
#include <string>

std::string SayHello(std::string msg)
{
  return msg + " too.";
}

```

## **3 关联绑定**

使用`JSBind`工具宏声明需要被绑定的类、函数：

```C++
// Step 1 注册 JSBind 插件

JSBIND_ADDON(xxx) // 注册 JSBind 插件名为: xxx

// Step 2 注册 FFI 特性

#include <jsbind/jsbind.h>

JSBIND_GLOBAL()
{
  JSBIND_FUNCTION(SayHello);
}
```

## **4 编译构建使用**

鸿蒙工程代码调用:

```javascript
import jsbind from 'libhello.so'

jsbind.SayHello("hello world");
```

# <a id="user_guide"> 用户指南 </a>
  - [hello world](#hello_world)
  - [用户自定义创建工程](#用户自定义创建工程)
  - [绑定特性](#bind_feature)
  - [绑定 C++](#bind_cpp)
    - [插件注册](#register_jsbind_addon) <sup>new in v1.1.0</sup>
    - [绑定全局函数](#bind_cpp_global)
    - [绑定类构造函数](#bind_cpp_contructor)
    - [绑定类成员函数](#bind_cpp_method)
    - [绑定类成员属性](#bind_cpp_field)
    - [回调](#bind_cpp_callback)
    - [等价类](#equivalence)
    - [JSBind && Node-API 混合开发](#hybrid)
    - [生成*.d.ts](#dts)
  - [绑定 JavaScript](#bind_javascript)
    - [绑定全局函数](#bind_js_function) <sup>new in v1.0.5</sup>
  - [Benchmark](#benchmark)

## <a id="hello_world"> hello world </a>

拉取示例代码：

```base
git clone
```

DevEco打开工程：examples/ohos/1_helloworld

进入 enter 目录并 install:

```bash
cd entry
npm install
```

通过DevEco build entry 安装到设备上 && 完成！

### 更多示例请查看：examples

## 用户自定义创建工程

1. 创建平台工程

   通过DevEco 创建包含C++的工程

    ```base
    File > New > Create Project | Module
    ```

2. 配置依赖并安装
* **源码依赖（推荐）**

  指定路径下（如：项目根路径/entry/src/main/cpp）
    ```
    cd entry/src/main/cpp
    git clone
    ```
  CMakeLists.txt添加依赖:

    ```cmake
    add_subdirectory(jsbind)
    target_link_libraries(entry PUBLIC jsbind)
    ```

* 或 npm依赖
   ```shell
   # 进入到需要依赖 jsbind 的 module 目录: entry
   cd entry
   
   # npm install 安装依赖
   npm i @ohos/jsbind
   ```

  CMakeLists.txt添加依赖:

    ```cmake
    # ...
    # 设置 JSBIND 路径依赖 && # 索引 JSBIND 依赖
    set(JSBIND_ROOT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../../../node_modules/@ohos/jsbind)
    set(CMAKE_MODULE_PATH ${JSBIND_ROOT_PATH})
    find_package(JSBIND REQUIRED)
    
    # ... target 编译构建配置
    
    # ${TARGET_NAME} 为用户target
    target_link_libraries(${TARGET_NAME} PUBLIC JSBind::libjsbind)
    # ...
    ```
  
1. 编译工程 && 运行

## <a id="bind_feature"> 绑定特性 </a>

### **语义绑定**

<table>
 <tr>
  <td> 语义 </td>
  <th colspan="2"> JSBind </th>
  <td> 说明 </td>
 </tr>
 <tr>
  <td> 插件注册 </td>
  <td> C++ </td>
  <td> JSBIND_ADDON </td>
  <td> 注册鸿蒙 Native 插件。 <br>
    <a href="#register_jsbind_addon">使用指导</a> </td>
 </tr>
 <tr>
  <td rowspan="2"> 全局函数 </td>
  <td> C++ </td>
  <td> JSBIND_FUNCTION </td>
  <td> 绑定 C++ 全局方法，JS 可调用。 <br>
    <a href="#bind_cpp_global">使用指导</a> </td>
 </tr>
 <tr>
  <td> JavaScript </td>
  <td> JSbind.bindFunction </td>
  <td> 绑定 JS 全局方法，C++ 可调用。 <br>
    <a href="#bind_js_function">使用指导</a> </td>
 </tr>
 <tr>
  <td rowspan="2"> 类构造函数 </td>
  <td> C++ </td>
  <td> JSBIND_CONSTRUCTOR<> </td>
  <td> 绑定 C++ 类构造函数，JS 可调用。构造函数可重载，需指定构造函数参数类型。<br>
    <a href="#bind_cpp_contructor">使用指导</a> </td>
 </tr>
 <tr>
  <td> JavaScript </td>
  <td> - </td>
  <td> 暂不支持 </td>
 </tr>
 <tr>
  <td rowspan="2"> 类成员函数 </td>
  <td> C++ </td>
  <td> JSBIND_METHOD </td>
  <td> 绑定 C++ 类成员函数，JS 可调用。<br>
    成员函数可以为：类静态函数，类成员函数，const类成员函数。<br>
    <a href="#bind_cpp_method">使用指导</a> </td>
 </tr>
 <tr>
  <td> JavaScript </td>
  <td> - </td>
  <td> 暂不支持 </td>
 </tr>
 <tr>
  <td rowspan="2"> 类成员属性 </td>
  <td> C++ </td>
  <td> JSBIND_FIELD </td>
  <td> 绑定 C++ 类成员属性（Get/Set），JS 可访问。<br>
    <a href="#bind_cpp_field">使用指导</a> </td>
 </tr>
 <tr>
  <td> JavaScript </td>
  <td> - </td>
  <td> 暂不支持 </td>
 </tr>
</table>

### 类型转换

|  **JavaScript**  | **C++** | 
| ---------------- | ------------------------ | 
| Boolean          | `bool`                                      |
| Number           | short, `int32`, `uint32`, `int64`, `double` |
| String           | `const char*`, `std::string`                |
| Array            | std::vector<T>, std::array<T, N>            |
| Function         | `std::function<R (P...)>` <br> `Jsb::Callbackn<R (P...)>` <br> `Jsb::SafetyCallbackn<R (P...)>` <br> See [回调](#bind_cpp_callback)             |
| Object           | `class`                                     |
| ArrayBuffer, <br> TypedArray      | `Jsb::ArrayBuffer`  [参考](#array_buffer)  |
| Promise          |  |

NOTE: 带有阴影部分的表示已支持

`const char*` 是以引用方式传递参数，如遇到异步操作，请使用传值方式：std::string

## <a id="data_structures"> 数据类型 </a>

### <a id="array_buffer"> 二进制数据缓冲区 ArrarBuffer & TypedArray </a>

`ArrayBuffer`, `TypedArray` 是 JavaScript 中非常常见的缓冲区类型结构，JSBind 提供了内建结构体：`Jsb::ArrayBuffer`用来支持该特性：

- `data()*` 获取 ArrayBuffer 数组缓冲区地址，Jsb::ArrayBuffer 本身不申请数据内存，data 都来源于JavaScript引擎分配的内存，也无需做内存生命周期管理，**禁止对该内存进行危险的释放**。

- `length()` 获取 ArrayBuffer 数组缓冲区长度，以单字节为计量单位。

- `typed()` 获取 ArrayBuffer 数组缓冲区的类型化类型。

- `count()` 获取 ArrayBuffer 数组缓冲区的类型化数据元素个数。

## <a id="bind_cpp"> 绑定 C++ </a>

使用`JSBind`的 C++ 库，绑定`native`的业务功能后，可由 JavaScript 直接调用。

### <a id="register_jsbind_addon"> 插件注册 </a>

#### JSBIND_ADDON(addonName)

使用`JSBIND_ADDON`注册鸿蒙 Native 插件，可从 JavaScript `import` 导入插件。

**参数：**

|  **参数名**  | **类型** | **必填** | **说明** |
| ----------- | -------- | ------- | ------------------------ | 
| addonName   | -        | Y       | 注册的鸿蒙 native 插件名，可从 JavaScript `import lib${addonName}.so` 导入插件 |

- C++

``` C++
#include <string>
#include <jsbind/jsbind.h>

JSBIND_ADDON(addon0)

```

- JavaScript

``` JavaScript
import addon from 'libaddon0.so' // 插件名为：addon0
```

### <a id="bind_cpp_global"> 绑定全局函数 </a>

#### JSBIND_FUNCTION(func, alias)

在`JSBIND_GLOBAL`作用域下使用`JSBIND_FUNCTION`绑定 C++ 全局函数后，可从 JavaScript 直接调用。

**参数：**

|  **参数名**  | **类型** | **必填** | **说明** |
| ----------- | -------- | ------- | ------------------------ | 
| func        | 函数指针  | Y       | 被绑定的`C++`函数指针，当alias未被指定时，`JavaScript`与`C++`函数名相同。 |
| alias       | string   | N       | 指定`JavaScript`调用的`C++`函数 |

- C++

``` C++
#include <string>
#include <jsbind/jsbind.h>

std::string SayHello(std::string msg)
{
    return msg + " too.";
}

JSBIND_GLOBAL()
{
    JSBIND_FUNCTION(SayHello);
}
```

- JavaScript

``` JavaScript
import jsbind from 'libhello.so' // 插件名

let message = jsbind.SayHello("hello world");
```

### <a id="bind_cpp_contructor"> 绑定类构造函数 </a>

- C++

``` C++
#include <string>
#include <jsbind/jsbind.h>

class TestObject {
public:
    TestObject();
    
    explicit TestObject(double) {
        // ...
    }
    
    ~TestObject() = default;
} // TestObject

JSBIND_CLASS(TestObject)
{
    JSBIND_CONSTRUCTOR<>();
    JSBIND_CONSTRUCTOR<double>();
}
```

- JavaScript

``` JavaScript
import jsbind from 'libhello.so' // 插件名

var obj1 = new testNapi.TestObject();
var obj2 = new testNapi.TestObject(3.14);
```

### <a id="bind_cpp_method"> 绑定类成员函数 </a>

`JSBind` 使用 `JSBIND_METHOD` 对C++ 的3种类成员函数进行绑定：类静态函数、类成员函数、const 类成员函数。
- `JSBIND_METHOD` 需要在`JSBIND_CLASS`的作用域下；

例：使用 `JSBind` 对C++类成员函数绑定
``` C++
#include <string>
#include <jsbind/jsbind.h>

class TestObject {
public:
    TestObject();
    
    explicit TestObject(double) {
        // ...
    }
    
    ~TestObject() = default;
    
    static double MultiplyObject(TestObject obj1, TestObject obj2) {
        return obj1.value_ * obj2.value_;
    }
    
    double Multiply(double mult) {
        value_ *= mult;
        return value_;
    }

private:
    double value_;
} // TestObject

JSBIND_CLASS(TestObject)
{
    JSBIND_CONSTRUCTOR<>();
    JSBIND_CONSTRUCTOR<double>();
    JSBIND_METHOD(MultiplyObject);
    JSBIND_METHOD(Multiply);
}
```

例：JavaScript 侧调用绑定的C++类成员函数

``` JavaScript
import jsbind from 'libhello.so' // 插件名

var obj1 = new testNapi.TestObject();
var obj2 = new testNapi.TestObject(3.14);
obj1.Multiply(-1);
jsbind.TestObject.MultiplyObject(obj1, obj2) // 静态方法
```


### <a id="bind_cpp_field"> 绑定类成员属性 </a>

- C++

``` C++
#include <string>
#include <jsbind/jsbind.h>

class TestObject {
public:    
    explicit TestObject(double) {
        // ...
    }
    
    ~TestObject() = default;
    
    double GetValue() const {
        return value_;
    }

    void SetValue(double value) {
        value_ = value;
    }

private:
    double value_;
} // TestObject

JSBIND_CLASS(TestObject)
{
    JSBIND_CONSTRUCTOR<double>();
    JSBIND_FIELD("value", GetValue, SetValue);
}
```

- JavaScript

``` JavaScript
import jsbind from 'libhello.so' // 插件名

var obj = new testNapi.TestObject(3.14);
obj.value = 1;
let value = obj.value;
```

### <a id="bind_cpp_callback"> 回调 </a>

Function是JS的一种基本数据类型，当JS传入Function作为参数时，Native可在适当的时机调用触发回调。JSBind支持如下3中C++数据类型作为参数处理回调：

- Jsb::Callback<R (P...)>：指定回调类型为`R (*)(P...)`的**高性能**回调。**非线程安全，禁止在非JS线程使用**，否则会发生异常；
- Jsb::SafetyCallback<R (P...)>：指定回调类型为`R (*)(P...)`的线程安全回调。因为需要创建线程安全资源，所以性能不如Jsb::Callback；
- std::function<R (P...)>：用法与Jsb::SafetyCallback一致；

### <a id="equivalence"> 等价类 </a>

等价类用于绑定 JavaScript && C++ 的类对象，在进行跨语言调用传值时，可保证两端类成员属性保持一致。

**约束**
1. C++ 侧**必须**给类对象显示加上成员函数 `void Equals(Jsb::Matcher matcher);` (函数暂不用实现);
   
2. JavaScript 侧**必须**给类对象加上成员函数 `equals(matcher: (...) => void)` ，并将matcher方法依次按照 C++ 构造函数形式传值；


- C++

``` C++
#include <jsbind/jsbind.h>

class EquivalenceTest {
public:

    EquivalenceTest(int value);

    static int ShowByValue(EquivalenceTest obj) {
        return obj.value_;
    }

    void Equals(Jsb::Matcher matcher) {
    // 暂不实现
    }

private:
    int value_;
};

JSBIND_CLASS(EquivalenceTest)
{
    JSBIND_CONSTRUCTOR<int>();
    JSBIND_METHOD(ShowByValue);
}
```

- JavaScript

``` JavaScript
import jsbind from 'libhello.so' // 插件名

class EquivalenceTest {
    constructor(value: Number) {
      this.value = value;
    }

    private equals(matcher: (v: Number) => void) {
      try {
        matcher(this.value);
      } catch(e) {
        console.log(e)
      }
    }

    value: Number;
}

var value = new EquivalenceTest(250);
var result = jsbind.EquivalenceTest.ShowByValue(value);
```

## <a id="bind_javascript"> 绑定 JavaScript </a>

使用`JSBind`的 JavaScript 插件，绑定 JavaScript 的业务功能后，可由`native`直接调用。

- **线程安全**：使用`JSBind`绑定的 JavaScript 函数是线程安全的，可在非JS线程直接调用。最终会由框架调度JS线程执行业务；
- **阻塞式调用**：C++ 触发调用 JavaScript 函数的调用是阻塞式的，对于在JS线程执行业务这点没有疑义。但当C++触发 JavaScript 业务调用的线程是非JS线程时，就存在跨线程任务调度。此时由框架进行了阻塞式调用，即 C++ 会等待 JavaScript 函数执行结束后返回；

### <a id="bind_js_function"> 绑定全局函数 </a>

#### JSBind.bindFunction(name: string, func: function)

在 JavaScript 使用 `JSBind.bindFunction` 绑定 JavaScript 全局函数后，可从 C++ 直接调用。

**参数：**

|  **参数名**  | **类型** | **必填** | **说明** |
| ----------- | -------- | ------- | ------------------------ | 
| name        | string   | Y       | 指定绑定的`JavaScript`函数名，用于Native索引。 |
| func        | function | Y       | 被绑定的`JavaScript`函数 |

**返回值：**

| **类型** | **说明** |
| ----------- | -------- |
| number      | 当前被绑定的函数下标索引   |

``` JavaScript
// name: 指定函数名，func: JavaScript 全局函数
libAddon.JSBind.bindFunction(name: string, func: Function);
```

C++ 使用Jsb::JSBind::GetJSFunction获取指定 JavaScript 函数句柄后，使用Invoke触发调用

``` C++
auto jsFunc = Jsb::JSBind::GetJSFunction("xxx"); // 获取指定函数句柄
auto result = jsFunc->Invoke<T>(...); // 调用JavaScript函数，Invoke<T>指定返回值类型
```

- JavaScript

``` JavaScript
import libAddon from 'libhello.so' // 插件名

function sayHelloFromJS (value) {
  console.log('what do you say: ' + value);
  return "hello from JS"
}

libAddon.JSBind.bindFunction("sayHelloFromJS", sayHelloFromJS);
```

- C++

``` C++
#include <string>
#include <jsbind/jsbind.h>

void DoSomething() {
    // 索引 JS 函数句柄
    auto jsFunc = Jsb::JSBind::GetJSFunction("sayHelloFromJS");

    // Invoke 指定 JS 方法的返回值类型
    auto result = jsFunc->Invoke<std::string>("hello from C++"); // 可在非JS线程执行
    // result == "hello from JS"
}
```

查看示例：bind_from_js

## <a id="hybrid"> JSBind && Node-API 混合开发 </a>

`JSBind` 支持与 Node-API 混合开发。接口 `Jsb::BindSymbols` 用于绑定使用 `JSBind` 的 Native 符号表给指定的 napi_value 对象。

如下示例：

examples/ohos/4_hybrid_napi/entry/src/main/hello.cpp
```C++
EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports)
{
    napi_property_descriptor desc[] = {
        ...
    };
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    
    exports = Jsb::BindSymbols(env, exports); // Jsb::BindSymbols 函数传入 js 对象绑定符号
    return exports;
}
EXTERN_C_END
```

## <a id="dts"> 生成*.d.ts </a>

暂未支持自动生成TypeScript声明文件。

## <a id="benchmark"> Benchmark </a>

* IDE： DevEco Studio 3.1.1.130
* SDK：3.2.10.6

API接口压测，当前采用了OHOS上单元测试框架的数据驱动能力，详见benchmark

<table>
 <tr>
  <td> API </td>
  <td> 调用次数 </td>
  <td> JSBind (ms) </td>
  <td> Node-API (ms) </td>
 </tr>
 <tr>
  <td> bool (*)() </td>
  <td> 10000 </td>
  <td> 0.0032 </td>
  <td> 0.0031 </td>
 </tr>
 <tr>
  <td> string (*)(string) </td>
  <td> 10000 </td>
  <td> 0.0058 </td>
  <td> 0.0043 </td>
 </tr>
 <tr>
  <td> void (*)( std::function )  </td>
  <td> 10000 </td>
  <td> 0.0667 </td>
  <td rowspan="3"> 0.0176 </td>
 </tr>
 <tr>
  <td> void (*)( Jsb::Callback )  </td>
  <td> 10000 </td>
  <td> 0.0178 </td>
 </tr>
 <tr>
  <td> void (*)( Jsb::SafetyCallback )  </td>
  <td> 10000 </td>
  <td> 0.0664 </td>
 </tr>
</table>

# 如何反馈

* issue
  
* jsbind用户交流群1：577978229


# 如何贡献
* 贡献代码

