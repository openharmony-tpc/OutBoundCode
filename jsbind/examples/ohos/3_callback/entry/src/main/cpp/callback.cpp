/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <functional>
#include <jsbind/jsbind.h>
#include <hilog/log.h>

void CallFunctionReturnVoid(std::function<void()> func) {
    func();
}

void CallFunctionReturnString(std::function<std::string()> func) {
    auto str = func();
    OH_LOG_Print(LOG_APP, LOG_INFO, 0x000, "callback-cpp", "std::function callback return from Js: %{public}s", str.c_str());
}

void CallJsbCallbackReturnVoid(Jsb::Callback<void()> func) {
    func();
}

void CallJsbCallbackReturnString(Jsb::Callback<std::string()> func) {
    auto str = func();
    OH_LOG_Print(LOG_APP, LOG_INFO, 0x000, "callback-cpp", "Jsb::Callback callback return from Js: %{public}s", str.c_str());
}

void CallJsbSafetyCallbackReturnVoid(Jsb::SafetyCallback<void()> func) {
    func();
}

void CallJsbSafetyCallbackReturnString(Jsb::SafetyCallback<std::string()> func) {
    auto str = func();
    OH_LOG_Print(LOG_APP, LOG_INFO, 0x000, "callback-cpp", "Jsb::SafetyCallback callback return from Js: %{public}s", str.c_str());
}

JSBIND_GLOBAL() {
    JSBIND_FUNCTION(CallFunctionReturnVoid);
    JSBIND_FUNCTION(CallFunctionReturnString);
    JSBIND_FUNCTION(CallJsbCallbackReturnVoid);
    JSBIND_FUNCTION(CallJsbCallbackReturnString);
    JSBIND_FUNCTION(CallJsbSafetyCallbackReturnVoid);
    JSBIND_FUNCTION(CallJsbSafetyCallbackReturnString);
}

JSBIND_ADDON(callback) // 注册 JSBind 插件名为: callback