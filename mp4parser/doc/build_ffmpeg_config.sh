/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
# FFmpeg build configure
#!/bin/bash
FFMPEG_PATH=$1
FFMPEG_OUT_PATH=$2
pwd
echo "***开始准备编译ffmpeg***"
echo $FFMPEG_OUT_PATH
echo $FFMPEG_PATH
FF_CONFIG_OPTIONS="
    --disable-programs
    --disable-avdevice
    --disable-postproc
    --disable-network
    --disable-dwt
    --disable-lsp
    --disable-lzo
    --disable-faan
    --disable-pixelutils
    --disable-bsfs
    --disable-encoders
    --disable-decoders
    --disable-hwaccels
    --disable-muxers
    --disable-demuxers
    --disable-parsers
    --disable-protocols
    --disable-devices
    --disable-asm
    --disable-doc
    --disable-debug
    --disable-iconv
    --disable-stripping
    --disable-vaapi
    --disable-vdpau
    --disable-xlib
    --disable-cuvid
    --disable-cuda
    --disable-libxcb
    --disable-libxcb_shm
    --disable-libxcb_shape
    --disable-libxcb_xfixes
    --disable-sdl2
    --disable-bzlib
    --disable-lzma
    --disable-filters
    --enable-zlib
    --enable-filter=scale,format,trim,null,amix,anull,abuffer,abuffersink,aformat,aresample,overlay,volume,movie
    --enable-protocol=file
    --enable-bsf=h264_mp4toannexb
    --enable-demuxer=concat,mp3,aac,ape,flac,ogg,wav,mov,mpegts,amr,pcm_s16le,image2
    --enable-muxer=mp4,h264,mp3,wav,amr,image2
    --enable-parser=h263,mpeg4video,vp8,vp9,mp3,h264
    --enable-parser=mpegaudio,aac,aac_latm
    --enable-decoder=h263,h264,mpeg2video,mpeg4,vp8,vp9
    --enable-decoder=mp3,mp3float,aac,aac_latm,ape,flac,vorbis,opus,png,pcm_s16le
    --enable-encoder=aac,aac_latm,opus,png
    --enable-encoder=mpeg4,h263,mp3,pcm_s16le
"
FF_CONFIG_OPTIONS=`echo $FF_CONFIG_OPTIONS`
${FFMPEG_PATH}/configure ${FF_CONFIG_OPTIONS}
sed -i 's/HAVE_SYSCTL 1/HAVE_SYSCTL 0/g' ohffmpeg_config.h
sed -i 's/HAVE_SYSCTL=yes/!HAVE_SYSCTL=yes/g' ./ohffmpeg_ffbuild/config.mak
sed -i 's/HAVE_GLOB 1/HAVE_GLOB 0/g' ohffmpeg_config.h
sed -i 's/HAVE_GLOB=yes/!HAVE_GLOB=yes/g' ohffmpeg_config.h
sed -i 's/HAVE_GMTIME_R 1/HAVE_GMTIME_R 0/g' ohffmpeg_config.h
sed -i 's/HAVE_LOCALTIME_R 1/HAVE_LOCALTIME_R 0/g' ohffmpeg_config.h
sed -i 's/HAVE_PTHREAD_CANCEL 1/HAVE_PTHREAD_CANCEL 0/g' ohffmpeg_config.h
sed -i 's/HAVE_VALGRIND_VALGRIND_H 1/HAVE_VALGRIND_VALGRIND_H 0/g' ohffmpeg_config.h
tmp_file=".ohffmpeg_tmpfile"
## remove invalid restrict define
sed 's/#define av_restrict restrict/#define av_restrict/' ./ohffmpeg_config.h >$tmp_file
mv $tmp_file ./ohffmpeg_config.h
## replace original FFMPEG_CONFIGURATION define with $FF_CONFIG_OPTIONS
sed '/^#define FFMPEG_CONFIGURATION/d' ./ohffmpeg_config.h >$tmp_file
mv $tmp_file ./ohffmpeg_config.h
total_line=`wc -l ./ohffmpeg_config.h | cut -d' ' -f 1`
tail_line=`expr $total_line - 3`
head -3 ohffmpeg_config.h > $tmp_file
echo "#define FFMPEG_CONFIGURATION \"${FF_CONFIG_OPTIONS}\"" >> $tmp_file
tail -$tail_line ohffmpeg_config.h >> $tmp_file
mv $tmp_file ./ohffmpeg_config.h
rm -f config.err
## rm BUILD_ROOT information
sed '/^BUILD_ROOT=/d' ./ohffmpeg_ffbuild/config.mak > $tmp_file
rm -f ./ohffmpeg_ffbuild/config.mak
mv $tmp_file ./ohffmpeg_ffbuild/config.mak
## rm amr-eabi-gcc
sed '/^CC=arm-eabi-gcc/d' ./ohffmpeg_ffbuild/config.mak > $tmp_file
rm -f ./ohffmpeg_ffbuild/config.mak
mv $tmp_file ./ohffmpeg_ffbuild/config.mak
## rm amr-eabi-gcc
sed '/^AS=arm-eabi-gcc/d' ./ohffmpeg_ffbuild/config.mak > $tmp_file
rm -f ./ohffmpeg_ffbuild/config.mak
mv $tmp_file ./ohffmpeg_ffbuild/config.mak
## rm amr-eabi-gcc
sed '/^LD=arm-eabi-gcc/d' ./ohffmpeg_ffbuild/config.mak > $tmp_file
rm -f ./ohffmpeg_ffbuild/config.mak
mv $tmp_file ./ohffmpeg_ffbuild/config.mak
## rm amr-eabi-gcc
sed '/^DEPCC=arm-eabi-gcc/d' ./ohffmpeg_ffbuild/config.mak > $tmp_file
rm -f ./ohffmpeg_ffbuild/config.mak
mv $tmp_file ./ohffmpeg_ffbuild/config.mak
sed -i 's/restrict restrict/restrict /g' ohffmpeg_config.h
sed -i '/getenv(x)/d' ohffmpeg_config.h
sed -i 's/HAVE_DOS_PATHS 0/HAVE_DOS_PATHS 1/g' ohffmpeg_config.h
mv ohffmpeg_config.h ${FFMPEG_OUT_PATH}/config.h
mv ./ohffmpeg_ffbuild/config.mak ${FFMPEG_OUT_PATH}/config.mak
mv -f libavcodec_oh ${FFMPEG_OUT_PATH}
mv -f libavformat_oh ${FFMPEG_OUT_PATH}
mv -f libavutil_oh ${FFMPEG_OUT_PATH}
mv -f libavdevice_oh ${FFMPEG_OUT_PATH}
mv -f libavfilter_oh ${FFMPEG_OUT_PATH}
cd ${FFMPEG_OUT_PATH}
mv libavcodec_oh libavcodec
mv libavformat_oh libavformat
mv libavutil_oh libavutil
mv libavdevice_oh libavdevice
mv libavfilter_oh libavfilter
#rm -rf ./ffbuild
echo "***编译FFmpeg准备结束***"
## other work need to be done manually
cat <<!EOF
#####################################################
                    ****NOTICE****
You need to modify the file config.mak and delete
all full path string in macro:
SRC_PATH, SRC_PATH_BARE, BUILD_ROOT, LDFLAGS.
Please refer to the old version of config.mak to
check how to modify it.
#####################################################
!EOF
