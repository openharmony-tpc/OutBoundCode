#include "../include/libavcodec/avcodec.h"
#include "../include/libavformat/avformat.h"
#include "../include/libswscale/swscale.h"
#include "../ffmpeg/ffmpeg.h"
#include <pthread.h>
#include <string.h>
#ifdef __cplusplus
extern "C" {
#endif
int ffmpeg_thread_run_cmd(int cmdnum,char **argv);
void ffmpeg_thread_callback(void (*cb)(int ret));
void ffmpeg_thread_exit(int ret);
void ffmpeg_thread_cancel();
#ifdef __cplusplus
}
#endif

