/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import mp4parser_napi from "libmp4parser_napi.so"
import { IFileCallBack } from "../mp4parser/callback/IFileCallBack"
import { ICallBack } from "../mp4parser/callback/ICallBack"
import { FileUtils } from "../mp4parser/utils/FileUtils"
import { IFileCachePathCallBack } from "../mp4parser/callback/IFileCachePathCallBack";


export class MP4Parser {

  /**
   * Video clipping
   * @param startTime
   * @param endTime
   * @param sourcePath
   * @param outPath
   * @param callback
   */
  static videoClip(startTime: string, endTime: string, sourcePath: string, outPath: string, callBack: ICallBack): void {
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -i " + sourcePath + " -ss " + startTime + " -c copy -to " + endTime + " " + outPath)
      .then(function (res) {
        callBack.callBackResult(res)
      });
  }

  /**
   * Video synthesis
   * @param sourcePath
   * @param outPath
   * @param callBack
   */
  static videoMerge(filePath_one: string, filePath_two: string, outPath: string, callBack: ICallBack): void {
    var fileCachePathCallBack: IFileCachePathCallBack = {
      callBackResult(path: string) {
        mp4parser_napi.exeFFmpegCmd("ffmpeg -y -f concat -safe 0 -i " + path + " -c copy " + outPath)
          .then(function (res) {
            callBack.callBackResult(res)
          });
      }
    }
    FileUtils.createMergeFileByPath(filePath_one, filePath_two, fileCachePathCallBack);

  }

  /**
  * Batch video synthesis
  * @param sourcePath
  * @param outPath
  * @param callBack
  */
  static videoMultMerge(sourcePath: string, outPath: string, callBack: ICallBack): void {
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -f concat -safe 0 -i " + sourcePath + " -c copy " + outPath)
      .then(function (res) {
        callBack.callBackResult(res)
      });
  }

  /**
    * Batch audio synthesis
    * @param sourcePath
    * @param outPath
    * @param callBack
    */
  static audioMultMerge(sourcePath: string, outPath: string, callBack: ICallBack): void {
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -f concat -safe 0 -i " + sourcePath + " -c copy " + outPath)
      .then(function (res) {
        callBack.callBackResult(res)
      });
  }


  /**
   * Audio synthesis
   * @param sourcePath
   * @param outPath
   * @param callBack
   */
  static audioMerge(filePath_one: string, filePath_two: string, outPath: string, callBack: ICallBack) {
    var fileCachePathCallBack: IFileCachePathCallBack = {
      callBackResult(path: string) {
        mp4parser_napi.exeFFmpegCmd("ffmpeg -y -f concat -safe 0 -i  " + path + " -c copy " + outPath)
          .then(function (res) {
            callBack.callBackResult(res)
          });
      }
    }
    FileUtils.createMergeFileByPath(filePath_one, filePath_two, fileCachePathCallBack);
  }


  /**
   * Audio clipping
   * @param startTime
   * @param endTime
   * @param sourcePath
   * @param outPath
   * @param callBack
   */
  static audioClip(startTime: string, endTime: string, sourcePath: string, outPath: string, callBack: ICallBack) {
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -i " + sourcePath + " -vn -acodec copy -ss  " + startTime + " -to " + endTime + " " + outPath)
      .then(function (res) {
        callBack.callBackResult(res)
      });
  }

  /**
  * 音频混音
  * @param sourcePath_1
  * @param sourcePath_2
  * @param outPath
  * @param callBack
  */
  static audioMix(sourcePath1: string, sourcePath2: string, outPath: string, callBack: ICallBack) {
    let tempPath = FileUtils.fileNameClip(outPath);
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -i " + sourcePath1 + " -i " + sourcePath2 + " -filter_complex amix=inputs=2:duration=shortest " + tempPath)
      .then(function (res) {
        if (res == 0) {
          var fileCallBack: IFileCallBack = {
            callBackResult(code: number) {
              callBack.callBackResult(code)
            }
          }
          FileUtils.reFileName(tempPath, outPath, fileCallBack)
        }
        else {
          callBack.callBackResult(1)
        }
      });
  }


  /**
   * 设置某段音频静音
   * @param startTime
   * @param endTime
   * @param sourcePath
   * @param outPath
   * @param callBack
   */
  static audioMute(startTime: string, endTime: string, sourcePath: string, outPath: string, callBack: ICallBack) {
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -i " + sourcePath + " -vn -acodec copy -ss  " + startTime + " -t " + endTime + " " + outPath)
      .then(function (res) {
        callBack.callBackResult(res)
      });
  }

  /**
  * 视频和音频分离
  * @param sourcePath
  * @param outPath
  * @param callBack
  */
  static videoDetachedAudio(sourcePath: string, outPath: string, callBack: ICallBack) {
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -i " + sourcePath + " -vcodec copy -an " + outPath).then(function (res) {
      callBack.callBackResult(res)
    });
  }

  /**
   * 视频替换音轨
   * @param sourcePath
   * @param audioPath
   * @param outPath
   * @param callBack
   */
  static videoReplaceAudio(sourcePath: string, audioPath: string, outPath: string, callBack: ICallBack) {
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -i " + sourcePath + " -i " + audioPath + " -vcodec copy -acodec copy " + outPath)
      .then(function (res) {
        callBack.callBackResult(res)
      });
  }


  /**
   * 视频添加水印
   * @param sourcePath
   * @param outPath
   * @param fontPath
   * @param text
   * @param x
   * @param y
   * @param fontsize
   * @param fontColor
   * @param callBack
   */
  static videoAddWaterMark(sourcePath: string, outPath: string, picPath: string, x: number, y: number, callBack: ICallBack) {
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -i "+sourcePath+" -vf movie="+picPath+"[wm];[in][wm]overlay="+x+":"+y+"[out] "+outPath)
      .then(function (res) {
        callBack.callBackResult(res)
      });
  }

  /**
   * 转码
   * @param sourcePath
   * @param outPath
   * @param callBack
   */
  static transcode(sourcePath: string, outPath: string, callBack: ICallBack) {
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -i " + sourcePath + " " + outPath).then(function (res) {
      callBack.callBackResult(res)
    });
  }

  /**
   * 修改音量
   * @param sourcePath
   * @param outPath
   * @param volume
   * @param callBack
   */
  static resetAudioVolume(sourcePath: string, outPath: string, volume: string, callBack: ICallBack) {
    let tempPath = FileUtils.fileNameClip(outPath);
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -i " + sourcePath + " -af volume=" + volume + " " + tempPath)
      .then(function (res) {
        if (res == 0) {
          var fileCallBack: IFileCallBack = {
            callBackResult(code: number) {
              callBack.callBackResult(code)
            }
          }
          FileUtils.reFileName(tempPath, outPath, fileCallBack)
        }
        else {
          callBack.callBackResult(1)
        }
      });
  }

  /**
   * 提取音频
   * @param sourcePath
   * @param outPath
   * @param volume
   * @param callBack
   */
  static getAudioFromVideo(sourcePath: string, outPath: string, callBack: ICallBack) {
    mp4parser_napi.exeFFmpegCmd("ffmpeg -y -i " + sourcePath + " -vn " + outPath)
      .then(function (res) {
        callBack.callBackResult(res)
      });
  }

  static openNativeLog() {
    mp4parser_napi.openNativeLog();
  }
}