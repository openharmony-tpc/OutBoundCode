# awsSdkS3Demo

@aws-sdk/client-s3使用了官方版本和OpenHarmony Polyfill ACE2.0 ETS版本进行提供标准Web接口适配

@aws-sdk/client-s3 地址

https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/clients/client-s3/index.html

OpenHarmony Polyfill地址 

https://gitee.com/originjs/openharmony-polyfill

进入entry文件夹目录，打开cmd 执行npm i openharmony-polyfill、
npm i @aws-sdk/client-s3、npm i @aws-sdk/client-cognito-identity、npm i @aws-sdk/credential-provider-cognito-identity命令

引入openharmony-polyfill初始标准Web接口适配
```js
import 'openharmony-polyfill'
```
导入@aws-sdk相关模块
```js
import {S3Client, ListObjectsCommand, GetBucketCorsCommand, DeleteObjectCommand, PutObjectCommand } from "@aws-sdk/client-s3";
import {CognitoIdentityClient} from "@aws-sdk/client-cognito-identity";
import {fromCognitoIdentityPool} from "@aws-sdk/credential-provider-cognito-identity";
```


开启网络权限

```js
"reqPermissions": [
{
"name": "ohos.permission.INTERNET"
}
]
```
## 命令API发送

### `send`

```js
S3Client.send(...)
```
## 常用命令
### `ListObjectsCommand`
获取桶对象命令
```js
import { S3Client, ListObjectsCommand } from "@aws-sdk/client-s3"; // ES Modules import
// const { S3Client, ListObjectsCommand } = require("@aws-sdk/client-s3"); // CommonJS import
const client = new S3Client(config);
const command = new ListObjectsCommand(input);
const response = await client.send(command);
```

### `GetBucketCorsCommand`
获取桶Cors命令
```js
import { S3Client, GetBucketCorsCommand } from "@aws-sdk/client-s3"; // ES Modules import
// const { S3Client, GetBucketCorsCommand } = require("@aws-sdk/client-s3"); // CommonJS import
const client = new S3Client(config);
const command = new GetBucketCorsCommand(input);
const response = await client.send(command);
```

### `DeleteObjectCommand`
删除对象命令
```js
import { S3Client, DeleteObjectCommand } from "@aws-sdk/client-s3"; // ES Modules import
// const { S3Client, DeleteObjectCommand } = require("@aws-sdk/client-s3"); // CommonJS import
const client = new S3Client(config);
const command = new DeleteObjectCommand(input);
const response = await client.send(command);
```

### `PutObjectCommand`

上传对象命令

```js
import { S3Client, PutObjectCommand } from "@aws-sdk/client-s3"; // ES Modules import
// const { S3Client, PutObjectCommand } = require("@aws-sdk/client-s3"); // CommonJS import
const client = new S3Client(config);
const command = new PutObjectCommand(input);
const response = await client.send(command);
```

## @aws-sdk/client-s3其他命令的调用请参考官网
https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/clients/client-s3/index.html
