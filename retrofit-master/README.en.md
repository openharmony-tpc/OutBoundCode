# retrofit
一A HTTP client for openharmony ETS platform.

## Download & Install
```javascript
npm install @ohos/retrofit --save
```

Details about OpenHarmony NPM environment configuration, see at
(https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md)

## Usage Instructions

##### Create HTTP Request class
    Retrofit parses HTTP Request class and provides method based api calling. 
    As shown below, Create Custom HTTP request class like "DataService" Extending "BaseService".
    Retrofit supports API declaration with Decorators, for example to make "GET" api call use @GET. Other api's supported are listed under "API Introduction" below.
    Use "retrofit" module to import retrofit apis.

``` javascript
import {BaseService,ServiceBuilder,GET,POST,DELETE,PUT,Path,Body,BasePath,Response,Header,Query} from '@ohos/retrofit';

//BasePath - Append "/" to basepath.
@BasePath("/")
class DataService extends BaseService{
  
  //GET Api call with Header and Query params
  @GET("get?test=arg123")
  async getRequest(@Header("Content-Type") contentType: string,@Query('d1') data1: string,@Query('d2') data2: number): Promise<Response<Data>> { return <Response<Data>>{} };

  //POST Api call with Header and Body info
  @POST("post")
  async postRequest(@Header("Content-Type") contentType: string,@Body user: User): Promise<Response<Data>> { return <Response<Data>>{} };

  //PUT Api call with Header and Body info
  @PUT("put")
  async putRequest(@Header("Content-Type") contentType: string,@Body user: User): Promise<Response<Data>> { return <Response<Data>>{} };

  //GET Api call with Header, Query and Path Param
  @GET("{req}?test=arg123")
  async getRequest2(@Header("Content-Type") contentType: string,@Query('d1') data1: string,@Query('d2') data2: number,@Path("req") requestPath: string): Promise<Response<Data>> { return <Response<Data>>{} };
}
```

##### Initialize Retrofit service and call methods. 
    Initialize the Retrofit BaseService, Set End point and Call required apis by its method.

``` javascript
    const dataService = new ServiceBuilder()
      .setEndpoint("https://restapiservice.com")    //Base Url
      .build(DataService);

    dataService.getRequest("application/json","dat1",8).then((resp)=>{  //Get request with params
        console.log("getRequest Response =" + JSON.stringify(resp.result));
    }
```

### API/Interface Details

#### BaseService.ServiceBuilder
| Interface name | Parameter | Return Value | Remarks |
| ----------------- | ----------------- | ----------------- | ----------------- |
| setEndpoint() | endpoint: string | ServiceBuilder | Set request base url |
| setTimeout | timeout: number | ServiceBuilder | set Request timeout in milli second(ms) |
| build<T> | service: new (builder: ServiceBuilder) => T | T | Build Retrofit Base service |

### BaseService
| Interface name | Parameter | Return Value | Remarks |
| ----------------- | ----------------- | ----------------- | ----------------- |
| setEndpoint() | endpoint: string | void | Set the base url |
| clone() | None | BaseService | Clone the entire Base service request. so we can reuse existing request configuration for new request. |

### Decorators
| Decorators name | Remarks |
| ----------------- | ----------------- |
| @GET | Create GET HTTP request |
| @POST | Create POST HTTP request |
| @PUT | Create PUT HTTP request |
| @DELETE | Create DELETE HTTP request |
| @HEAD | Create HEAD HTTP request |
| @OPTIONS | Create OPTIONS HTTP request |
| @BasePath | BasePath Decorator for appending text to base path |
| @Path | Add path to url |
| @Body | Body Decorator for passing body |
| @Headers | Headers Decorator for setting Headers |
| @Header | Header Decorator for setting an header |
| @HeaderMap | HeaderMap Decorator for setting header as map object |
| @Queries | Queries Decorator for setting query list |
| @Query | Query Decorator for setting query |
| @QueryMap | QueryMap Decorator for setting query in map |
| @FormUrlEncoded | FormUrlEncoded Decorator for enabling formurlencoding for request body. |
| @Field | Field Decorator for setting a field for post method |
| @FieldMap | FieldMap Decorator for setting fields with Map object |
| @Timeout | Timeout Decorator for setting request timeout |

## Compatibility
Supports OpenHarmony API version 9

## Directory Structure
````
|---- retrofit  
|     |---- entry  # sample app usage
|     |---- retrofit  # retrofit library
|           |---- index.ets  # External interface
|     |---- README.MD  # installation and usage                   
````

## Code Contribution
If you find any problems during usage, you can submit an [Issue](https://gitee.com/openharmony-tpc/retrofit/issues) to us. Of course, we also welcome you to send us [PR](https://gitee.com/openharmony-tpc/retrofit/pulls).

## Open source License
This project is based on [Apache License 2.0](https://gitee.com/openharmony-tpc/retrofit/blob/master/LICENSE) ，please enjoy and participate in open source freely.

