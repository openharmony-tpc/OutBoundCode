/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const { ProvidePlugin } = require("webpack");
module.exports = class OpenharmonyPolyfillPlugin {
  constructor() {}

  apply(compiler) {
    compiler.options.plugins.push(
      new ProvidePlugin({
        TextDecoder: [
          require.resolve("@ohos/openharmony-node-polyfill/lib/node/util"),
          "TextDecoder",
        ],
        process: require.resolve(
          "@ohos/openharmony-node-polyfill/lib/node/process/"
        ),
        Buffer: [
          require.resolve("@ohos/openharmony-node-polyfill/lib/node/buffer/"),
          "Buffer",
        ],
        primordials: require.resolve(
          "@ohos/openharmony-node-polyfill/lib/core/primordials.js"
        ),
        global: require.resolve(
          "@ohos/openharmony-node-polyfill/lib/global.js"
        ),
        WebSocket: [
          require.resolve("@ohos/openharmony-node-polyfill/lib/web/websocket/"),
          "WebSocket",
        ],
      })
    );

    compiler.options.resolve.fallback = {
      primordials: require.resolve(
        "@ohos/openharmony-node-polyfill/lib/core/primordials"
      ),
      events: require.resolve(
        "@ohos/openharmony-node-polyfill/lib/node/events/"
      ),
      fs: require.resolve("@ohos/openharmony-node-polyfill/lib/node/fs"),
      buffer: require.resolve(
        "@ohos/openharmony-node-polyfill/lib/node/buffer"
      ),
      stream: require.resolve(
        "@ohos/openharmony-node-polyfill/lib/node/stream/lib/ours/index"
      ),
      path: require.resolve("@ohos/openharmony-node-polyfill/lib/node/path"),
    };

    // compiler.options.resolve.modules = [
    //   path.resolve(__dirname, "./myplugins/openharmony-node-polyfill/lib/node"),
    //   ...compiler.options.resolve.modules,
    // ];
  }
};
