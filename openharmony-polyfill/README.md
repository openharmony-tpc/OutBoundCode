# openharmony-polyfill

用于 npm 仓中 nodejs build-in 基础模块的 api 适配，通过 webpack 插件形式，避免原 npm 库在鸿蒙平台上的侵入式修改。

# 一、简述

- **openharmony-polyfill**：开发思想是参考 NodeJs 中,模块以及 Api 的设计思想，基于 oh 现有的能力去做封装适配
  <br>

- **openharmony-webpack-plugin**：是一款 webpack 插件，原理是利用了 webpack 打包时，加载模块失败可以重定向的机制。有这个机制我们就可以将三方库引用的 NodeJs 的模块，重定向到 openharmony-polify 中

### <a name="nodejs适配功能模块范围"></a>其他

|   nodejs 模块    |              描述              | 备注 |
| :--------------: | :----------------------------: | :--: |
|     [buffer]     |             缓冲区             |
|    [console]     |           控制台输出           |
|     [crypto]     |          安全加密算法          |
|     [dgram]      |        UDP 数据报套接字        |
|      [dns]       |            域名解析            |
|     [events]     |         事件触发与监听         |
|       [fs]       |           文件子系统           |
|      [net]       |           网络子系统           |
|      [path]      |        文件和目录的路径        |
|    [process]     |            进程处理            |
|  [querystring]   |           查询字符串           |
|    [readline]    |      逐行的方式读取数据流      |
|     [stream]     | 可读流，可写流，双向流，转换流 |
| [string_decoder] |          字符串解码器          |
|     [timers]     |             定时器             |
|      [tls]       |        安全加密网络传输        |
|      [url]       |         URL 处理与解析         |
|      [zlib]      |              压缩              |
|      [util]      |            工具集合            |
| [worker_threads] |             多线程             |

# 二、集成

- 1.找到 openharmony 的 SDK 目录（例如：<span style="color:rgb(230,73,25)">\Users\AppData\Local\OpenHarmony\Sdk\ets\1.1.1.5\build-tools\ets-loader</span>）
- 2.将 **myplugins** 文件夹，拷贝到当前目录下

- 3.修改**package.json**。如下图：

```javascript
"@ohos/openharmony-webpack-plugin": "file:./myplugins/openharmony-webpack-plugin",
"@ohos/openharmony-node-polyfill": "file:./myplugins/openharmony-node-polyfill"
```

![图片说明](http://image.huawei.com/tiny-lts/v1/images/0c529b926b5f90006733ef749ee028b8_938x402.png)

- 4.在当前 SDK 目录下运行：**npm install**
- 5.修改 webpack.config.js 文件。如下图：
  ![图片说明](http://image.huawei.com/tiny-lts/v1/images/fbfe2afcc51738a2780cb073d6341f89_871x696.png)
