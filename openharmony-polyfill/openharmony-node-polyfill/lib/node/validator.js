const nodeInternalPrefix = "__node_internal_";
const { ObjectDefineProperty, ArrayIsArray } = primordials;
const hideStackFrames = (fn) => {
  // We rename the functions that will be hidden to cut off the stacktrace
  // at the outermost one
  const hidden = nodeInternalPrefix + fn.name;
  ObjectDefineProperty(fn, "name", { __proto__: null, value: hidden });
  return fn;
};
exports.validateString = hideStackFrames((value, name) => {
  if (typeof value !== "string") {
    throw new Error("ERR_INVALID_ARG_TYPE value:" + value + " name:" + name);
  }
});
exports.validateFunction = hideStackFrames((value, name) => {
  if (typeof value !== "function")
    throw new Error("ERR_INVALID_ARG_TYPE value:" + value + " name:" + name);
});
exports.validateAbortSignal = hideStackFrames((signal, name) => {
  if (
    signal !== undefined &&
    (signal === null || typeof signal !== "object" || !("aborted" in signal))
  ) {
    // TODO yzq
    throw new Error("ERR_INVALID_ARG_TYPE value:" + value + " name:" + name);
    // throw new ERR_INVALID_ARG_TYPE(name, "AbortSignal", signal);
  }
});

exports.validateObject = hideStackFrames((value, name, options) => {
  const useDefaultOptions = options == null;
  const allowArray = useDefaultOptions ? false : options.allowArray;
  const allowFunction = useDefaultOptions ? false : options.allowFunction;
  const nullable = useDefaultOptions ? false : options.nullable;
  if (
    (!nullable && value === null) ||
    (!allowArray && ArrayIsArray(value)) ||
    (typeof value !== "object" &&
      (!allowFunction || typeof value !== "function"))
  ) {
    // TODO yzq
    throw new Error("ERR_INVALID_ARG_TYPE value:" + value + " name:" + name);
    // throw new ERR_INVALID_ARG_TYPE(name, "Object", value);
  }
});
