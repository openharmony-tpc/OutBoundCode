# CameraView

## 简介

> cameraview主要是实现相机相关基础功能的开发 (包含启动/关闭相机,预览,拍照,录像等)。

![img1.png](img1.png)

## 下载安装
```
```

## 使用说明

1.	启动应用，弹出权限弹窗中授权后返回应用，首页显示当前设备的相册信息，首页监听相册变化会刷新 相册列表。
2.	点击 + 按钮，弹出相机图标；
3.	点击 相机 图标，进入相机界面，默认是拍照模式，点击底部拍照按钮可以拍照，拍照完成后会在底部左侧显示照片预览图；点击录像切换到录像模式，点击底部按钮开始/结束录像，结束录像后底部左侧显示视频图标。点击系统Back键返回。

```
     	// 相机基础事件回调
	protected final Callback mCallback;
	// 渲染视图 
	protected final PreviewImpl mPreview;
	// 获取渲染视图
	View getView() {
	return mPreview.getView();
	
	// 启动相机
	abstract boolean start();
	// 暂停相机
	abstract void stop();
	// 相机使用状态
	abstract boolean isCameraOpened();
	// 获取相机支持的预览比例
	abstract Set&lt;AspectRatio&gt; getSupportedAspectRatios();
	// 设置拍摄照片比例
	abstract boolean setAspectRatio(AspectRatio ratio);
	// 获取相机当前摄照片比例
	abstract AspectRatio getAspectRatio();
	// 设置自动聚焦 
	abstract void setAutoFocus(boolean autoFocus);
	// 获取自动聚焦 
	abstract boolean getAutoFocus();
	// 获取静态图片&#xff0c;即拍照
	abstract void takePicture();
	// 设置相机方向
	abstract void setDisplayOrientation(int displayOrientation);
	// 相机基础回调接口
	interface Callback {
	// 相机已打开
	void onCameraOpened();
	// 相机已关闭
	void onCameraClosed();
	
```



## 目录结构
````
|---- CameraView 
|     |---- entry  # 示例代码文件夹
|           |---- index.ets  # 对外接口
|     |---- modules  # 库模块
|     |---- README.MD  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue]() 给我们，当然，我们也非常欢迎你给我们发 [PR]() 。

## 开源协议
本项目基于 [Apache License 2.0]() ，请自由地享受和参与开源。